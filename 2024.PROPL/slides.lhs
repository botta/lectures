% -*-Latex-*-
%\documentclass[a4paper,landscape]{slides}
\documentclass[colorhighlight,coloremph,fleqn,aspectratio=169, 
               %handout
              ]{beamer}
\usepackage[mode=buildnew]{standalone}
\usetheme{boxes}
\usepackage{color,soul}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[english]{babel}
\usepackage{ucs}
\usepackage[utf8x]{inputenc}
\usepackage{csquotes}
\usepackage{moreverb}
\usepackage{fancyvrb}
\usepackage{xcolor}
\usepackage{tikz}
\usepackage{tikz-cd}
\usetikzlibrary{cd, automata, arrows, positioning, backgrounds} 
\usetikzlibrary{arrows,positioning,shapes.callouts,decorations.pathmorphing,
  shapes, fadings}
\usepackage[T1]{fontenc}


\setbeamertemplate{navigation symbols}{}%remove navigation symbols

\setbeamerfont{normal text}{size=\small}
\AtBeginDocument{\usebeamerfont{normal text}}

\newcommand{\yslant}{0.5}
\newcommand{\xslant}{-1.5}

%include slides.fmt

\input{macros.TeX}

\definecolor{beamerblue}{rgb}{0.2,0.2,0.7}

\addfootbox{section}{\quad \tiny PROgramming for the PLanet workshop, Jan~2024}
\addfootbox{author in head/foot}{Botta, Jansson, et al.}

\title{Can computer science help climate policy?}

\author{N.~Botta\inst{1,2}, P.~Jansson\inst{2}, N.~Brede\inst{3},
  C.~Ionescu\inst{4}, T.~Richter\inst{3}, \dots}
\institute{\inst{1}{Potsdam Institute for Climate Impact Research} \\
           \inst{2}{Chalmers University of Technology} \\
           \inst{3}{Universität Potsdam} \\
           \inst{4}{Technische Hochschule Deggendorf}}

\begin{document}

\date{}

\frame{\maketitle}

%% \newcommand{\fixlengths}{\setlength{\abovedisplayskip}{6pt plus 1pt minus 1pt}\setlength{\belowdisplayskip}{6pt plus 1pt minus 1pt}}
%% \renewcommand{\hscodestyle}{\small\fixlengths}
%% \fixlengths

%%\renewcommand{\hscodestyle}{\small}

%include Slides.lidr

\bibliographystyle{jfplike}
\bibliography{references}

\end{document}
