% -*-Latex-*-

%if False

> module Slides

> import Data.Vect

> %default total
> %auto_implicits off
> %access public export

> -- %hide Prelude.Functor.Functor
> -- %hide Prelude.Functor.map
> -- %hide Prelude.Monad.Monad

> infix 6 <=>

> (<=>) : {A, B : Type} -> (A -> B) -> (A -> B) -> Type
> (<=>) {A} f g = (x : A) -> f x = g x

> interface VeriFunctor (F : Type -> Type) where
>   map          :  {A, B : Type}     ->  (A -> B) -> F A -> F B
>
>   mapPresEE    :  {A, B : Type}     ->  (f, g : A -> B) ->  f <=> g -> map f <=> map g
>   mapPresId    :  {A : Type}        ->  map id <=> id {a = F A}
>   mapPresComp  :  {A, B, C : Type}  ->  (g : B -> C) -> (f : A -> B) -> map (g . f) <=>  map g . map f

> infixr 1 >=>
> infixr 1 <=<

> interface VeriFunctor M => VeriMonad (M : Type -> Type) where
>
>   pure                :  {A : Type}        ->  A -> M A
>   join                :  {A : Type}        ->  M (M A) -> M A
>   (>>=)               :  {A, B : Type}     ->  M A -> (A -> M B) -> M B
>   (>=>)               :  {A, B, C : Type}  ->  (A -> M B) -> (B -> M C) -> (A -> M C)
>
>   bindJoinMapSpec     :  {A, B : Type}     ->  (f : A -> M B)  ->  (>>= f)    <=>  join . map f
>   kleisliJoinMapSpec  :  {A, B, C : Type}  ->  (f : A -> M B)  ->
>                                                  (g : B -> M C)  ->  (f >=> g)  <=>  join . map g . f
>   triangleLeft        :  {A : Type}    ->  join . pure      <=> id {a = M A}
>   triangleRight       :  {A : Type}    ->  join . map pure  <=> id {a = M A}
>   square              :  {A : Type}    ->  join . join      <=> join . map {A = M (M A)} join
>
>   pureNatTrans        :  {A, B : Type}   ->  (f : A -> B) ->  map f . pure  <=>  pure . f
>   joinNatTrans        :  {A, B : Type}   ->  (f : A -> B) ->  map f . join  <=>  join . map (map f)

> interface Preorder (A : Type) where
>   (<=)                :  A -> A -> Type
>
>   reflexive           : (x : A) -> x <= x
>   transitive          : (x, y, z : A) -> x <= y -> y <= z -> x <= z

> namespace Sigma
>   public export
>   record Sigma (A : Type) (P : A -> Type) where
>     constructor MkSigma
>     outl : A
>     outr : P outl

> {-

> -}

%endif


%% -------------------------------------------------------------------

\begin{frame}{The gap between climate science and climate policy}
%
\pause
%
IPCC Special Report on the impacts of global warming of 1.5°C, Jan. 2019:
%
\begin{minipage}[t][6cm][t]{1.0\textwidth}
\begin{figure}[htbp]
  \centering
  \includegraphics[clip, trim=2.0cm 6cm 0.0cm 9cm, width=0.70\textwidth]{./img/sr15_spm_final_p19.pdf}
\end{figure}
\end{minipage}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}{The gap between climate science and climate policy}

  \begin{itemize}
    %
    \vfill
    %
    \item<1-> Climate science has provided \hl<1>{enough} \hl<1>{scientific} \hl<1>{evidence}
      for the \hl<1>{need} for \hl<1>{global} \hl<1>{negative} GHG \hl<1>{emissions by} the mid of the
      century but \dots
      %
    \vfill
    %
    \item<2-> \dots\ achieving global negative GHG emissions requires dealing
      with \hl<2>{questions} that \hl<2>{cannot} be answered by \hl<2>{climate} \hl<2>{science} alone:
    %
    \vfill
    %
    \begin{itemize}
      \vfill
    \item<3-> Do fast green transformations lead to more/less wealth, inequality, migration?
      \vfill
    \item<3-> Which climate decisions over the next decades are likely
      to matter most?
      \vfill
    \item<3-> How (much) should long-term damages from CC inform current climate decisions?
      \vfill
    \item<3-> How do uncertainties affect "optimal" climate decisions?
      \vfill
    \item<3-> What does "optimal" mean when decisions have to account for conflicting goals?
      %
    \end{itemize}
    %
  \end{itemize}
  %
  \vfill
  %
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}{Can computer science help dealing with these questions?}

\pause
  
  \begin{itemize}
    %
    \vfill
    %
  \item<2-> Ionescu's formalization of vulnerability (2008-2014)
    %
    \vfill
    %
  \item<2-> Monadic dynamical systems (2008, 2021 \hl<3>{TiPES})
    %
    \vfill
    %
  \item<2-> Theory of policy advice and avoidability (2012-2017)
    %
    \vfill
    %
  \item<2-> Correctness of monadic backward induction (2021 \hl<3>{TiPES})
    %
    \vfill
    %
  \item<2-> Applications (2018, 2024 \hl<3>{TiPES})
    %
    \vfill
    %
  \item<2-> Conclusions    
    %
    \vfill
    %
  \item<3-> \hl<3>{TiPES}: European Union’s Horizon 2020 grant agreement No 820970
    %
    %
    \vfill
    %
  \item<4-> \hl<4->{\texttt{Slides.lidr}} \ at \ \hl<4->{\url{https://gitlab.pik-potsdam.de/botta/lectures}}
    %
  \end{itemize}
  %
  \vfill
  %
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Ionescu's formalization of vulnerability (2008-2014)}
\pause
%
\vfill
%
\begin{minipage}[t]{0.99\textwidth}
%\begin{flushleft}
\vspace{0pt}
\includegraphics[width=1.0\textwidth]{./img/MSCS_ionescu2014_1hl.pdf}
%\end{flushleft}
\end{minipage}
%
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

%if False

> X0              :  Type
> Evolution       :  Type -> Type
> F0              :  Type -> Type
> possible        :  X0 -> F0 (Evolution X0)
> V0              :  Type
> W0              :  Type
> harm            :  Evolution X0 -> V0
> measure         :  F0 V0 -> W0
> vulnerability   :  VeriFunctor F0 => X0 -> W0
> vulnerability   =  measure . map harm . possible
> nd              :  Preorder V0 => (V0 -> V0) -> Type
> nd f            =  (v : V0) -> v <= f v
> monMeasure      :  (VeriFunctor F0, Preorder V0, Preorder W0) =>
>                    (f : V0 -> V0) -> nd f -> (vs : F0 V0) ->
>                    measure vs <= measure (map f vs)

%endif

\begin{frame}[fragile]
\frametitle{Ionescu's formalization of vulnerability (2008-2014)}

\begin{itemize}
\vfill
\item<1-> \hl<1,2,6>{|vulnerability|} = \hl<1,5>{|measure|} |.| \hl<1>{|map|} \hl<1,4>{|harm|} |.| \hl<1,3>{|possible|}
\vfill
\item<2-> \hl<2>{|vulnerability|} is a function
\vfill
\item<3-> \hl<3>{|possible|} |:| |X -> F (Evolution X)|
\vfill
\item<4-> |Evolution X| is the type of \hl<4>{``scenarios''}: |X|, |(X, X)|, |Vect 100 X|, \dots
\vfill
\item<5-> |F| \hl<5>{uncertainty} functor: |List|, |Maybe|, |FiniteProb|, \dots
\vfill
\item<6-> \hl<6>{|harm|} |:| |Evolution X -> V|
\vfill
\item<7-> \hl<7>{|measure|} |:| |F V -> W|
\vfill
\item<8-> \hl<8>{|vulnerability|} |:| |VeriFunctor F => X -> W|
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Ionescu's formalization of vulnerability (2008-2014)}
%
\begin{itemize}
\vfill
\item<1-> If \hl<1-2>{harm} \hl<1-2>{increases}, \hl<1-2>{measures} of harm shall \hl<1-2>{not} \hl<1-2>{decrease}:

\pause

< monMeasure     :  (VeriFunctor F, Preorder V, Preorder W) =>
<
<                   (f : V -> V) -> nd f -> (vs : F V) ->
<
<                   measure vs <= measure (map f vs)

< nd f           =  (v : V) -> v <= f v

\vfill
\item<3-> \hl<3>{lessons} \hl<3>{learned} (Ionescu2014, Conclusions and Perspectives):
\vfill
\item<4-> \hl<4>{Formalization} is closely related with \hl<4>{generic} programming
  \vfill
\item<5-> \hl<5->{Dependent} \hl<5->{types} are best for expressing generic program
  \hl<5>{specifications} \dots
\vfill
\item<6-> \dots \hl<6->{understanding}, \hl<6->{testing} or \hl<6->{proving} the \hl<6->{correctness} of
  vulnerability studies
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Monadic dynamical systems (2008, 2021)}
\pause
%
\vfill
%
\begin{minipage}[t]{0.49\textwidth}
%\begin{flushleft}
\vspace{0pt}
% trim={<left> <lower> <right> <upper>}
\includegraphics[clip, trim={3.0cm 6cm 3.0cm 6cm},width=1.0\textwidth]{./img/PhD_ionescu2008_1hl.pdf}
%\end{flushleft}
\end{minipage}
\hfill
\begin{minipage}[t]{0.49\textwidth}
%\begin{flushleft}
\vspace{0pt}
\includegraphics[clip, trim={1.0cm 6cm 1.0cm 3cm}, width=1.0\textwidth]{./img/JFP_botta2021_1.pdf}
%\end{flushleft}
\end{minipage}
%
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Monadic dynamical systems (2008, 2021)}
%
\begin{itemize}
\vfill
\item<1-> |Evolution X| is the type of \hl<1>{scenarios}
\vfill
\item<2-> What are \hl<2>{computational} \hl<2>{principles} for scenarios?
\vfill
\item<3-> In a \hl<3>{time} \hl<3>{discrete} setting, deterministic dynamical systems
are \hl<3>{endo-functions}

> DetSys    :  Type -> Type
>
> DetSys X  =  X -> X

\vfill
\item<4-> and scenarios can be obtained by ``integrating'' (\hl<5>{iterating!}) a system:

\pause\pause\pause\pause
  
> flowDet : {X : Type} -> DetSys X -> Nat -> DetSys X
>
> flowDet f     Z     =  id
>
> flowDet f  (  S n)  =  f . flowDet f n

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Monadic dynamical systems (2008, 2021)}
%
\begin{itemize}
\vfill
\item<1-> Uncertainty can be represented functorially \dots

> MonSys : (Type -> Type) -> Type -> Type
>
> MonSys F X = X -> F X

\vfill
\item<2-> \dots monadic systems can be integrated like deterministic ones:

> flow  :  {M : Type -> Type} -> {X : Type} ->
>
>          VeriMonad M => MonSys M X -> Nat -> MonSys M X
>
> flow f     Z     =  pure
>
> flow f  (  S n)  =  f >=> flow f n

%if False

> X1              :  Type
> Evolution1      :  Type -> Type
> Evolution1 A    =  A
> M1              :  Type -> Type
> next1           :  X1 -> M1 X1
> possible1       :  VeriMonad M1 => X1 -> M1 (Evolution1 X1)
> possible1       =  flow next1 10

%endif

\vfill
\item<3-> Example: \hl<4->{|Evolution A = A|}, \ \hl<4->{|next : X -> M X|}, \ \hl<4->{|possible =  flow next n|}
%
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Monadic dynamical systems (2008, 2021)}
%
\begin{itemize}
\vfill
\item<1-> Scenarios can also be full trajectories:

< trj  :  {M : Type -> Type} -> {X : Type} -> VeriMonad M =>
<
<         MonSys M X -> (n : Nat) ->  X -> M (Vect (S n) X)
<
< trj f     Z     x  =  map (x ! ::) (pure Nil)
<
< trj f  (  S n)  x  =  map (x ! ::) ((f >=> trj f n) x)

%if False

> trj2  :  {M : Type -> Type} -> {X : Type} ->
>          VeriMonad M =>
>          MonSys M X -> (n : Nat) ->  X -> M (Vect (S n) X)
> trj2 f     Z     x  =  map (x ::) (pure Nil)
> trj2 f  (  S n)  x  =  map (x ::) ((f >=> trj2 f n) x)
> X2              :  Type
> Evolution2      :  Type -> Type
> Evolution2 A    =  Vect 11 A
> M2              :  Type -> Type
> next2           :  X2 -> M2 X2
> possible2       :  VeriMonad M2 => X2 -> M2 (Evolution2 X2)
> possible2       =  trj2 next2 10

%endif

\vfill
\item<2-> Example: \hl<2>{|Evolution A = Vect (S n) A|}, \ \hl<2>{|next : X -> M X|}, \ \hl<2>{|possible = trj next n|}
\vfill
\item<3-> \hl<3->{Consistent} theory/treatment of \hl<3->{deterministic}, \hl<3->{non-deterministic},
  \hl<3->{stochastic}, etc. systems
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Theory of policy advice and avoidability (2012-2017)}
\pause
%
\vfill
%
\begin{minipage}[t]{0.49\textwidth}
%\begin{flushleft}
\vspace{0pt}
\includegraphics[width=0.85\textwidth]{./img/notes2012_1hl.pdf}
%\end{flushleft}
\end{minipage}
\hfill
\begin{minipage}[t]{0.49\textwidth}
%\begin{flushleft}
\vspace{0pt}
\includegraphics[clip, trim={2.0cm 10cm 2.0cm 2cm},width=1.0\textwidth]{./img/JFP_botta2017_1.pdf}
%\end{flushleft}
\end{minipage}
%
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Theory of policy advice and avoidability (2012-2017)}
%
\vfill
%
\begin{minipage}[t]{0.49\textwidth}
%\begin{flushleft}
\vspace{0pt}
\includegraphics[width=0.85\textwidth]{./img/notes2012_1hl.pdf}
%\end{flushleft}
\end{minipage}
\hfill
\begin{minipage}[t]{0.49\textwidth}
%\begin{flushleft}
\vspace{0pt}
\includegraphics[clip, trim={2.0cm 10cm 2.0cm 2cm},width=1.0\textwidth]{./img/JFP_botta2017_1hl.pdf}
%\end{flushleft}
\end{minipage}
%
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

%if False

> M         :  Type -> Type
> X         :  (t : Nat) -> Type
> Y         :  (t : Nat) -> X t -> Type
> next      :  (t : Nat) -> (x : X t) -> Y t x -> M (X (S t))
> Val       :  Type
> reward    :  (t : Nat) -> (x : X t) -> Y t x -> X (S t) -> Val
> (<+>)     :  Val -> Val -> Val
> meas      :  M Val -> Val
> Policy    :  (t : Nat) -> Type
> Policy t  =  (x : X t) -> Y t x
> data PolicySeq : (t : Nat) -> (n : Nat) -> Type where
>   Nil   :  {t : Nat} -> PolicySeq t Z
>   (::)  :  {t, n : Nat} -> Policy t -> PolicySeq (S t) n -> PolicySeq t (S n)
> zero  :  Val
> infixr 7 <++>
> (<++>) : {A : Type} -> (f, g : A -> Val) -> A -> Val
> f <++> g = \ a => f a <+> g a
> val : Functor M => {t, n : Nat} -> PolicySeq t n -> X t -> Val
> val {t}  Nil      x  =  zero
> val {t} (p :: ps) x  =  let y    =  p x in
>                         let mx'  =  next t x y in
>                         meas (map (reward t x y <++> val ps) mx')
> OptPolicySeq  :  (Functor M, Preorder Val) => {t, n : Nat} -> PolicySeq t n -> Type
> OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> (x : X t) -> val ps' x <= val ps x
> BestExt  :  (Functor M, Preorder Val) => {t, n : Nat} -> PolicySeq (S t) n -> Policy t -> Type
> BestExt {t} ps p  =  (p' : Policy t) -> (x : X t) -> val (p' :: ps) x <= val (p :: ps) x
> Bellman  :  (Functor M, Preorder Val) => {t, n : Nat} ->
>             (ps   :  PolicySeq (S t) n) -> OptPolicySeq ps ->
>             (p    :  Policy t)          -> BestExt ps p ->
>             OptPolicySeq (p :: ps)
> bestExt      :  (Functor M, Preorder Val) => {t, n : Nat} -> PolicySeq (S t) n -> Policy t
> bestExtSpec  :  (Functor M, Preorder Val) => {t, n : Nat} ->
>                 (ps : PolicySeq (S t) n) -> BestExt ps (bestExt ps)
> bi  :   (Functor M, Preorder Val) => (t : Nat) -> (n : Nat) -> PolicySeq t n
> bi t  Z     =  Nil
> bi t (S n)  =  let ps = bi (S t) n in bestExt ps :: ps
> biLemma  :  (Functor M, Preorder Val) => (t : Nat) -> (n : Nat) -> OptPolicySeq (bi t n)

%endif

\begin{frame}[fragile]
\frametitle{Theory of policy advice and avoidability (2012-2017)}
%
\begin{itemize}
\vfill
\item<1-> The theory consists of \hl<1>{two} sets of software components
\vfill
\item<2-> One for \hl<2>{specifying} a finite horizon sequential decision problem
\vfill
\item<3-> One for \hl<3>{solving} that problem with \hl<3>{verified} backward induction
\vfill
\item<4-> The specification components are global \hl<4>{declarations}, for example

< M         :  Type -> Type
<
< X         :  (t : Nat) -> Type
<
< Y         :  (t : Nat) -> X t -> Type
<
< next      :  (t : Nat) -> (x : X t) -> Y t x -> M (X (S t))

\vfill
\item<5-> The solution components are \hl<5>{generic} \hl<5>{definitions}, for example

< Policy    :  (t : Nat) -> Type; ! ! Policy t  =  (x : X t) -> Y t x

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Theory of policy advice and avoidability (2012-2017)}
%
\begin{itemize}
\vfill
\item<1-> The theory is applied by \dots
\vfill
\item<2-> \dots \hl<2>{defining} all the \hl<2>{specification} \hl<2>{components}, for example

< M         =  FiniteProb
<
< X t       =  ClimateEconomyState t
<
< Y         =  (EmissionsAbatementRate, CarbonTax)

\vfill
\item<3-> \dots then \hl<3>{applying}

< bi       :  (t : Nat) -> (n : Nat) -> PolicySeq t n

\vfill
\item<4-> and \hl<4>{type} \hl<4>{checking}

< biLemma  :  (t : Nat) -> (n : Nat) -> OptPolicySeq (bi t n)

\vfill
\item<5->  to be \hl<5>{total} for a problem-specific initial decision step |t| and a horizon |n|

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Correctness of monadic backward induction (2021)}
\pause
%
\vfill
%
\begin{minipage}[t]{0.49\textwidth}
%\begin{flushleft}
\vspace{0pt}
\includegraphics[clip, trim={2.0cm 10cm 2.0cm 2cm}, width=0.9\textwidth]{./img/JFP_botta2017_1.pdf}
%\end{flushleft}
\end{minipage}
\hfill
\begin{minipage}[t]{0.49\textwidth}
%\begin{flushleft}
\vspace{0pt}
\includegraphics[clip, trim={2.0cm 6cm 2.0cm 3cm}, width=0.9\textwidth]{./img/JFP_brede2021_1hl.pdf}
%\end{flushleft}
\end{minipage}
%
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Correctness of monadic backward induction (2021)}
%
\begin{itemize}
\vfill

\item<1-> The JFP2017 theory entailed a \hl<1>{generic} \hl<1>{implementation} of
Bellman's 1957 principle

< Bellman  :  (Functor M, Preorder Val) => {t, n : Nat} ->
<
<             (ps   :  PolicySeq (S t) n) -> OptPolicySeq ps ->
<
<             (p    :  Policy t)          -> BestExt ps p ->
<
<             OptPolicySeq (p :: ps)

\vfill
\item<2-> \dots and (perhaps) the \hl<2>{first} \hl<2>{verified} implementation of
\hl<2>{backward} \hl<2>{induction}. But \dots

\vfill
\item<3-> \dots under which conditions is it \hl<3>{legitimate} to \hl<3>{apply} \hl<3>{backward} \hl<3>{induction}?

\vfill
\item<4-> The question is crucial: \hl<4->{IAM}s for climate policy are based on \hl<4->{SDP}s and \dots

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

%if False

> infixr 7 ##

> data StateCtrlSeq : (t : Nat) -> (n : Nat) -> Type where
>   Last  :  {t : Nat} -> (x : X t) -> StateCtrlSeq t (S Z)
>   (##)  :  {t, n : Nat} ->
>            Sigma (X t) (Y t) -> StateCtrlSeq (S t) (S n) -> StateCtrlSeq t (S (S n))

> head : {t, n : Nat} -> StateCtrlSeq t (S n) -> X t
> head (Last x)              =  x
> head (MkSigma x y ## xys)  =  x

> sumR : {t, n : Nat} -> StateCtrlSeq t n -> Val
> sumR {t} (Last x)              =  zero
> sumR {t} (MkSigma x y ## xys)  =  (reward t x y (head xys)) <+> (sumR xys)

> trj  :  {t, n : Nat} -> VeriMonad M =>
>         (ps : PolicySeq t n) -> (x : X t) -> M (StateCtrlSeq t (S n))
> trj {t}     Nil       x  =  pure (Last x)
> trj {t}  (  p :: ps)  x  =  let y = p x in
>                             let mx' = next t x y in
>                             map ((MkSigma x y) ##) (mx' >>= trj ps)

> val'  :  {t, n : Nat} -> VeriMonad M =>
>          (ps : PolicySeq t n) -> (x : X t) -> Val
> val' ps x = meas (map sumR (trj ps x))

%endif

\begin{frame}[fragile]
\frametitle{Correctness of monadic backward induction (2021)}
%
\begin{itemize}
\vfill
\item<1-> \dots \hl<1-2>{backward} \hl<1-2>{induction} yields policy sequences for SDPs that maximize \hl<1-2>{|val|}

< val : Functor M => {t, n : Nat} -> PolicySeq t n -> X t -> Val
<
< val {t}  Nil      x  =  zero
<
< val {t} (p :: ps) x  =  let y    =  p x in
<
<                         let mx'  =  next t x y in
<
<                         meas (map (reward t x y <++> val ps) mx')

\vfill
\item<2-> \dots but solving SDPs means computing policy sequences that
  maximize \hl<2>{|val'|}

< val'  :  {t, n : Nat} -> VeriMonad M => (ps : PolicySeq t n) -> (x : X t) -> Val
<
< val' ps x = meas (map sumR (trj ps x))

\vfill
\item<3-> Under which conditions are \hl<3->{|val|} and \hl<3->{|val'|} extensionally \hl<3->{equal}?

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\newcommand{\placediag}[1]{\hfill \(\vcenter{\hbox{#1}}\)}
\begin{frame}[fragile]
\frametitle{Correctness of monadic backward induction (2021)}
%
\begin{itemize}
  %
  \vfill
\item<1-> |meas . pure = id|:
   \placediag{\includegraphics[clip, width=0.25\textwidth]{img/diag1.eps}}
  %
  \vfill
\item<1-> |meas . join = meas . map meas|:
   \placediag{\includegraphics[clip, width=0.3\textwidth]{img/diag2.eps}}
  %
  \vfill
\item<1-> |meas . map (v ! <+>) = (v ! <+>) . meas|:
   \placediag{\includegraphics[clip, width=0.3\textwidth]{img/diag3.eps}}
   %
  \vfill
\item<2-> \hl<2>{Brede}, Potsdam University, 2021
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Applications (2018-2024)}
\pause
%
\begin{minipage}[t]{0.49\textwidth}
%\begin{flushleft}
\vspace{0pt}
\includegraphics[width=1.0\textwidth]{./img/ESD_botta2018_1hl.pdf}
%\end{flushleft}
\end{minipage}
\hfill
\begin{minipage}[t]{0.49\textwidth}
%\begin{flushleft}
\vspace{0pt}
\includegraphics[width=1.0\textwidth]{./img/EMA_botta2023_1hl.pdf}
%\end{flushleft}
\end{minipage}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Conclusions}
%
\begin{itemize}
  %
  \vfill
\item<2-> Principles of programming, type-driven analysis and
  dependently-typed formalizations can \hl<2>{help} \hl<2>{formulating}, \hl<2>{understanding}
  and \hl<2>{answering} important \hl<2>{questions} at the interface between climate
  science and climate policy
  %
  \vfill
\item<3-> Publishing in \hl<3>{computer} \hl<3>{science} journals was \hl<3>{challenging} but very \hl<3>{rewarding}
  %
  \vfill
\item<4-> Publishing in \hl<4>{natural} \hl<4>{science} journals was \hl<4>{frustrating} because of \hl<4>{typesetting} issues
  %
  \vfill
\item<5-> We have \hl<5>{failed} to convince climate scientists of the usefulness of ``thinking functionally''!
  %
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Conclusions (why have we failed?)}
%
\begin{minipage}[t]{0.68\textwidth}
\vspace{0pt}
\includegraphics[clip, trim={1.0cm 6cm 1.0cm 11cm}, width=1.1\textwidth]{./img/JFP_botta2024_1hl.pdf}
\end{minipage}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Conclusions (why have we failed?)}
%
\begin{minipage}[t]{0.68\textwidth}
\vspace{0pt}
\includegraphics[clip, trim={1.0cm 6cm 1.0cm 2cm}, width=1.1\textwidth]{./img/JFP_botta2024_1hl.pdf}
\end{minipage}
%
\end{frame}

%% -------------------------------------------------------------------

