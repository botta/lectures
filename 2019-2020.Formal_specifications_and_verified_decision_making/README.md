# Introduction to formal specifications and verified decision making

This repository contains teaching material for an introduction to formal
specifications and verified decision-making, using the computational
theory of policy advice and avoidability developed by Botta et
al.([1],[2]).


## Acknowledgement

The data in this repository is part of **Deliverable D6.1 (D27)** [4] of the **TiPES**  (Tipping Points in the Earth System) project funded by **European Union’s Horizon 2020** research and innovation programme under grant agreement No 820970.

## References

[1] Botta, N., Jansson, P., Ionescu, C. (2017). Contributions to a computational theory of policy advice and avoidability. J. Funct. Program., 27, e23.

[2] Botta, N., Jansson, P., Ionescu, C.(2018). The impact of uncertainty on optimal emission policies. Earth Syst. Dynam., 9, 525-542.

[3] Botta, N. et al.(2016-2021). IdrisLibs, https://gitlab.pik-potsdam.de/botta/IdrisLibs

[4] Botta, Nicola, Brede, Nuria, Crucifix, Michel, & Martı́nez Montero, Marina. (2020). Course on functional languages and dependently typed languages.
[Zenodo](https://doi.org/10.5281/zenodo.4543579).
