% -*-Latex-*-
\documentclass{article}

\input{setup.tex}

%include lhs.fmt
%format one = 1
%format `NonNegDouble.Predicates.LTE` = "\leq"
%format SequentialDecisionProblems.CoreTheory.State = X
%format SequentialDecisionProblems.CoreTheory.Ctrl = Y
%format SequentialDecisionProblems.CoreTheory.next = next
%format SequentialDecisionProblems.CoreTheory.reward = reward
%format SequentialDecisionProblems.CoreTheory.Val = Val
%format SequentialDecisionProblems.CoreTheory.plus = plus
%format SequentialDecisionProblems.CoreTheory.zero = zero
%format SequentialDecisionProblems.CoreTheory.LTE = "(\leq)"
%format SequentialDecisionProblems.FullTheory.reflexiveLTE = reflexiveLTE
%format SequentialDecisionProblems.FullTheory.transitiveLTE = transitiveLTE
%format SequentialDecisionProblems.FullTheory.monotonePlusLTE = monotonePlusLTE
%format SequentialDecisionProblems.CoreTheoryOptDefaults.totalPreorderLTE = totalPreorderLTE

%format SequentialDecisionProblems.CoreTheory.meas = meas
%format SequentialDecisionProblems.FullTheory.measMon = measMon
%format SequentialDecisionProblems.CoreTheory.Viable = Viable
%format SequentialDecisionProblems.CoreTheory.Reachable = Reachable
%format SequentialDecisionProblems.CoreTheory.viableSpec1 = viableSpec1
%format SequentialDecisionProblems.CoreTheory.NotEmpty = NotEmpty
%format SequentialDecisionProblems.CoreTheory.All = All
%format SequentialDecisionProblems.Utils.finiteViable = finiteViable
%format SequentialDecisionProblems.Utils.decidableViable = decidableViable
%format SequentialDecisionProblems.CoreTheory.reachableSpec1 = reachableSpec1
%format SequentialDecisionProblems.TabBackwardsInduction.decidableReachable = decidableReachable
%format SequentialDecisionProblems.Utils.finiteCtrl = finiteCtrl
%format SequentialDecisionProblems.TabBackwardsInduction.finiteState = finiteState
%format SequentialDecisionProblems.Utils.showState = showState
%format SequentialDecisionProblems.Utils.showCtrl = showCtrl


\input{macros.TeX}


\begin{document}

%include Lecture11.lidr

\pagebreak
\bibliographystyle{plain}
\bibliography{references}

\end{document}


