% -*-Latex-*-
\documentclass{article}

\input{setup.tex}
\usepackage{asymptote}
\usepackage{wrapfig}

%include lhs.fmt

\input{macros.TeX}


\begin{document}

%include Lecture8.lidr

% \bibliographystyle{plain}
% \bibliography{references}

\end{document}


