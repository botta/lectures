% -*-Latex-*-

%if False

> -- module Lecture7
> module Main

> import Syntax.PreorderReasoning

> import Sigma.Sigma

> %default total
> %auto_implicits off
> %access public export

> infixr 7 :::
> infixr 7 <++>
> infix 6 <=

> %hide head
> %hide lteRefl

%else

%endif

\setcounter{section}{6}


\stepcounter{section}
%\section{Deterministic sequential decision problems, naive theory}

We go back to the \emph{deterministic case} and first build a theory of
optimal decision making for deterministic sequential decision problems
(SDP). In lecture 9 we will then generalize the theory to \emph{monadic}
SDPs.


\subsection{States, controls and transition function}

At the core of an SDP we have a dynamical system with control as
discussed in lecture 5:

> X     :  (t : Nat) -> Type
> 
> Y     :  (t : Nat) -> (x : X t) -> Type
> 
> next  :  (t : Nat) -> (x : X t) -> (y : Y t x) -> X (S t)

Remember that |X t| represents the set of states a decision maker can
observe at decision step |t|. For a given state |x : X t|, |Y t x| are
the controls (options, choices, etc.) available to the decision maker in
|x|.

\textbf{Remark:} In order to specify an SDP, |X|, |Y| and |next| have to
be defined.

\textbf{Example:} In an emission problem like the one discussed in the first
lecture, |X t|\ represents the cumulated emissions, the current
emissions, the availability of technologies for reducing emissions and
the ``state of the world''.


\subsection{Reward functions}

SDPs can be formulated is by introducing a \emph{reward} function

> Val : Type
>
> reward : (t : Nat) -> (x : X t) -> (y : Y t x) -> (x' : X (S t)) -> Val

that associates a unique value to every transition. Specifically,
|reward t x y x'| represents the reward (payoff, etc.) that the decision
maker associates to a transition from |x| to |x'| when the control |y|
is selected.

\textbf{Remark:} In many SDPs, controls are associated with the
consumption of resources that might be scarce: money, fuel, common
goods, etc.

\textbf{Remark:} In shortest path and optimal routing problems, rewards
are often zero everywhere and one for values of |x'| corresponding to
the destination or goal.

Since the original work of Bellman \cite{bellman1957}, the above has
turned out to be a useful approach for formulating and solving SDPs. The
idea is that the decision maker seeks controls that maximize the sum of
the rewards obtained in a finite number of steps. This implies that
values of type |Val| have to be "addable"

> (<+>) : Val -> Val -> Val

Moreover, |Val| has to be equipped with a "zero"

> zero : Val

and with a binary "comparison" relation

> (<=) : Val -> Val -> Type

\textbf{Remark:} In many SDPs, |Val| is |Nat| or |Real| and |(<+>)| and
|zero| are the standard addition and its neutral element.


\subsection{Policies and policy sequences}

Policies are functions that associate to every state |x : X t| at
decision step |t| a control in |Y t x|:

> Policy : (t : Nat) -> Type
> 
> Policy t = (x : X t) -> Y t x

Policy sequences are literally sequences of policies:

> data PolicySeq : (t : Nat) -> (n : Nat) -> Type where
> 
>   Nil   :  {t : Nat} -> PolicySeq t Z
>   
>   (::)  :  {t, n : Nat} -> Policy t -> PolicySeq (S t) n -> PolicySeq t (S n)

The |Nil| data constructor warrants that we can construct an empty
policy sequence for every decision step; |(::)| warrants that with a
decision policy for step |t| and a policy sequence that supports |n|
decision steps starting from states in |X (S t)|, we can construct a
policy sequence that supports |S n| decision steps starting from states
in |X t|.

\begin{shaded}
  \begin{exercise}
    \label{exercise7.1}    
    Assume that |X t = R| and |Y t x = S| for all |t : Nat|, |x : X t =
    R|. Formalize the notions of policy and policy sequence for this
    special case.
  \end{exercise}
\end{shaded}
    

\subsection{The value of policy sequences}

Given a policy sequence for |n| decision steps, we can easily compute
the value of taking |n| decisions according to that sequence in terms of
the sum of the rewards obtained:

> val  :  {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Val
>   
> val {t}    Nil       x  =  zero
>   
> val {t} (  p :: ps)  x  =  let y = p x in
> 
>                            let x' = next t x y in
>                            
>                            reward t x y x' <+> val ps x'


\subsection{Optimality of policy sequences}

Remember that the decision maker seeks controls that maximize the sum of
the rewards obtained in a finite number of decision steps.

Because |val| exactly computes this sum for arbitrary policy sequences,
we can formalise what it means for one such sequence to be \emph{optimal}:

> OptPolicySeq  :  {t, n : Nat} -> PolicySeq t n -> Type
> 
> OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> (x : X t) -> val ps' x <= val ps x

\textbf{Remark:} This notion of optimality contains a quantification over 
all states |x : X t|. This implies that a policy sequence which is worse
(in terms of |val|) than another sequence in a particular state cannot
be optimal!


\subsection{Optimal extensions of policy sequences}

The computation of \emph{optimal extensions} of policy sequences is the key for
the computation of optimal policy sequences.

An extension of a policy sequence for making |m| decision steps starting
from states at decision step |S t| is a policy for taking decisions at
step |t|.

A policy |p| is an optimal extension of a policy sequence |ps| if there
is no better way than |p :: ps| to make |S m| decision steps starting
from step |t|:

> OptExt  :  {t, m : Nat} -> PolicySeq (S t) m -> Policy t -> Type
>
> OptExt {t} ps p  =  (p' : Policy t) -> (x : X t) -> val (p' :: ps) x <= val (p :: ps) x 

The idea behind the notion of optimal extension is that if |p| is an
optimal extension of |ps| and |ps| is optimal, then |p :: ps| is
optimal.  This is \emph{Bellman's principle of optimality}.


\subsection{Bellman's principle}

> Bellman  :  {t, m : Nat} -> 
> 
>             (ps  :  PolicySeq (S t) m)  ->   OptPolicySeq ps ->
>             
>             (p   :  Policy t)           ->   OptExt ps p ->
>             
>             OptPolicySeq (p :: ps)

If |<=| is reflexive and transitive and |<+>| is monotonic with
respect to |<=|,

> lteRefl  : {a : Val} -> a <= a

> lteTrans : {a, b, c : Val} -> a <= b -> b <= c -> a <= c

> plusMon  : {a, b, c, d : Val} -> a <= b -> c <= d -> (a <+> c) <= (b <+> d)

then proving Bellman's principle is straightforward:

> Bellman {t} ps ops p oep = opps where
> 
>   opps (p' :: ps') x = 
>   
>     let y'  =  p' x in
>     
>     let x'  =  next t x y' in
>     
>     let s1  =  plusMon lteRefl (ops ps' x') in  -- |val (p' :: ps') x <= val (p' :: ps) x|
>     
>     let s2  =  oep p' x in                      -- |val (p' :: ps)  x <= val (p :: ps)  x|
>     
>     lteTrans s1 s2


\subsection{Generic verified backwards induction}

From the reflexivity of |<=| it follows that empty policy sequences are
optimal:

> nilOptPolicySeq : OptPolicySeq Nil
> 
> nilOptPolicySeq Nil x = lteRefl

Thus, assuming that we have a method for computing optimal extensions of
arbitrary policy sequences:

> optExt  :  {t, n : Nat} -> PolicySeq (S t) n -> Policy t

> optExtSpec  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)

it is easy to implement a generic backwards induction for computing
optimal policies for arbitrary decision problems:

> bi : (t : Nat) -> (n : Nat) -> PolicySeq t n
> 
> bi t    Z     =  Nil
> 
> bi t (  S n)  =  let ps = bi (S t) n in optExt ps :: ps

and to prove that |bi| is correct:

> biLemma : (t : Nat) -> (n : Nat) -> OptPolicySeq (bi t n)
> 
> biLemma t    Z     =  nilOptPolicySeq
> 
> biLemma t (  S n)  =  let ps   =  bi (S t) n in
> 
>                       let ops  =  biLemma (S t) n in
>                     
>                       let p    =  optExt ps in
>                     
>                       let oep  =  optExtSpec ps in
>                     
>                       Bellman ps ops p oep


\subsection{Naive theory, wrap up}

The theory is applied in three steps:

\begin{itemize}

  \item First, specify a concrete SDP by implementing |X|, |Y|, |next|,
  |Val|, |reward|, |<+>|, |zero|, |<=| and |optExt|.

  \item Then, apply |bi t n| and compute an optimal
  policy sequence |[p_t .. p_tpnm1]| for |n > 0| decision steps starting from
  step |t|.

  \item For an initial observation |x_t : X t|, compute the |n| optimal
  controls:

< y_t      =  p_t      x_t,      x_tp1  =  next    t           x_t      y_t   
<
< y_tp1    =  p_tp1    x_tp1,    x_tp2  =  next (  t + 1)      x_tp1    y_tp1  
<
< ...
<
< y_tpnm1  =  p_tpnm1  x_tpnm1,  x_tpn  =  next (  t + n - 1)  x_tpnm1  y_tpnm1   

  \item Bonus: If |<=| is reflexive and transitive, |<+>| is
  monotonic with respect to |<=| and |optExt| fulfills |optExtSpec| then
  |y_t, y_tp1 .. y_tpnm1| are verified optimal decisions.

\end{itemize}


\subsection{The bad news}

The naive theory is simple and straightforward but has a major flaw.

What if |Y t x_t| is empty? In this case, we cannot compute a |y_t : Y t
x_t| and thus a next state! There is so far nothing in our formulation
that prevents the set of controls associated with a certain state to be
empty.

Conversely, there is nothing in the computation of optimal policies that
prevents a policy to select a control that through |next| leads to a
state whose set of controls is empty.

As a result, we might not be able to ``solve'' even very simple decision
problems. This is made evident in the following example. Let 

> head : {t, n : Nat} -> PolicySeq t (S n) -> Policy t
> 
> head (p :: ps) = p 

> tail : {t, n : Nat} -> PolicySeq t (S n) -> PolicySeq (S t) n
> 
> tail (p :: ps) = ps 

and 

> data GoodOrBad  =  Good  |  Bad

> implementation Show GoodOrBad where
> 
>   show  Good  =  "Good"
> 
>   show   Bad  =   "Bad"

> data UpOrDown   =  Up    |  Down

Consider the decision problem

> X t  =  GoodOrBad

> Y t  Good  =  UpOrDown 
> 
> Y t   Bad  =  Void

In |Good| there are two options: |Up| and |Down|. But in |Bad| there are
no controls to select. We can define a transition function

> next t  Good  Up    =  Good
>
> next t  Good  Down  =  Bad
>   
> next t  Bad   v     impossible

but we will not be able to apply |next| for the |Bad| case unless we
manage to construct a value |v : Void|. This is impossible. Still, we
can complete the specification of the problem:

> Val = Nat
>
> (<+>) = (+)
>
> zero = Z
>
> (<=) = Prelude.Nat.LTE
>
> reward t Good    Up x'  =  1
> 
> reward t Good  Down x'  =  3
> 
> reward t  Bad     v x'  impossible

Notice that, again, we will not be able to compute an argument |v :
Void| to apply |reward|. This also implies that we cannot give a
complete implementation of |optExt|. We can compute a best control for
|Good|:

> optExt {t} ps Good  =  
> 
>   let x'Up     =  next t Good  Up in
>   
>   let x'Down   =  next t Good  Down in
>       
>   let valUp    =  reward t Good Up    x'Up     <+>  val ps x'Up in
>   
>   let valDown  =  reward t Good Down  x'Down   <+>  val ps x'Down in
>                         
>   if valUp >= valDown then Up else Down

But not for |Bad|:

> optExt {t} ps Bad  =  ?whatNow

In spite of this, we can try to compute a policy sequence for two
decision steps and see what happens:

> computation : IO ()
> 
> computation  =  let ps  =  bi 0 2 in
> 
>                 let x0  =  Good in
>                 
>                 let p0  =  head ps in
>                 
>                 let y0  =  p0 x0 in
>                 
>                 let x1  =  next Z x0 y0 in
>                 
>                 let p1  =  head (tail ps) in
>                 
>                 let y1  =  p1 x1 in
>                 
>                 let x2  =  next 1 x1 y1 in
> 
>                 do  putStrLn ("x0 = " ++ show x0)
>      
>                     putStrLn ("x1 = " ++ show x1)
>      
>                     putStrLn ("x2 = " ++ show x2)

> main : IO ()
> 
> main = computation

\begin{shaded}
  \begin{exercise}
    \label{exercise7.2}  
    Do you expect this program to terminate? If so, what do you expect
    to be the result of the computation?
  \end{exercise}
\end{shaded}


\subsection{Wrap-up, outlook}

\begin{itemize}

  \item If the control space for one or more states is empty, the theory
  becomes problematic. We can proceed in two ways:

  \item Require |Y t x| to be non-empty for all |t : Nat|, |x : X t|.

  \item Accept that |Y t x| might be empty and be more careful in the
  definition of the domain and of the codomain of policies.

  \item The second approach is the one adopted in
    \cite{2014_Botta_et_al}, \cite{2017_Botta_Jansson_Ionescu} and
    \cite{botta20162020}. We discuss it in the next lecture.

\end{itemize}


\section*{Additional remarks: trajectories and consistency of |val|}

We want to show that |val ps x| does indeed compute the sum of the
rewards obtained along the trajectory that is obtained under the policy
sequence |ps| when starting in |x|. To this end, we start by defining
sequences of state-control pairs

> data StateCtrlSeq : (t : Nat) -> (n : Nat) -> Type where
> 
>   Last   :  {t : Nat} -> (x : X t) -> StateCtrlSeq t (S Z)           
>   
>   (:::)  :  {t, n : Nat} -> Sigma (X t) (Y t) -> StateCtrlSeq (S t) n -> StateCtrlSeq t (S n)

and a function |sumR| that computes the sum of the rewards of a
state-control sequence:

> head' : {t, n : Nat} -> StateCtrlSeq t (S n) -> X t
> 
> head' (Last x)              =  x
> 
> head' (MkSigma x y ::: xys)  =  x  

> sumR : {t, n : Nat} -> StateCtrlSeq t n -> Val
> 
> sumR {t} {n = S    Z}     (Last x)               =  zero
> 
> sumR {t} {n = S (  S m)}  (MkSigma x y ::: xys)  =  reward t x y (head' xys) <+> sumR xys

Next, we implement a function that computes the trajectory that is
obtained under a policy sequence |ps| when starting in |x|:

> trj : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> StateCtrlSeq t (S n)
> 
> trj {t}  Nil      x  =  Last x
> 
> trj {t} (p :: ps) x  =  let y   =  p x in
> 
>                         let x'  =  next t x y in                            
>                         
>                         (MkSigma x y) ::: trj ps x'

Finally, we compute the measure of the sum of the rewards obtained along
the trajectory that is obtained under the policy sequence |ps| when
starting in |x|

> val' : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> Val
> 
> val' ps x = sumR (trj ps x)

Now we can formulate the property that |val ps| does indeed compute the
measure of the sum of the rewards obtained along the trajectories
obtained under the policy sequence |ps|:

< valVal'Th  :  {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> val ps x = val' ps x 

With 

> head'Lemma  :  {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> head' (trj ps x) = x
>               
> head'Lemma    Nil       x  =  Refl
> 
> head'Lemma (  p :: ps)  x  =  Refl

we can prove the |val|-|val'| theorem by induction on |ps|:

> valVal'Th  :  {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> val ps x = val' ps x 
>               
> valVal'Th        Nil      x  =  Refl
> 
> valVal'Th {t} (  p :: ps) x  = 
> 
>   let y = p x in
>   
>   let x' = next t x y in
>   
>       ( val (p :: ps) x )
>     
>   ={  Refl }=
>   
>       ( reward t x y x' <+> val ps x' )
>     
>   ={  cong (valVal'Th ps x') }=
>   
>       ( reward t x y x' <+> val' ps x' )
>     
>   ={  cong {f = \ α => reward t x y α <+> val' ps x'} (sym (head'Lemma ps x')) }=
>   
>       ( reward t x y (head' (trj ps x')) <+> val' ps x' )
>     
>   ={  Refl }=
>   
>       ( sumR ((MkSigma x y) ::: trj ps x') )
>     
>   ={  Refl }=
>   
>       ( val' (p :: ps) x )
>     
>   QED


\section*{Solutions}


\subsubsection*{Exercise \ref{exercise7.1}:}

< Policy : Type
< 
< Policy = R -> S

< PolicySeq : Nat -> Type
< 
< PolicySeq n = Vect n Policy


% \subsubsection*{Exercise \ref{exercise7.2}:}

