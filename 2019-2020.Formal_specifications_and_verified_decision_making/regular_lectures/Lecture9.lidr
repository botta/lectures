% -*-Latex-*-

%if False

> module Lecture9

> import Syntax.PreorderReasoning
> import Data.Vect
> import Control.Monad.Identity

> import Sigma.Sigma
> import Sigma.Operations

> %default total
> %auto_implicits off
> %access public export

%endif

\setcounter{section}{8}

\stepcounter{section}
%\section{Generic optimal extensions, viability and reachability decision procedures}

\subsection{Wrap up lecture 8, part 1}

In lecture 8 we have demonstrated that if we specify a SDP in terms of 

> X         :  (t : Nat) -> Type
> 
> Y         :  (t : Nat) -> (x : X t) -> Type
> 
> next      :  (t : Nat) -> (x : X t) -> (y : Y t x) -> X (S t)
>
> Val       :  Type
>
> reward    :  (t : Nat) -> (x : X t) -> (y : Y t x) -> (x' : X (S t)) -> Val
> 
> (<+>)     :  Val -> Val -> Val
>
> zero      :  Val
>
> (<=)      :  Val -> Val -> Type
 
such that

> lteRefl   :  {a : Val} -> a <= a
>
> lteTrans  :  {a, b, c : Val} -> a <= b -> b <= c -> a <= c
>
> plusMon   :  {a, b, c, d : Val} -> a <= b -> c <= d -> (a <+> c) <= (b <+> d)

and if we can define

> Viable     :  {t : Nat} -> (n : Nat) -> X t -> Type

> Reachable  :  {t' : Nat} -> X t' -> Type

such that

> viableSpec0 : {t : Nat} -> (x : X t) -> Viable Z x

> viableSpec1  :  {t : Nat} -> {n : Nat} -> (x : X t) -> 
> 
>                 Viable (S n) x -> Exists (\ y => Viable n (next t x y))

> viableSpec2  :  {t : Nat} -> {n : Nat} -> (x : X t) -> 
> 
>                 Exists (\ y => Viable n (next t x y)) -> Viable (S n) x

and

> reachableSpec0  :  (x : X Z) -> Reachable x

> reachableSpec1  :  {t : Nat} -> (x : X t) -> Reachable x -> (y : Y t x) -> Reachable (next t x y)


> Pred : {t : Nat} -> X t -> X (S t) -> Type
> 
> Pred {t} x x'  =  Exists (\ y => x' = next t x y)

> ReachablePred : {t : Nat} -> X t -> X (S t) -> Type
> 
> ReachablePred x x'  = (Reachable x, x `Pred` x')

> reachableSpec2  :  {t : Nat} -> (x' : X (S t)) -> Reachable x' -> Exists (\ x => x `ReachablePred` x')

hold, then, if we can implement a function that computes optimal
extensions of arbitrary policy sequences

< optExt  :  {t, n : Nat} -> PolicySeq (S t) n -> Policy t (S n)

< optExtSpec  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)

we can implement a generic backwards induction

< bi : (t : Nat) -> (n : Nat) -> PolicySeq t n

that is correct by construction, i.e.\ for which we can prove

< biLemma : (t : Nat) -> (n : Nat) -> OptPolicySeq (bi t n)

We have derived this result in a context. This is given by the notions:

> GoodY : (t : Nat) -> (x : X t) -> (m : Nat) -> Type
> 
> GoodY t x m = Sigma (Y t x) (\ y => Viable m (next t x y))

> Policy : (t : Nat) -> (n : Nat) -> Type
> 
> Policy t    Z     =  Unit
> 
> Policy t (  S m)  =  (x : X t) -> Reachable x -> Viable (S m) x -> GoodY t x m

> data PolicySeq : (t : Nat) -> (n : Nat) -> Type where
> 
>   Nil   :  {t : Nat} -> PolicySeq t Z
>   
>   (::)  :  {t, n : Nat} -> Policy t (S n) -> PolicySeq (S t) n -> PolicySeq t (S n)

< val  :  {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Reachable x -> Viable n x -> Val
   
< val {t}    Nil       x rx vx  =  zero
<
< val {t} (  p :: ps)  x rx vx  =  let gy   =  p x rx vx in
< 
<                                  let y    =  outl gy in
<                                  
<                                  let x'   =  next t x y in
<                                  
<                                  let rx'  =  reachableSpec1 x rx y in  -- ?hole1 
<                                  
<                                  let vx'  =  outr gy in                -- ?hole2
<                                  
<                                  reward t x y x' <+> val ps x' rx' vx'

< OptPolicySeq  :  {t, n : Nat} -> PolicySeq t n -> Type
< 
< OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> 
< 
<                             (x : X t) -> (rx : Reachable x) -> (vx : Viable n x) -> 
< 
<                             val ps' x rx vx <= val ps x rx vx

and

< OptExt  :  {t, m : Nat} -> PolicySeq (S t) m -> Policy t (S m) -> Type
<
< OptExt {t} {m} ps p  =  (p' : Policy t (S m)) -> 
< 
<                         (x : X t) -> (rx : Reachable x) -> (vx : Viable (S m) x) -> 
< 
<                         val (p' :: ps) x rx vx <= val (p :: ps) x rx vx


\subsection{Wrap up lecture 8, part 2}

In lecture 8, we have also shown that it is not difficult to give
generic and correct implementations of |Viable| and |Reachable|:

> Viable {t}    Z     x = Unit
> 
> Viable {t} (  S n)  x = Exists (\ y => Viable n (next t x y))

> viableSpec0 {t} x = ()

> viableSpec1 {t} {n} x (Evidence y gy)  =  Evidence y gy

> viableSpec2 {t} {n} x (Evidence y gy)  =  Evidence y gy

> Reachable {t' = Z}    x'  =  Unit 
>
> Reachable {t' = S t}  x'  =  Exists (\ x => ReachablePred x x')

> reachableSpec0 x = ()

> reachableSpec1 x rx y = Evidence x (rx, Evidence y Refl)

> reachableSpec2 {t} x' rx' = rx'

However, we have implemented |optExt| only for a specific (and quite
simple) SDP and we have argued that showing that this implementation is
correct (by implementing |optExtSpec|) would not be trivial.

The first objective of this lecture is to derive a generic, correct
implementation of |optExt|.

To this end, we start by reminding ourselves of what it means to compute
optimal extensions of policy sequences.


\subsection{Generic, correct optimal extensions}

Consider again the specification

< optExt  :  {t, n : Nat} -> PolicySeq (S t) n -> Policy t (S n)

< optExtSpec  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)

The signature of |optExt| tells us that, for arbitrary |t, n : Nat| and
|ps : PolicySeq (S t) n|, |p = optExt ps| has to be a policy for
selecting controls at decision step |t| and that the controls selected
by |p| have to support |n| further decision steps. Thus, because of the
definition of |Policy|

< Policy : (t : Nat) -> (n : Nat) -> Type
< 
< Policy t    Z     =  Unit
< 
< Policy t (  S m)  =  (x : X t) -> Reachable x -> Viable (S m) x -> GoodY t x m

we also know that |p| has to associate a \emph{good} control to every state
|x : X t| which is reachable and viable for |n + 1| steps. This is a control |y
: Y t x| paired with a proof that |next t x y| is viable |n| steps:

< GoodY : (t : Nat) -> (x : X t) -> (m : Nat) -> Type
< 
< GoodY t x m = Sigma (Y t x) (\ y => Viable m (next t x y))

\begin{shaded}
  \begin{exercise}
    \label{exercise9.1}    
    We know for sure that at least one such control exists. Do you see
    why?
  \end{exercise}
\end{shaded}
    
Thus, if |Y t x| happens to contain only one control (|Y t x| is a
singleton type), we can define

< p x r v = gy

where, neglecting the differences between |Exists| and |Sigma|, |gy| is
just |viableSpec1 x v| ! What if |Y t x| contains more than one control?

In this case, we truly have to make a choice. The second part of the
specification of |optExt|

< optExtSpec  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)

tells us that |p = optExt ps| has to be an optimal extension of
|ps|. The definitions of |OptExt|

< OptExt  :  {t, m : Nat} -> PolicySeq (S t) m -> Policy t (S m) -> Type
<
< OptExt {t} {m} ps p  =  (p' : Policy t (S m)) -> 
< 
<                         (x : X t) -> (rx : Reachable x) -> (vx : Viable (S m) x) -> 
< 
<                         val (p' :: ps) x rx vx <= val (p :: ps) x rx vx

and of |val|

< val  :  {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Reachable x -> Viable n x -> Val
<   
< val {t}    Nil       x rx vx  =  zero
<
< val {t} (  p :: ps)  x rx vx  =  let gy   =  p x rx vx in
< 
<                                  let y    =  outl gy in
<                                  
<                                  let x'   =  next t x y in
<                                  
<                                  let rx'  =  reachableSpec1 x rx y in
<                                  
<                                  let vx'  =  outr gy in
<                                  
<                                  reward t x y x' <+> val ps x' rx' vx'

suggest that |p x r v| has to be \emph{a} pair consisting of a control
|y : Y t x| and of a proof that |x' = next t x y| is viable |m| steps
such that 

\textbf{Condition:} |y| maximises the sum of the current reward, |reward
t x y x'|, and of the value |val ps x' rx' vx'| of taking |m| further
decision steps with |ps|.

We can see that this intuition is correct in three steps. First, we
rewrite |val| in terms of a helper function |cval|:

> mutual
>
>
>   cval  :  {t, m : Nat} -> PolicySeq (S t) m -> 
>   
>            (x  : X t) -> Reachable x -> Viable (S m) x -> GoodY t x m -> Val
>            
>   cval {t} ps x rx vx gy  =  let y    =  outl gy in
>   
>                              let x'   =  next t x y in
>                                  
>                              let rx'  =  reachableSpec1 x rx y in
>                                  
>                              let vx'  =  outr gy in                             
>   
>                              reward t x y x' <+> val ps x' rx' vx'
>
>   val  :  {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Reachable x -> Viable n x -> Val
>   
>   val {t}    Nil       x rx vx  =  zero
>
>   val {t} (  p :: ps)  x rx vx  =  let gy = p x rx vx in
>   
>                                    cval ps x rx vx gy

The interpretation of |cval ps x rx vx gy| is clear: it is the value (as
always, in terms of sum of rewards) of selecting the (good) control |gy|
at decision step |t| and then making |m| further decision steps
according to the policy sequence |ps|. 

Second, we assume that we can implement functions

> cvalargmax  :  {t, n : Nat} -> PolicySeq (S t) n -> 
> 
>                (x  : X t) -> Reachable x -> Viable (S n) x -> GoodY t x n

> cvalmax  :  {t, n : Nat} -> PolicySeq (S t) n -> 
> 
>             (x : X t) -> Reachable x -> Viable (S n) x -> Val

that fulfills the specification

> cvalargmaxSpec  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) ->
> 
>                    (x  : X t) -> (rx  : Reachable x) -> (vx  : Viable (S n) x) ->  
>                    
>                    cvalmax ps x rx vx = cval ps x rx vx (cvalargmax ps x rx vx)

> cvalmaxSpec  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) ->
> 
>                 (x  : X t) -> (rx  : Reachable x) -> (vx  : Viable (S n) x) ->  
>                 
>                 (y : GoodY t x n) -> (cval ps x rx vx y) <= (cvalmax ps x rx vx)

In other words, |cvalargmax| computes \emph{a} good control that
maximizes |cval|. The first specification ensures that |cvalmax| is
indeed the value of |cval| for that control. The second specification
ensures that no value of |cval| is better than |cvalmax|.

Third, we show that under these assumption we can derive a correct,
generic implementation of |optExt| that is, an imlementation that
fulfils

< optExt  :  {t, n : Nat} -> PolicySeq (S t) n -> Policy t (S n)

< optExtSpec  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)

with

> OptExt  :  {t, m : Nat} -> PolicySeq (S t) m -> Policy t (S m) -> Type
>
> OptExt {t} {m} ps p  =  (p' : Policy t (S m)) -> 
> 
>                         (x : X t) -> (rx : Reachable x) -> (vx : Viable (S m) x) -> 
> 
>                         val (p' :: ps) x rx vx <= val (p :: ps) x rx vx

This is done as follows:

> optExt  :  {t, n : Nat} -> PolicySeq (S t) n -> Policy t (S n)
> 
> optExt {t} {n} ps  =  p where
> 
>   p          :  Policy t (S n)
>   
>   p x rx vx  =  cvalargmax ps x rx vx

> optExtLemma  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)
> 
> optExtLemma {t} {n} ps p' x rx vx = s4 where
> 
>   p     :  Policy t (S n)
>   
>   p     =  optExt ps
>   
>   gy    :  GoodY t x n
>   
>   gy    =  p x rx vx
>   
>   y     :  Y t x
>   
>   y     =  outl gy
>   
>   gy'   :  GoodY t x n
>   
>   gy'   =  p' x rx vx
>   
>   y'    :  Y t x
>   
>   y'    =  outl gy'
>   
>   s1    :  cval ps x rx vx gy' <= cvalmax ps x rx vx
>   
>   s1    =  cvalmaxSpec ps x rx vx gy'
>   
>   s2    :  cval ps x rx vx gy' <= cval ps x rx vx (cvalargmax ps x rx vx)
>   
>   s2    =  replace {P = \ z => (cval ps x rx vx gy' <= z)} (cvalargmaxSpec ps x rx vx) s1
>   
>   -- the next steps are for the (sort of) human reader
>   
>   s3    :  cval ps x rx vx gy' <= cval ps x rx vx gy
>   
>   s3    =  s2
>   
>   s4    :  val (p' :: ps) x rx vx <= val (p :: ps) x rx vx
>   
>   s4    =  s3

This completes the derivation of a generic, correct implementation of
|optExt| but raises one important question. 

\textbf{Question:} Can we compute good controls that maximize |cval|
that is, provide correct implementations of |cvalargmax| and |cvalmax|?
Under which conditions?

\begin{shaded}
  \begin{exercise}
    \label{exercise9.2}    
    Answer the above questions. What does it mean for |cvalargmax| and
    |cvalmax| to be correct? Could one establish the correctness of a
    given implementation by means of tests?
  \end{exercise}
\end{shaded}

There is no provably correct generic method for solving arbitrary
optimization problems.

But it is easy to find a best (good) control for a given (reachable and
viable) |x : X t| when set of controls |Y t x| is \emph{finite}.

The case in which a decision maker has to select one of a finite set of
options is particularly important in practice.

Formalizing the notion of finiteness for a type and deriving correct
implementations of |cvalargmax| and |cvalmax| for the finite case would
go beyond the scope of these lectures.

But IdrisLibs \cite{botta20162020} provides default implementations for
this important case. To take advantage of these implementations,
practitioners only need to provide a proof that |Y t x| is finite for
every |x : X t| at every decision step |t|.


\subsection{Viability and reachability decision procedures}

In the example discussed at the end of lecture 8, we have computed two
decision steps for a simple SDP. The computation entailed, among others,
steps like

< computation  =  let ps   =  bi 0 2 in
< 
<                 let x0   =  Good in
<                 
<                 let rx0  =  () in
<                 
<                 let vx0  =  Evidence Up (Evidence Up ()) in
<                 
<                 ...
<                 
<                 do  putStrLn ("x0 = " ++ show x0)
<      
<                     putStrLn ("x1 = " ++ show x1)
<      
<                     putStrLn ("x2 = " ++ show x2)

In these steps, we have taken advantage of our understanding of the
specific problem (and of the fact that the problem is very simple) to
compute an evidence |vx0| that the initial state |x0 = Good| is viable
for two decision steps.

In the computation, a proof that |x0| is viable for two decision steps
is mandatory to apply the first policy of |ps| to |x0| and thus compute
an optimal control for the first decision step.

In general and for more realistic problems, proving the viability of an
initial state for a sufficiently large number of decision steps might be
difficult or simply impossible.

In the specific example, we would not have been able to construct any
evidence of viability for more than zero decision steps if we had chosen
|x0 = Bad|.

In realistic applications (for instance, in a tool that supports the
computation of optimal policies during negotiations on matter of GHG
emissions) a decision maker might want to compute optimal policies for
different initial states.

These observations suggest that, in order to apply the theory to
realistic SDPs, it would be useful to have decision procedures for
|Viable| and |Reachable|.

\textbf{Question:} What is a decision procedure? 

A decision procedure for a property |P : A -> Type| of values of type
|A| is a function that associates to every |a : A| either a value of
type |P a| or a value of type |Not (P a)|.

A property |P : A -> Type| which has a decision procedure is called
\emph{decidable}. The Idris prelude defines a data type

< data Dec : Type -> Type where
<
<   Yes  :  (prf : prop) -> Dec prop
<
<   No   :  (contra : prop -> Void) -> Dec prop

to characterize decidable properties. If |P : Type| is decidable, we can
easily implement a decision procedure for any value of type |P|:

> dec : {P : Type} -> Dec P -> Either P (Not P)
> 
> dec (Yes prf) = Left prf
> 
> dec (No  contra) = Right contra

We can use |Dec| to formalize the notion of decidability for viability:

> decidableViable : {t : Nat} -> (n : Nat) -> (x : X t) -> Dec (Viable n x)

and take advantage of |decidableViable| to implement a viability test
inside |computation|:

< computation = 
< 
<   do x0 <- pure Good
<   
<      putStrLn ("x0 = " ++ show x0)
<   
<      case (decidableViable {t = Z} 2 x0) of
<   
<        (Yes prf)     =>  let ps   =  bi 0 2 in
<                      
<                          let rx0  =  () in
< 
<                          let vx0  =  prf in
<                    
<                          ...
<                      
<                          do  putStrLn ("x0 = " ++ show x0)
<      
<                              putStrLn ("x1 = " ++ show x1)
<      
<                              putStrLn ("x2 = " ++ show x2)
<        
<        (No  contra)  =>  do putStrLn ("x0 not viable for 2 decision steps") 

A viability test is a necessary condition for computing optimal policy
sequences of a length |n| for initial states that are selected at run
time and that may or may not be actually viable for |n| steps.


\subsection{Wrap-up, outlook}

\begin{itemize}

  \item We have obtained a \emph{generic} implementation of |optExt|.

  \item We have seen how to take advantage of viability decision
  procedures for run-time tests.
    
  \item In the next lecture we will extend the theory to \emph{monadic}
  SDPs.

\end{itemize}


\section*{Solutions}


\subsubsection*{Exercise \ref{exercise9.1}:}

It is because of 

< viableSpec1  :  {t : Nat} -> {n : Nat} -> (x : X t) -> 
< 
<                 Viable (S n) x -> Exists (\ y => Viable n (next t x y))

Since

< Policy t (S n)  =  (x : X t) -> Reachable x -> Viable (S n) x -> GoodY t x n

we know that |x| is viable |S n| steps (we have a value of type |Viable
(S n) x|) every time we have to compute a good control for that |x|.


% \subsubsection*{Exercise \ref{exercise9.2}:}


