% -*-Latex-*-

%if False

> module Lecture3

> %default total
> %auto_implicits on
> %access public export

< %hide Nat
< %hide List

%endif

\setcounter{section}{2}

\stepcounter{section}
%\section{A crash-course in functional programming}
\label{FP-crash-course}

\paragraph{Objectives of this lecture}
\begin{itemize}
\item Get acquainted with the basic usage of functions, function
  composition, and higher order functions in functional programming 
\item Learn about primitive and inductive data types in Idris
\item Get to know two forms of polymorphism and learn how one of them
  is related to generic programming
\item Learn about the |List| and |Vector| data types
\end{itemize}


\subsection{Imperative and functional languages}

Traditionally, computer scientists use(d) to distinguish a number of
different programming \emph{paradigms} (e.g. precedural,
object-oriented, functional or declarative). As of today these
distinctions have become somewhat blurred, with modern programming
languages often integrating features from different paradigms.

However, it still seems valid to contrast against each other an
\emph{imperative} and a \emph{functional way of thinking} about the
programs. 

Climate scientists and modelers are often well acquainted with
\emph{imperative} programming languages/style of programming. \\ 

Very roughly, imperative programming is a method of specifying
what a computing machine shall do in terms of \emph{instructions} and
\emph{execution procedures}. \\

In functional programming, one specifies what a computing machine shall
do in terms of \emph{functions} and their \emph{application} and
\emph{composition}, with an emphasis on inductive definitions and
algebraic structure.

In this lecture we are going to learn some basics
of FP using Idris \cite{idristutorial} as a language. \\

Idris is a strongly typed functional programming language. A prototype
implementation appeared in 2008, the current implementation began in
2011. Thus, Idris is a relatively young language. However, its roots
are much older and in fact reach back to the quest for a logical
foundation of mathematics in the beginning of the 20th
century. ($\rightsquigarrow$ L4E1 for historical background) 


\subsection{Expressions and their types}

At the core of all programming languages is a sublanguage of
\emph{expressions} like

< 1+2
<
< "Hello"
<
< [1,7,3,8]
<
< \x => 2 * x + 1

In functional languages this core is expressive enough to implement
almost all programs you may want to write. \\

In strongly typed languages like Idris each \emph{valid} expression
has a \emph{type}. The judgment |e : t| states that the expression |e|
has type |t|.

< 1+2          :  Nat
<
< "Hello"      :  String
<
< [1,7,3,8]    :  List Nat
<
< \x => 2 * x  :  Integer -> Integer

Most of the power of Idris comes from its type-checker which can check
these judgments for very complex expressions |e| and types |t|.

\paragraph{Remark:} Note that the above ``code'' in the pdf-document
is ``prettified'' by preprocessing and slightly differs from the
actual Idris code. E.g.
\begin{verbatim}
   \x => t 
\end{verbatim}
is printed as

< λ x ⇒ t

instead.


\subsection{Function application and currying}

In Idris (and several other functional languages like Haskell and Agda)
the notation for function application is juxtaposition. Thus,

< f x

instead of

< f(x)

denotes the application of the function |f| to the argument |x|. Apart
from this notational difference, parantheses in Idris play the same
role as in mathematics: enclosing sub-expressions to resolve operator
precedence. \\ 

A function of |n > 1| arguments in mathematics is usually 
considered as a function taking one $n-$tuple as argument. In Idris,
we often use nested function application

< (g x) y

instead of

< g (x,y)

That is, functions ``of |n| arguments'' take one argument at a time,
resulting in a function ``of |n-1| arguments'' which in turn is
applied to the next input. (Both ways of looking at functions have
their merit, though, and we will come back to the relation between
the two.)

As an aside, |(g x) y| can also be written |g x y| because function
application is left-associative.

\begin{shaded}
  \begin{exercise}
    \label{exercise3.1}    
    What is the type of |g|?
  \end{exercise}
\end{shaded}

Here |a|, |b| and |c| are arbitrary types. Examples of functions that
take two arguments are infix operators like |(+)|. Infix operators can
be written between their first and second arguments:

< (+) 1 2 = 1 + 2


\subsection{Function composition and higher order functions}

Function composition is another example of an infix operator. In Idris
(but also in Haskell, Agda and plain mathematics), function composition
is denoted by a dot:

< (.) : (b -> c) -> (a -> b) -> a -> c
< (.) g f x = g (f x)

Function composition is an example of a \emph{higher order} function. It
takes two functions and returns their composite. \\

Higher order functions take one or more functions as arguments and/or
return a function. Every function of two or more arguments can be
partially applied and is thus a higher order function. Thus, if

< g : a -> (b -> c) = a -> b -> c

then
 
< g x : b -> c

We can easily turn |g| into an equivalent function |g'| that takes as
arguments pairs

< g' : (a, b) -> c
< g' (x, y) = g x y

In Idris (an most functional languages) two high order functions convert
between the two forms:

< uncurry : (a -> b -> c) -> (a, b) -> c

< curry : ((a, b) -> c) -> a -> b -> c

\paragraph{Remark:} Unfortunately, in Idris (as in Haskell), the
product type constructor and the product term constructor are both
denoted by |(·,·)|. So in the above, |(a,b)| is the product type
which could be thought of as cartesian product |a × b|, while |(x,y)|
is a pair of values, where |x| is of type |a| and |y| of type |b|.  

\begin{shaded}
  \begin{exercise}
    \label{exercise3.2}      
    Define |uncurry| and |curry|.
  \end{exercise}
\end{shaded}
  
\begin{shaded}
  \begin{exercise}
    \label{exercise3.3}          
    Show with equational reasoning (as introduced in lecture 2) that
    |uncurry . curry = curry . uncurry = id|.
  \end{exercise}
\end{shaded}
  


If |g' : (a, b) -> c|, |curry g' : a -> b -> c| is called the
\emph{curried} form of |g'|. Similarly |uncurry g : (a, b) -> c| is
called the \emph{uncurried} form of |g : a -> b -> c| .
\footnote{The names stem from Haskell Brooks Curry (1900-1982) but the idea that
every function of more than one variable can be understood as a (higher
order) of only one variable was apparently first used by Frege
\cite{QuineVanHeijenoort} and 
studied further by Schönfinkel \cite{schoenfinkel1924}. But Curry's
combinatory logic \cite{curryFeys} ws more influential with respect to functional
programming which might explain why Reynold's chose this name in his
influential 1972 paper \cite{Reynolds1998} which then was adopted by the
community.} 


\subsection{Polymorphism}

Some functions can be used for more than one type of data -- they are
\emph{polymorphic}. However, in Idris and similar languages, one
distinguishes between two conceptually different forms of
polymorphism.


\subsubsection{Parametric polymorphism and generic programs}

The notion of \emph{parametric polymorphims} relates to functions
whose defiition does not depend on the structure of the underlying
datatypes. (In category theoretical terms, they can be seen as
\emph{natural transformations} \cite{DBLP:conf/fpca/Wadler89},
$\rightsquigarrow$ see LE32)  

Function composition, |curry| and |uncurry| are examples of this kind
of \emph{polymorphic} function. The symbols |a|, |b| and |c| in

< (.) : (b -> c) -> (a -> b) -> a -> c

denote \emph{implicit type variables}. The judgment is in fact an
abbreviation of

< (.) : {a, b, c : Type} -> (b -> c) -> (a -> b) -> a -> c

which is itself an abbreviation of

< (.)  :  {a : Type} -> {b : Type} -> {c : Type} ->
<         (b -> c) -> (a -> b) -> a -> c

We say that |(.)| is a \emph{generic} program. It computes the
composition |f . g| of any two functions |f| and |g| as long as the
domain of |f| coincides with the |codomain| of |g|. \\

A simpler example of polymorphic functions are the projection functions
for pairs:

< fst : (s, t)  ->  s -- Remark: |s × t -> s|
< fst   (x, y)  =   x

> snd : (s, t)  ->  t
> snd   (x, y)  =   y

Here too, |s| and |t| are implicit type variables and the above are
abbreviations for

> fst :  {s : Type} ->  {t : Type} ->  (s, t)  ->  s
> fst    {s}            {t}            (x, y)  =   x

and similarly for |snd|. In Idris we can afford to write abbreviated
forms because the type checker can often infer the type of implicit
arguments. When needed, such types can be supplied within curly
braces. \\

Polymorphic functions are central to generic programming. Generic
programming is a methodology that aims at improving sofware reuse and
correctness while at the same time reducing documentation efforts. \\

IdrisLibs \cite{botta20162018} provides, among others, generic methods
for specifying and solving sequential decision problems.



\subsubsection{Aside: Constrained polymorphism and type classes}

There often is another form of polymorphism available in modern
programming languages which concerns the overloading of operator
symbols. In the context of functional programming, this is often
referred to as \emph{ad-hoc polymorphism} and provided by the
so-called \emph{type classes}. In Idris these go by the name of
\emph{interfaces}. As we do not need them for now, we just mention
this concept for completeness and in contrast to the concept of
parametric polymorphism.


\subsection{Data types}

Beside providing methods to define, compose and apply functions, most
functional programming languages support the definition of new data
types. 

We can introduce new types that extend the language via inductive
definitions like

< data Nat : Type where
<   Z  :  Nat
<   S  :  Nat -> Nat

which defines the type of natural numbers in Idris. The definition
coincides with the usual inductive definition of |Nat|: It states that  

  * |Nat| is a type.

  * |Z| (zero) is a value of type |Nat|.

  * ∀ |n : Nat|, |S n| (the successor of |n|) is a value of type |Nat|.

|Z| and |S| are called the \emph{data constructors} of |Nat|. Data
constructors are \emph{disjoint}: no natural number can be both zero and
a successor. Moreover every natural number is either zero or the 
successor of another natural number. These properties make it possible
to define \emph{total} functions via \emph{pattern matching}:

< plus : Nat -> Nat -> Nat
< plus  Z     n  =  n
< plus (S m)  n  =  S (plus m n)

(This amounts to specifying functions by recursion
equations which are accepted by the type checker, if it can guarantee
termination of the recursive calls because of a syntactic monotonicity
criterion.) 

\subsection{Lists}

< data List : Type -> Type where
<   Nil   :  List a
<   (::)  :  a -> List a -> List a

  * ∀ |a : Type|, |List a| is a type.

  * ∀ |a : Type|, |Nil| (the empty list) is a value of type |List a|.

  * ∀ |a : Type|, |x : a|, |xs : List a|, |x :: xs| is a value of type |List a|.

Thus, for instance

< xs : List Nat
< xs = Nil

< ys : List Nat
< ys = Z :: ((S Z) :: Nil)

\textbf{Notation: we usually write |[3,0,1]| instead of |(S (S (S Z)))
:: (Z :: ((S Z) :: Nil))|.}

In Idris, lists come with a number of useful predefined functions and
abbreviations:

\subsubsection{List comprehension}

< Idris> [0..3]
< [0, 1, 2, 3] : List Integer

< Idris> [5..2]
< [5, 4, 3, 2] : List Integer

< Idris> [2 * n | n <- [1..9]]
< [2, 4, 6, 8, 10, 12, 14, 16, 18] : List Integer

< Idris> map (2 *) [1..9]
< [2, 4, 6, 8, 10, 12, 14, 16, 18] : List Integer

\begin{shaded}
  \begin{exercise}
    \label{exercise3.4}  
    What is the type of |map|?
  \end{exercise}
\end{shaded}  

\subsubsection{Basic operations}

< length : List a -> Nat

\begin{shaded}
  \begin{exercise}
    \label{exercise3.5}    
    Implement |length|.
  \end{exercise}
\end{shaded}  
    
< (++) : List a -> List a -> List a
< (++)      Nil   ys  =  ys
< (++) (x :: xs)  ys  =  x :: (xs ++ ys)

\begin{shaded}
  \begin{exercise}
    \label{exercise3.6}    
    What is the result of |[3,1] ++ [2,0,1]|? Give a computational proof
    of your conjecture (i.e. by step-wise evaluation according to the
    definition of |(++)|).   
  \end{exercise}
\end{shaded}  

> concat : List (List a) -> List a
> concat        Nil   =  Nil
> concat (xs :: xss)  =  xs ++ concat xss

> map : (a -> b) -> List a -> List b
> map f      Nil   =  Nil
> map f (x :: xs)  =  f x :: map f xs

\begin{shaded}
  \begin{exercise}
    \label{exercise3.7}      
    Show that |map id = id|.
  \end{exercise}
\end{shaded}

\begin{shaded}
  \begin{exercise}
    \label{exercise3.8}
    Show that |map (f . g) = map f . map g|.
  \end{exercise}
\end{shaded}


\subsection{Vectors}

In many cases, one would like to operate with lists of specific
lengths.

For instance, require a function 

< zip : List a -> List b -> List (a, b)

to only take arguments of the same length. This can be done by encoding
the length of a list in its type:

> data Vect : Nat -> Type -> Type where
>   Nil   :  Vect Z a
>   (::)  :  (x : a) -> (xs : Vect n a) -> Vect (S n) a

This declaration can be seen as an infinite family of simpler datatype
declarations where |Vect0 A| only contains 0-length vectors, etc.

> data Vect0  :  Type -> Type where
>   Nil0   : Vect0 a
>   
> data Vect1  :  Type -> Type where
>   Cons1  :  (x : a) -> (xs : Vect0  a) -> Vect1  a
>   
> data Vect2  :  Type -> Type where
>   Cons2  :  (x : a) -> (xs : Vect1  a) -> Vect2  a

In this view it is easy to see that, even though the family as a whole
(|Vect|) has two constructors, each \emph{family member} (|Vect0|,
|Vect1|, etc.) has exactly one.

A simple example of a vector based function is |head| which extracts the
first element of a vector:

> head : Vect (S n) a -> a
> head (x :: xs) = x

Note that |head| is only defined for non-empty vectors: vectors of
length |S n| for some |n|.

\begin{shaded}
  \begin{exercise}
    \label{exercise3.9}  
    Implement a |tail| function that computes the tail of a non-empty
    vector.
  \end{exercise}
\end{shaded}
  
\begin{shaded}
  \begin{exercise}
    \label{exercise3.10}      
    It is easy to see that |∀ n : Nat|, |a : Type|, |v :
    Vect (S n) a|, |head v :: tail v| = v.
    Give a formal proof (like in section 2.5).
    What happens in the case |v = Nil|? 
  \end{exercise}
\end{shaded}

\subsection{Coming up}

The next lecture will be an introduction to dependently-typed
programming and theorem proving.


\pagebreak
\section*{Solutions}


\subsubsection*{Exercise \ref{exercise3.1}:}
< g : a -> (b -> c) = a -> b -> c



\subsubsection*{Exercise \ref{exercise3.2}:}
< uncurry g (x, y) = g x y

< curry g' x y   = g' (x, y)


\subsubsection*{Exercise \ref{exercise3.3}:}
< (uncurry . curry) g' (x, y)
<
<   = {- Def. composition -}
<
< uncurry (curry g') (x, y)
<
<   = {- Def. uncurry -}
<
< (curry g') x y
<
<   = {- Def. curry -}
<
< g' (x, y)

< (curry . uncurry) g x y
<
<   = {- Def. composition -}
<
< curry (uncurry g) x y
<
<   = {- Def. curry -}
<
< (uncurry g) (x, y)
<
<   = {- Def. uncurry -}
<
< g x y


\subsubsection*{Exercise \ref{exercise3.4}:}

< map : (a -> b) -> List a -> List b


\subsubsection*{Exercise \ref{exercise3.5}:}

< length      Nil   =  Z
< length (x :: xs)  =  S (length xs)


\subsubsection*{Exercise \ref{exercise3.6}:}

< [3,1] ++ [2,0,1] = [3,1,2,0,1]

< [3,1] ++ [2,0,1]
<
<   = {- Syntax -}
<
< (3 :: (1 :: Nil)) ++ (2 :: (0 :: (1 :: Nil)))
<
<   = {- Def. (++), case 2 -}
<
< 3 :: ((1 :: Nil) ++ (2 :: (0 :: (1 :: Nil))))
<
<   = {- Def. (++), case 2 -}
<
< 3 :: (1 :: (Nil ++ (2 :: (0 :: (1 :: Nil)))))
<
<   = {- Def. (++), case 1 -}
<
< 3 :: (1 :: (2 :: (0 :: (1 :: Nil))))
<
<   = {- Syntax -}
<
< [3,1,2,0,1]


\subsubsection*{Exercise \ref{exercise3.7}:}
The |Nil| case:

< map id Nil
<
<   = {- Def. of |map|, case 1 -}
<
< Nil
<
<   = {- Def. of |id| on lists -}
<
< id Nil

The |(::)| case:

< map id (x :: xs)
<
<   = {- Def. of |map|, case 2 -}
<
< id x :: map id xs
<
<   = {- Def. of |id| on list elements -}
<
< x :: map id xs
<
<   = {- induction hypothesis -}
<
< x :: xs
<
<   = {- Def. of |id| on lists -}
<
< id (x :: xs)


\subsubsection*{Exercise \ref{exercise3.8}:}
The |Nil| case:

< map (f . g) Nil
<
<   = {- Def. of |map|, case 1 -}
<
< Nil
<
<   = {- Def. of |map|, case 1, reverse -}
<
< map f Nil
<
<   = {- Def. of |map|, case 1, reverse, replace -}
<
< map f (map g Nil)
<
<   = {- Def. composition -}
<
< (map f . map g) Nil

The |(::)| case:

< map (f . g) (x :: xs)
<
<   = {- Def. of |map|, case 2 -}
<
< (f . g) x :: map (f . g) xs
<
<   = {- Def. of |.| -}
<
< f (g x) :: map (f . g) xs
<
<   = {- induction hypothesis -}
<
< f (g x) :: (map f . map g) xs
<
<   = {- Def. of |.| -}
<
< f (g x) :: map f (map g xs)
<
<   = {- Def. of |map|, case 2 -}
<
< map f (g x :: map g xs)
<
<   = {- Def. of |map|, case 2 -}
<
< map f (map g (x :: xs))
<
<   = {- Def. of |.| -}
<
< (map f . map g) (x :: xs)


\subsubsection*{Exercise \ref{exercise3.9}:}

> tail : Vect (S n) a -> Vect n a
> tail (x :: xs) = xs

\subsubsection*{Exercise \ref{exercise3.10}:}

In an formal-logic proof, we would need to treat case |v = Nil| which leads
  to a contradiction, since |Nil| is of type |Vect Z a ≠
  Vect S n a|. If this is not necessary in Idris, then because
  the type-checker is able to treat such impossible cases for us.

So we just have to look at the case |vs = v :: vs'|:
 
< head (v :: vs')  :: tail (v :: vs')
<
< = {- Def. of head -}
<
< v :: tail (v :: vs') 
<
< = {- Def. of tail -}
<
< v :: vs' 
<
< = {- Hypothesis |vs = v :: vs'| -}
<
< vs

In Idris the following suffices, though (the above is just the work
the type-checker is doing in the background and the programmer is
doing in her head...):

> headTailId : (n : Nat) -> (a : Type) -> (vs : Vect (S n) a)
>
>              ->  head vs :: tail vs = vs
>
> headTailId n a (v :: vs') = Refl                    
  
\pagebreak