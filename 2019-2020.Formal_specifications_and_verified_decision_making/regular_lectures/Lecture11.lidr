% -*-Latex-*-

%if False

> -- module Lecture11
> module Main

> import Data.Fin
> import Data.List
> import Data.List.Quantifiers
> import Data.Vect
> import Data.So
> import Effects
> import Effect.Exception
> import Effect.StdIO

> import SequentialDecisionProblems.CoreTheory
> import SequentialDecisionProblems.FullTheory
> import SequentialDecisionProblems.TabBackwardsInduction
> import SequentialDecisionProblems.Utils
> import SequentialDecisionProblems.FastStochasticDefaults
> import SequentialDecisionProblems.CoreTheoryOptDefaults
> import SequentialDecisionProblems.FullTheoryOptDefaults
> import SequentialDecisionProblems.TabBackwardsInductionOptDefaults

> import code.LowHigh
> import code.AvailableUnavailable
> import code.GoodBad

> import FastSimpleProb.SimpleProb
> import FastSimpleProb.BasicOperations
> import FastSimpleProb.BasicProperties
> import FastSimpleProb.MonadicOperations
> import FastSimpleProb.MonadicProperties
> import FastSimpleProb.Measures
> import FastSimpleProb.MeasuresProperties
> import FastSimpleProb.Operations
> import Sigma.Sigma
> import Double.Predicates
> import Double.Postulates
> import Double.Operations
> import Double.Properties
> import NonNegDouble.NonNegDouble
> import NonNegDouble.Constants
> import NonNegDouble.BasicOperations
> import NonNegDouble.Operations
> import NonNegDouble.Properties
> import NonNegDouble.Predicates
> import NonNegDouble.LTEProperties
> import Finite.Predicates
> import Finite.Operations
> import Finite.Properties
> import Decidable.Predicates
> import Decidable.Properties
> import LocalEffect.Exception
> import LocalEffect.StdIO
> import Fin.Operations
> import List.Operations
> import Unit.Properties

> %default total
> %auto_implicits off
> %access public export

%endif


\setcounter{section}{10}

\stepcounter{section}
%\section{Specifying an emission problem}

In this lecture we learn how to specify and solve the GHG emission
problem sketched in lecture 1 using the |SequentialDecisionProblems|
components of |IdrisLibs|, see \cite{botta20162020}.

The idea is that "best" decisions on levels of greenhouse gases (GHG)
emissions (that is, how much GHG shall be allowed to be emitted in a
given time period) are affected by three major sources of uncertainty:

\begin{enumerate}

  \item uncertainty about the (typically negative) effects of high GHG
        concentrations in the atmosphere,

  \item uncertainty about the availability of effective (cheap,
        efficient) technologies for reducing GHG emissions,

  \item uncertainty about the capability of actually implementing a
        decision on a given GHG emission level.

\end{enumerate}

We study the effects of these uncertainties on optimal sequences of
emission policies.

We design an emission game that accounts for all three sources of
uncertainty and yet is simple enough to support investigating the
logical consequences of different assumptions through comparisons and
parametric studies. 

For a more comprehensive discussion of this approach and of the emission
game, see \cite{esd-9-525-2018}.


\subsection{Controls}

We consider a game in which, at each decision step, the decision maker
can select between low and high GHG emissions

> SequentialDecisionProblems.CoreTheory.Ctrl t x = LowHigh

Low emissions, if implemented, increase the cumulated GHG emissions less
than high emissions.


\subsection{States}

At each decision step, the decision maker has to choose an option on the
basis of four data: the cumulated GHG emissions, the current emission
level (low or high), the availability of effective technologies for
reducing GHG emissions and the state of the world. Effective
technologies for reducing GHG emissions can be either available or
unavailable. The state of the world can be either good or bad:

> CumulatedEmissions : (t : Nat) -> Type
> 
> CumulatedEmissions t = Fin (S t)

> SequentialDecisionProblems.CoreTheory.State t  =  (CumulatedEmissions t, LowHigh, AvailableUnavailable, GoodBad)

The idea is that the game starts with zero cumulated emissions, high
emission levels, unavailable GHG technologies and with the world in a
good state. 

In these conditions, the probability to enter the bad state is
low. But if the cumulated emissions increase beyond a fixed critical
threshold, the probability that the state of the world turns bad
increases. If the world is the bad state, there is no chance to come
back to the good state.

Similarly, the probability that effective technologies for reducing GHG
emissions become available increases after a fixed number of decision
steps. Once available, effective technologies stay available for ever.

The capability of actually implementing a decision on a given GHG
emission level in general depends on many factors. In our simplified
setup, we just investigate the effect of inertia: implementing low
emissions is easier when low emission policies are already in place than
when the current emission policies are high emission
policies. Similarly, implementing high emission policies is easier under
high emissions policies than under low emissions policies.


\subsection{Transition function}

The critical cumulated emissions threshold:
 
> crE : Double
> 
> crE = 4.0

The critical number of decision steps:
 
> crN : Nat
> 
> crN = 2

The probability of staying in a good world when the cumulated
emissions are |<=| the critical threshold |crE|:
 
> pS1  :  NonNegDouble
> 
> pS1  =  cast 0.9

The probability of staying in a good world when the cumulated emissions
are |>=| the critical threshold |crE|:
 
> pS2        :  NonNegDouble
> 
> pS2        =  cast 0.1

> check01  :  pS2 `NonNegDouble.Predicates.LTE` pS1 -- semantic check
> 
> check01  =  MkLTE Oh

The probability of effective technologies for reducing GHG emissions
becoming available when the number of decision steps is below |crN|:
 
> pA1  :  NonNegDouble
> 
> pA1  =  cast 0.1

The probability of effective technologies for reducing GHG emissions
becoming available when the number of decision steps is above |crN|:
 
> pA2        :  NonNegDouble
> 
> pA2        =  cast 0.9

> check02  :  pA1 `NonNegDouble.Predicates.LTE` pA2 -- semantic check
> 
> check02  =  MkLTE Oh

The probability of being able to implement low emission policies when the
current emissions are low and low emissions are selected:

> pLL  :  NonNegDouble
> 
> pLL  =  cast 0.9

The probability of being able to implement low emission policies when the
current emissions are high and low emissions are selected:
 
> pLH        :  NonNegDouble
> 
> pLH        =  cast 0.7

> check03  :  pLH `NonNegDouble.Predicates.LTE` pLL -- semantic check
> 
> check03  =  MkLTE Oh

The probability of being able to implement high emission policies when the
current emissions are low and high emissions are selected;
 
> pHL  :  NonNegDouble
> 
> pHL  =  cast 0.7

The probability of being able to implement high emission policies when the
current emissions are high and high emissions are selected:
 
> pHH        :  NonNegDouble
> 
> pHH        =  cast 0.9

> check04  :  pHL `NonNegDouble.Predicates.LTE` pHH -- semantic check
> 
> check04  =  MkLTE Oh

Low emissions leave the cumulated emissions unchanged, high emissions
increase the cumulated emissions by one:

The transition function:

The transition function: \textbf{high} emissions

The transition function: \textbf{high} emissions, \textbf{unavailable} GHG technologies

The transition function: \textbf{high} emissions, \textbf{unavailable} GHG technologies, \textbf{good} world

> using implementation NumNonNegDouble
>   
>   SequentialDecisionProblems.CoreTheory.nexts t (e, H, U, G) L =
>   
>     let ttres = mkSimpleProb  -- case |t <= crN && fromFin e <= crE|
>      
>           [  ((weaken e,  L, U, G),          pLH   *  (one -  pA1)  *         pS1), 
>                                                                    
>              ((    FS e,  H, U, G),  (one -  pLH)  *  (one -  pA1)  *         pS1),
>                                                                    
>              ((weaken e,  L, A, G),          pLH   *          pA1   *         pS1), 
>                                                                    
>              ((    FS e,  H, A, G),  (one -  pLH)  *          pA1   *         pS1),
>                                                                    
>              ((weaken e,  L, U, B),          pLH   *  (one -  pA1)  * (one -  pS1)), 
>                                                                    
>              ((    FS e,  H, U, B),  (one -  pLH)  *  (one -  pA1)  * (one -  pS1)),
>                                                                    
>              ((weaken e,  L, A, B),          pLH   *          pA1   * (one -  pS1)), 
>                                                                    
>              ((    FS e,  H, A, B),  (one -  pLH)  *          pA1   * (one -  pS1))  ] in
>                      
>     let tfres = mkSimpleProb  -- case |t <= crN && fromFin e > crE|
>     
>           [  ((weaken e,  L, U, G),          pLH   *  (one -  pA1)  *         pS2), 
>                                                                    
>              ((    FS e,  H, U, G),  (one -  pLH)  *  (one -  pA1)  *         pS2),
>                                                                    
>              ((weaken e,  L, A, G),          pLH   *          pA1   *         pS2), 
>                                                                    
>              ((    FS e,  H, A, G),  (one -  pLH)  *          pA1   *         pS2),
>                                                                    
>              ((weaken e,  L, U, B),          pLH   *  (one -  pA1)  * (one -  pS2)), 
>                                                                    
>              ((    FS e,  H, U, B),  (one -  pLH)  *  (one -  pA1)  * (one -  pS2)),
>                                                                    
>              ((weaken e,  L, A, B),          pLH   *          pA1   * (one -  pS2)), 
>                                                                    
>              ((    FS e,  H, A, B),  (one -  pLH)  *          pA1   * (one -  pS2))  ] in
>            
>     let ftres = mkSimpleProb  -- case |t > crN && fromFin e <= crE| 
>     
>           [  ((weaken e,  L, U, G),          pLH   *  (one -  pA2)  *         pS1), 
>                                                                    
>              ((    FS e,  H, U, G),  (one -  pLH)  *  (one -  pA2)  *         pS1),
>                                                                    
>              ((weaken e,  L, A, G),          pLH   *          pA2   *         pS1), 
>                                                                    
>              ((    FS e,  H, A, G),  (one -  pLH)  *          pA2   *         pS1),
>                                                                    
>              ((weaken e,  L, U, B),          pLH   *  (one -  pA2)  * (one -  pS1)), 
>                                                                    
>              ((    FS e,  H, U, B),  (one -  pLH)  *  (one -  pA2)  * (one -  pS1)),
>                                                                    
>              ((weaken e,  L, A, B),          pLH   *          pA2   * (one -  pS1)), 
>                                                                    
>              ((    FS e,  H, A, B),  (one -  pLH)  *          pA2   * (one -  pS1))  ] in
>            
>     let ffres = mkSimpleProb  -- case |t > crN && fromFin e > crE| 
>     
>           [  ((weaken e,  L, U, G),          pLH   *  (one -  pA2)   *         pS2), 
>                                                                     
>              ((    FS e,  H, U, G),  (one -  pLH)  *  (one -  pA2)   *         pS2),
>                                                                     
>              ((weaken e,  L, A, G),          pLH   *          pA2    *         pS2), 
>                                                                     
>              ((    FS e,  H, A, G),  (one -  pLH)  *          pA2    *         pS2),
>                                                                     
>              ((weaken e,  L, U, B),          pLH   *  (one -  pA2)   * (one -  pS2)), 
>                                                                     
>              ((    FS e,  H, U, B),  (one -  pLH)  *  (one -  pA2)   * (one -  pS2)),
>                                                                     
>              ((weaken e,  L, A, B),          pLH   *          pA2    * (one -  pS2)), 
>                                                                     
>              ((    FS e,  H, A, B),  (one -  pLH)  *          pA2    * (one -  pS2))  ] in
>            
>     case (t <= crN) of
>     
>     True   =>   case (fromFin e <= crE) of
>       
>                 True  =>   trim ttres
>                   
>                 False =>   trim tfres
>                  
>     False  =>   case (fromFin e <= crE) of
>       
>                 True  =>   trim ftres
>                   
>                 False =>   trim ffres
>                   
>   SequentialDecisionProblems.CoreTheory.nexts t (e, H, U, G) H =
>   
>     let ttres = mkSimpleProb
>     
>           [  ((weaken e,  L, U, G),  (one -  pHH)  *  (one -  pA1)  *         pS1), 
>           
>              ((    FS e,  H, U, G),          pHH   *  (one -  pA1)  *         pS1),
>            
>              ((weaken e,  L, A, G),  (one -  pHH)  *          pA1   *         pS1), 
>            
>              ((    FS e,  H, A, G),          pHH   *          pA1   *         pS1),
>            
>              ((weaken e,  L, U, B),  (one -  pHH)  *  (one -  pA1)  * (one -  pS1)), 
>            
>              ((    FS e,  H, U, B),          pHH   *  (one -  pA1)  * (one -  pS1)),
>            
>              ((weaken e,  L, A, B),  (one -  pHH)  *          pA1   * (one -  pS1)), 
>            
>              ((    FS e,  H, A, B),          pHH   *          pA1   * (one -  pS1))  ] in
>            
>     let tfres = mkSimpleProb
>     
>           [  ((weaken e,  L, U, G),  (one -  pHH)  *  (one -  pA1)  *         pS2), 
>           
>              ((    FS e,  H, U, G),          pHH   *  (one -  pA1)  *         pS2),
>            
>              ((weaken e,  L, A, G),  (one -  pHH)  *          pA1   *         pS2), 
>            
>              ((    FS e,  H, A, G),          pHH   *          pA1   *         pS2),
>            
>              ((weaken e,  L, U, B),  (one -  pHH)  *  (one -  pA1)  * (one -  pS2)), 
>            
>              ((    FS e,  H, U, B),          pHH   *  (one -  pA1)  * (one -  pS2)),
>            
>              ((weaken e,  L, A, B),  (one -  pHH)  *          pA1   * (one -  pS2)), 
>            
>              ((    FS e,  H, A, B),          pHH   *          pA1   * (one -  pS2))  ] in
>            
>     let ftres = mkSimpleProb
>     
>           [  ((weaken e,  L, U, G),  (one -  pHH)  *  (one -  pA2)  *         pS1), 
>           
>              ((    FS e,  H, U, G),          pHH   *  (one -  pA2)  *         pS1),
>            
>              ((weaken e,  L, A, G),  (one -  pHH)  *          pA2   *         pS1), 
>            
>              ((    FS e,  H, A, G),          pHH   *          pA2   *         pS1),
>            
>              ((weaken e,  L, U, B),  (one -  pHH)  *  (one -  pA2)  * (one -  pS1)), 
>            
>              ((    FS e,  H, U, B),          pHH   *  (one -  pA2)  * (one -  pS1)),
>            
>              ((weaken e,  L, A, B),  (one -  pHH)  *          pA2   * (one -  pS1)), 
>            
>              ((    FS e,  H, A, B),          pHH   *          pA2   * (one -  pS1))  ] in
>            
>     let ffres = mkSimpleProb
>     
>           [  ((weaken e,  L, U, G),  (one -  pHH)  *  (one -  pA2)  *         pS2), 
>           
>              ((    FS e,  H, U, G),          pHH   *  (one -  pA2)  *         pS2),
>            
>              ((weaken e,  L, A, G),  (one -  pHH)  *          pA2   *         pS2), 
>            
>              ((    FS e,  H, A, G),          pHH   *          pA2   *         pS2),
>            
>              ((weaken e,  L, U, B),  (one -  pHH)  *  (one -  pA2)  * (one -  pS2)), 
>            
>              ((    FS e,  H, U, B),          pHH   *  (one -  pA2)  * (one -  pS2)),
>            
>              ((weaken e,  L, A, B),  (one -  pHH)  *          pA2   * (one -  pS2)), 
>            
>              ((    FS e,  H, A, B),          pHH   *          pA2   * (one -  pS2))  ] in
>            
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>       
>                True   =>  trim ttres
>                 
>                False  =>  trim tfres
>                  
>     False  =>  case (fromFin e <= crE) of
>       
>                True   =>  trim ftres
>                  
>                False  =>  trim ffres

The transition function: \textbf{high} emissions, \textbf{unavailable} GHG technologies, \textbf{bad} world

>   SequentialDecisionProblems.CoreTheory.nexts t (e, H, U, B) L =
>   
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),          pLH   * (one -  pA1)), 
>           
>              ((    FS e,  H, U, B),  (one -  pLH)  * (one -  pA1)),
>            
>              ((weaken e,  L, A, B),          pLH   *         pA1 ), 
>            
>              ((    FS e,  H, A, B),  (one -  pLH)  *         pA1 )  ] in
>                  
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),          pLH   * (one -  pA1)), 
>           
>              ((    FS e,  H, U, B),  (one -  pLH)  * (one -  pA1)),
>              
>              ((weaken e,  L, A, B),          pLH   *         pA1 ), 
>              
>              ((    FS e,  H, A, B),  (one -  pLH)  *         pA1 )  ] in
>                  
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),          pLH   * (one -  pA2)), 
>           
>              ((    FS e,  H, U, B),  (one -  pLH)  * (one -  pA2)),
>              
>              ((weaken e,  L, A, B),          pLH   *         pA2 ), 
>              
>              ((    FS e,  H, A, B),  (one -  pLH)  *         pA2 )  ] in
>                  
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),          pLH   * (one -  pA2)), 
>           
>              ((    FS e,  H, U, B),  (one -  pLH)  * (one -  pA2)),
>              
>              ((weaken e,  L, A, B),          pLH   *         pA2 ), 
>              
>              ((    FS e,  H, A, B),  (one -  pLH)  *         pA2 )  ] in
>                  
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>       
>                True   =>  trim ttres
>                  
>                False  =>  trim tfres
>                  
>     False  =>  case (fromFin e <= crE) of
>       
>                True   =>  trim ftres
>                  
>                False  =>  trim ffres
>                  
>   SequentialDecisionProblems.CoreTheory.nexts t (e, H, U, B) H =
>   
>     let ttres = mkSimpleProb 
>              
>           [  ((weaken e,  L, U, B),  (one -  pHH)  * (one -  pA1)), 
>           
>              ((    FS e,  H, U, B),          pHH   * (one -  pA1)),
>              
>              ((weaken e,  L, A, B),  (one -  pHH)  *         pA1 ), 
>              
>              ((    FS e,  H, A, B),          pHH   *         pA1 )  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),  (one -  pHH)  * (one -  pA1)), 
>           
>              ((    FS e,  H, U, B),          pHH   * (one -  pA1)),
>              
>              ((weaken e,  L, A, B),  (one -  pHH)  *         pA1 ), 
>              
>              ((    FS e,  H, A, B),          pHH   *         pA1 )  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),  (one -  pHH)  * (one -  pA2)), 
>           
>              ((    FS e,  H, U, B),          pHH   * (one -  pA2)),
>              
>              ((weaken e,  L, A, B),  (one -  pHH)  *         pA2 ), 
>              
>              ((    FS e,  H, A, B),          pHH   *         pA2 )  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),  (one -  pHH)  * (one -  pA2)), 
>           
>              ((    FS e,  H, U, B),          pHH   * (one -  pA2)),
>              
>              ((weaken e,  L, A, B),  (one -  pHH)  *         pA2 ), 
>              
>              ((    FS e,  H, A, B),          pHH   *         pA2 )  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres

The transition function: \textbf{high} emissions, \textbf{available} GHG technologies

The transition function: \textbf{high} emissions, \textbf{available} GHG technologies, \textbf{good} world

>   SequentialDecisionProblems.CoreTheory.nexts t (e, H, A, G) L =
>   
>     let ttres = mkSimpleProb 
>               
>           [  ((weaken e,  L, A, G),          pLH   *         pS1), 
>           
>              ((    FS e,  H, A, G),  (one -  pLH)  *         pS1),
>              
>              ((weaken e,  L, A, B),          pLH   * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLH)  * (one -  pS1))  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),          pLH   *         pS2), 
>           
>              ((    FS e,  H, A, G),  (one -  pLH)  *         pS2),
>              
>              ((weaken e,  L, A, B),          pLH   * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLH)  * (one -  pS2))  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),          pLH   *         pS1),
>            
>              ((    FS e,  H, A, G),  (one -  pLH)  *         pS1),
>              
>              ((weaken e,  L, A, B),          pLH   * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLH)  * (one -  pS1))  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),          pLH   *         pS2), 
>           
>              ((    FS e,  H, A, G),  (one -  pLH)  *         pS2),
>              
>              ((weaken e,  L, A, B),          pLH   * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLH)  * (one -  pS2))  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres
>                
>   SequentialDecisionProblems.CoreTheory.nexts t (e, H, A, G) H =
>   
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),  (one -  pHH)  *         pS1), 
>           
>              ((    FS e,  H, A, G),          pHH   *         pS1),
>              
>              ((weaken e,  L, A, B),  (one -  pHH)  * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),          pHH   * (one -  pS1))  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),  (one -  pHH)  *         pS2), 
>           
>              ((    FS e,  H, A, G),          pHH   *         pS2),
>              
>              ((weaken e,  L, A, B),  (one -  pHH)  * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),          pHH   * (one -  pS2))  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),  (one -  pHH)  *         pS1), 
>           
>              ((    FS e,  H, A, G),          pHH   *         pS1),
>              
>              ((weaken e,  L, A, B),  (one -  pHH)  * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),          pHH   * (one -  pS1))  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),  (one -  pHH)  *         pS2), 
>           
>              ((    FS e,  H, A, G),          pHH   *         pS2),
>              
>              ((weaken e,  L, A, B),  (one -  pHH)  * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),          pHH   * (one -  pS2))  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres

The transition function: \textbf{high} emissions, \textbf{available} GHG technologies, \textbf{bad} world

>   SequentialDecisionProblems.CoreTheory.nexts t (e, H, A, B) L =
>   
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),          pLH ), 
>           
>              ((    FS e,  H, A, B),  (one -  pLH))  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),          pLH ), 
>           
>              ((    FS e,  H, A, B),  (one -  pLH))  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),          pLH ), 
>           
>              ((    FS e,  H, A, B),  (one -  pLH))  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),          pLH ), 
>              ((    FS e,  H, A, B),  (one -  pLH))  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres
>                
>   SequentialDecisionProblems.CoreTheory.nexts t (e, H, A, B) H =
>   
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),  (one -  pHH)), 
>           
>              ((    FS e,  H, A, B),          pHH )  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),  (one -  pHH)), 
>           
>              ((    FS e,  H, A, B),          pHH )  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),  (one -  pHH)), 
>           
>              ((    FS e,  H, A, B),          pHH )  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),  (one -  pHH)), 
>           
>              ((    FS e,  H, A, B),          pHH )  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres

The transition function: \textbf{low} emissions

The transition function: \textbf{low} emissions, \textbf{unavailable} GHG technologies

The transition function: \textbf{low} emissions, \textbf{unavailable} GHG technologies, \textbf{good} world

>   SequentialDecisionProblems.CoreTheory.nexts t (e, L, U, G) L =
>   
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, G),          pLL   *  (one -  pA1)  *         pS1), 
>           
>              ((    FS e,  H, U, G),  (one -  pLL)  *  (one -  pA1)  *         pS1),
>              
>              ((weaken e,  L, A, G),          pLL   *          pA1   *         pS1), 
>              
>              ((    FS e,  H, A, G),  (one -  pLL)  *          pA1   *         pS1),
>              
>              ((weaken e,  L, U, B),          pLL   *  (one -  pA1)  * (one -  pS1)), 
>              
>              ((    FS e,  H, U, B),  (one -  pLL)  *  (one -  pA1)  * (one -  pS1)),
>              
>              ((weaken e,  L, A, B),          pLL   *          pA1   * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  *          pA1   * (one -  pS1))  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, G),          pLL   *  (one -  pA1)  *         pS2), 
>           
>              ((    FS e,  H, U, G),  (one -  pLL)  *  (one -  pA1)  *         pS2),
>              
>              ((weaken e,  L, A, G),          pLL   *          pA1   *         pS2), 
>              
>              ((    FS e,  H, A, G),  (one -  pLL)  *          pA1   *         pS2),
>              
>              ((weaken e,  L, U, B),          pLL   *  (one -  pA1)  * (one -  pS2)), 
>              
>              ((    FS e,  H, U, B),  (one -  pLL)  *  (one -  pA1)  * (one -  pS2)),
>              
>              ((weaken e,  L, A, B),          pLL   *          pA1   * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  *          pA1   * (one -  pS2))  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, G),          pLL   *  (one -  pA2)  *         pS1), 
>           
>              ((    FS e,  H, U, G),  (one -  pLL)  *  (one -  pA2)  *         pS1),
>              
>              ((weaken e,  L, A, G),          pLL   *          pA2   *         pS1), 
>              
>              ((    FS e,  H, A, G),  (one -  pLL)  *          pA2   *         pS1),
>              
>              ((weaken e,  L, U, B),          pLL   *  (one -  pA2)  * (one -  pS1)), 
>              
>              ((    FS e,  H, U, B),  (one -  pLL)  *  (one -  pA2)  * (one -  pS1)),
>              
>              ((weaken e,  L, A, B),          pLL   *          pA2   * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  *          pA2   * (one -  pS1))  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, G),          pLL   *  (one -  pA2)  *         pS2), 
>           
>              ((    FS e,  H, U, G),  (one -  pLL)  *  (one -  pA2)  *         pS2),
>              
>              ((weaken e,  L, A, G),          pLL   *          pA2   *         pS2), 
>              
>              ((    FS e,  H, A, G),  (one -  pLL)  *          pA2   *         pS2),
>              
>              ((weaken e,  L, U, B),          pLL   *  (one -  pA2)  * (one -  pS2)), 
>              
>              ((    FS e,  H, U, B),  (one -  pLL)  *  (one -  pA2)  * (one -  pS2)),
>              
>              ((weaken e,  L, A, B),          pLL   *          pA2   * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  *          pA2   * (one -  pS2))  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>   trim ttres
>                
>                False  =>   trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>   trim ftres
>                
>                False  =>   trim ffres
>                
>   SequentialDecisionProblems.CoreTheory.nexts t (e, L, U, G) H =
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, G),  (one -  pHL)  *  (one -  pA1)  *         pS1), 
>           
>              ((    FS e,  H, U, G),          pHL   *  (one -  pA1)  *         pS1),
>              
>              ((weaken e,  L, A, G),  (one -  pHL)  *          pA1   *         pS1), 
>              
>              ((    FS e,  H, A, G),          pHL   *          pA1   *         pS1),
>              
>              ((weaken e,  L, U, B),  (one -  pHL)  *  (one -  pA1)  * (one -  pS1)), 
>              
>              ((    FS e,  H, U, B),          pHL   *  (one -  pA1)  * (one -  pS1)),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  *          pA1   * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),          pHL   *          pA1   * (one -  pS1))  ] in
>              
>     let tfres = mkSimpleProb 
>                
>           [  ((weaken e,  L, U, G),  (one -  pHL)  *  (one -  pA1)  *         pS2), 
>           
>              ((    FS e,  H, U, G),          pHL   *  (one -  pA1)  *         pS2),
>              
>              ((weaken e,  L, A, G),  (one -  pHL)  *          pA1   *         pS2), 
>              
>              ((    FS e,  H, A, G),          pHL   *          pA1   *         pS2),
>              
>              ((weaken e,  L, U, B),  (one -  pHL)  *  (one -  pA1)  * (one -  pS2)), 
>              
>              ((    FS e,  H, U, B),          pHL   *  (one -  pA1)  * (one -  pS2)),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  *          pA1   * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),          pHL   *          pA1   * (one -  pS2))  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, G),  (one -  pHL)  *  (one -  pA2)  *         pS1), 
>           
>              ((    FS e,  H, U, G),          pHL   *  (one -  pA2)  *         pS1),
>              
>              ((weaken e,  L, A, G),  (one -  pHL)  *          pA2   *         pS1), 
>              
>              ((    FS e,  H, A, G),          pHL   *          pA2   *         pS1),
>              
>              ((weaken e,  L, U, B),  (one -  pHL)  *  (one -  pA2)  * (one -  pS1)), 
>              
>              ((    FS e,  H, U, B),          pHL   *  (one -  pA2)  * (one -  pS1)),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  *          pA2   * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),          pHL   *          pA2   * (one -  pS1))  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, G),  (one -  pHL)  *  (one -  pA2)  *         pS2), 
>           
>              ((    FS e,  H, U, G),          pHL   *  (one -  pA2)  *         pS2),
>              
>              ((weaken e,  L, A, G),  (one -  pHL)  *          pA2   *         pS2), 
>              
>              ((    FS e,  H, A, G),          pHL   *          pA2   *         pS2),
>              
>              ((weaken e,  L, U, B),  (one -  pHL)  *  (one -  pA2)  * (one -  pS2)), 
>              
>              ((    FS e,  H, U, B),          pHL   *  (one -  pA2)  * (one -  pS2)),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  *          pA2   * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),          pHL   *          pA2   * (one -  pS2))  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres

The transition function: \textbf{low} emissions, \textbf{unavailable} GHG technologies, \textbf{bad} world

>   SequentialDecisionProblems.CoreTheory.nexts t (e, L, U, B) L =
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),          pLL   * (one -  pA1)), 
>           
>              ((    FS e,  H, U, B),  (one -  pLL)  * (one -  pA1)),
>              
>              ((weaken e,  L, A, B),          pLL   *         pA1 ), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  *         pA1 )  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),          pLL   * (one -  pA1)), 
>           
>              ((    FS e,  H, U, B),  (one -  pLL)  * (one -  pA1)),
>              
>              ((weaken e,  L, A, B),          pLL   *         pA1 ), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  *         pA1 )  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),          pLL   * (one -  pA2)), 
>           
>              ((    FS e,  H, U, B),  (one -  pLL)  * (one -  pA2)),
>              
>              ((weaken e,  L, A, B),          pLL   *         pA2 ), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  *         pA2 )  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),          pLL   * (one -  pA2)), 
>           
>              ((    FS e,  H, U, B),  (one -  pLL)  * (one -  pA2)),
>              
>              ((weaken e,  L, A, B),          pLL   *         pA2 ), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  *         pA2 )  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres
>                
>   SequentialDecisionProblems.CoreTheory.nexts t (e, L, U, B) H =
>   
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),  (one -  pHL)  * (one -  pA1)), 
>           
>              ((    FS e,  H, U, B),          pHL   * (one -  pA1)),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  *         pA1 ), 
>              
>              ((    FS e,  H, A, B),          pHL   *         pA1 )  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),  (one -  pHL)  * (one -  pA1)), 
>           
>              ((    FS e,  H, U, B),          pHL   * (one -  pA1)),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  *         pA1 ), 
>              
>              ((    FS e,  H, A, B),          pHL   *         pA1 )  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),  (one -  pHL)  * (one -  pA2)), 
>           
>              ((    FS e,  H, U, B),          pHL   * (one -  pA2)),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  *         pA2 ), 
>              
>              ((    FS e,  H, A, B),          pHL   *         pA2 )  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, U, B),  (one -  pHL)  * (one -  pA2)), 
>           
>              ((    FS e,  H, U, B),          pHL   * (one -  pA2)),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  *         pA2 ), 
>              
>              ((    FS e,  H, A, B),          pHL   *         pA2 )  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres

The transition function: \textbf{low} emissions, \textbf{available} GHG technologies

The transition function: \textbf{low} emissions, \textbf{available} GHG technologies, \textbf{good} world

>   SequentialDecisionProblems.CoreTheory.nexts t (e, L, A, G) L =
>   
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),          pLL   *         pS1), 
>           
>              ((    FS e,  H, A, G),  (one -  pLL)  *         pS1),
>              
>              ((weaken e,  L, A, B),          pLL   * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  * (one -  pS1))  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),          pLL   *         pS2), 
>           
>              ((    FS e,  H, A, G),  (one -  pLL)  *         pS2),
>              
>              ((weaken e,  L, A, B),          pLL   * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  * (one -  pS2))  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),          pLL   *         pS1), 
>           
>              ((    FS e,  H, A, G),  (one -  pLL)  *         pS1),
>              
>              ((weaken e,  L, A, B),          pLL   * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  * (one -  pS1))  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),          pLL   *         pS2), 
>           
>              ((    FS e,  H, A, G),  (one -  pLL)  *         pS2),
>              
>              ((weaken e,  L, A, B),          pLL   * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),  (one -  pLL)  * (one -  pS2))  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres
>                
>   SequentialDecisionProblems.CoreTheory.nexts t (e, L, A, G) H =
>   
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),  (one -  pHL)  *         pS1), 
>           
>              ((    FS e,  H, A, G),          pHL   *         pS1),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),          pHL   * (one -  pS1))  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),  (one -  pHL)  *         pS2), 
>           
>              ((    FS e,  H, A, G),          pHL   *         pS2),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),          pHL   * (one -  pS2))  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),  (one -  pHL)  *         pS1), 
>           
>              ((    FS e,  H, A, G),          pHL   *         pS1),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  * (one -  pS1)), 
>              
>              ((    FS e,  H, A, B),          pHL   * (one -  pS1))  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, G),  (one -  pHL)  *         pS2), 
>           
>              ((    FS e,  H, A, G),          pHL   *         pS2),
>              
>              ((weaken e,  L, A, B),  (one -  pHL)  * (one -  pS2)), 
>              
>              ((    FS e,  H, A, B),          pHL   * (one -  pS2))  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres

The transition function: \textbf{low} emissions, \textbf{available} GHG technologies, \textbf{bad} world

>   SequentialDecisionProblems.CoreTheory.nexts t (e, L, A, B) L =
>   
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),         pLL ), 
>           
>              ((    FS e,  H, A, B), (one -  pLL))  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),         pLL ), 
>           
>              ((    FS e,  H, A, B), (one -  pLL))  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),         pLL ), 
>           
>              ((    FS e,  H, A, B), (one -  pLL))  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B),         pLL ), 
>           
>              ((    FS e,  H, A, B), (one -  pLL))  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres
>                
>   SequentialDecisionProblems.CoreTheory.nexts t (e, L, A, B) H =
>   
>     let ttres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B), (one -  pHL)), 
>           
>              ((    FS e,  H, A, B),         pHL )  ] in
>              
>     let tfres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B), (one -  pHL)), 
>           
>              ((    FS e,  H, A, B),         pHL )  ] in
>              
>     let ftres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B), (one -  pHL)), 
>           
>              ((    FS e,  H, A, B),         pHL )  ] in
>              
>     let ffres = mkSimpleProb 
>     
>           [  ((weaken e,  L, A, B), (one -  pHL)), 
>           
>              ((    FS e,  H, A, B),         pHL )  ] in
>              
>     case (t <= crN) of
>     
>     True   =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ttres
>                
>                False  =>  trim tfres
>                
>     False  =>  case (fromFin e <= crE) of
>     
>                True   =>  trim ftres
>                
>                False  =>  trim ffres


\subsection{|Val| and |LTE|:}

Values of type |Val| are just non-negative double precision floating
point numbers, addition, zero and |(<=)| are defined accordingly:

> SequentialDecisionProblems.CoreTheory.Val = NonNegDouble.NonNegDouble
 
> SequentialDecisionProblems.CoreTheory.plus = NonNegDouble.Operations.plus

> SequentialDecisionProblems.CoreTheory.zero = fromInteger @{NumNonNegDouble} 0

> SequentialDecisionProblems.CoreTheory.LTE = NonNegDouble.Predicates.LTE

> SequentialDecisionProblems.FullTheory.reflexiveLTE = NonNegDouble.LTEProperties.reflexiveLTE

> SequentialDecisionProblems.FullTheory.transitiveLTE = NonNegDouble.LTEProperties.transitiveLTE
 
> SequentialDecisionProblems.FullTheory.monotonePlusLTE = NonNegDouble.LTEProperties.monotonePlusLTE

> SequentialDecisionProblems.CoreTheoryOptDefaults.totalPreorderLTE = NonNegDouble.LTEProperties.totalPreorderLTE


\subsection{Reward function}

The idea is that being in a good world yields one unit of benefits per
step and being in a bad world yield less benefits. These are defined by
the ratio |badOverGood|.

The ratio between the benefits in a bad world and the benefits in a good world:

> badOverGood        :  NonNegDouble
> 
> badOverGood        =  cast 0.89

> check05  :  badOverGood `NonNegDouble.Predicates.LTE` one -- semantic check
> 
> check05  =  MkLTE Oh

Emitting GHGs also brings benefits. These are a fraction of the step
benefits in a good world and low emissions bring less benefits than high
emissions:

The ratio between low emissions benefits and step benefits in a good
world, when effective technologies for reducing GHG emissions are
unavailable:
 
> lowOverGoodUnavailable  :  NonNegDouble
> 
> lowOverGoodUnavailable  =  cast 0.1

> check06  :  lowOverGoodUnavailable `NonNegDouble.Predicates.LTE` one -- semantic check
> 
> check06  =  MkLTE Oh

The ratio between low emissions benefits and step benefits in a good
world, when effective technologies for reducing GHG emissions are
available:

> lowOverGoodAvailable  :  NonNegDouble
> 
> lowOverGoodAvailable  =  cast 0.2

> check07  :  lowOverGoodAvailable `NonNegDouble.Predicates.LTE` one -- semantic check
> 
> check07  =  MkLTE Oh
> 
> check08  :  lowOverGoodUnavailable `NonNegDouble.Predicates.LTE` lowOverGoodAvailable -- semantic check
> 
> check08  =  MkLTE Oh

The ratio between high emissions benefits and step benefits in a good world:

> highOverGood  :  NonNegDouble
> 
> highOverGood  =  cast 0.3

> check09  :  highOverGood `NonNegDouble.Predicates.LTE` one -- semantic check
> 
> check09  =  MkLTE Oh

> check10  :  lowOverGoodAvailable `NonNegDouble.Predicates.LTE` highOverGood -- semantic check
> 
> check10  =  MkLTE Oh

The rewards only depend on the next state, not on the current state or on
the selected control:

> using implementation NumNonNegDouble
>   
>   SequentialDecisionProblems.CoreTheory.reward t x y (e, H, U, G)  =  one                +  one  * highOverGood
>     
>   SequentialDecisionProblems.CoreTheory.reward t x y (e, H, U, B)  =  one * badOverGood  +  one  * highOverGood
>
>   SequentialDecisionProblems.CoreTheory.reward t x y (e, H, A, G)  =  one                +  one  * highOverGood
>     
>   SequentialDecisionProblems.CoreTheory.reward t x y (e, H, A, B)  =  one * badOverGood  +  one  * highOverGood
>     
>   SequentialDecisionProblems.CoreTheory.reward t x y (e, L, U, G)  =  one                +  one  * lowOverGoodUnavailable
>     
>   SequentialDecisionProblems.CoreTheory.reward t x y (e, L, U, B)  =  one * badOverGood  +  one  * lowOverGoodUnavailable
>
>   SequentialDecisionProblems.CoreTheory.reward t x y (e, L, A, G)  =  one                +  one  * lowOverGoodAvailable
>     
>   SequentialDecisionProblems.CoreTheory.reward t x y (e, L, A, B)  =  one * badOverGood  +  one  * lowOverGoodAvailable


\subsection{Completing the specification}

In order to apply the verified, generic backwards induction algorithm of
|CoreTheory| to compute optimal policies for our problem, we have to
explain how the decision maker accounts for uncertainties on rewards
induced by uncertainties in the transition function. We assume that the
decision maker measures uncertain rewards by their expected value:

> SequentialDecisionProblems.CoreTheory.meas = expectedValue

> SequentialDecisionProblems.FullTheory.measMon = monotoneExpectedValue

Further on, we have to implement the notions of viability and
reachability. We start by positing that all states are viable for any
number of steps (remember |Viable : (n : Nat) -> X t -> Type|):

> SequentialDecisionProblems.CoreTheory.Viable n x = Unit

From this definition, it trivially follows that all elements of an
arbitrary list of states are viable for an arbitrary number of steps:

> viableLemma : {t, n : Nat} -> (xs : List (State t)) -> All (Viable n) xs
> 
> viableLemma    Nil       =  Nil
> 
> viableLemma (  x :: xs)  =  () :: (viableLemma xs)

This fact and the (less trivial) result that simple probability
distributions are never empty, see |nonEmptyLemma| in
|MonadicProperties| in |SimpleProb|, allows us to show that the above
definition of |Viable| fulfills |viableSpec1| (remember that
|viableSpec1| is of type |(x : X t) -> Viable (S n) x -> GoodCtrl t x|):

> SequentialDecisionProblems.CoreTheory.viableSpec1 {t} {n} s v =
> 
>   MkSigma H (ne, av) where
>   
>     ne  :  SequentialDecisionProblems.CoreTheory.NotEmpty (nexts t s H)
>     
>     ne  =  nonEmptyLemma (nexts t s H)
>     
>     av  :  SequentialDecisionProblems.CoreTheory.All (Viable n) (nexts t s H)
>     
>     av  =  viableLemma (support (nexts t s H))

Because we have taken |Viable n x| to be the singleton type, |Viable| is
finite and decidable:

> -- SequentialDecisionProblems.Utils.finiteViable n x = finiteUnit

> SequentialDecisionProblems.Utils.decidableViable n x = decidableUnit

For reachability, we proceed in a similar way. We say that all states
are reachable

> SequentialDecisionProblems.CoreTheory.Reachable x' = Unit

which immediately implies (remember that |reachableSpec1| is of type |(x
: X t) -> Reachable x -> (y : Y t x) -> All Reachable (nexts t x y)|):

> SequentialDecisionProblems.CoreTheory.reachableSpec1 {t} x r y = all (nexts t x y) where
>   all : (sp : SimpleProb  (State (S t))) -> SequentialDecisionProblems.CoreTheory.All Reachable sp
>   all sp = all' (support sp) where
>     all' : (xs : List (State (S t))) -> Data.List.Quantifiers.All Reachable xs
>     all'    Nil       =  Nil
>     all' (  x :: xs)  =  () :: (all' xs)

and decidability of |Reachable|:

> SequentialDecisionProblems.TabBackwardsInduction.decidableReachable x = decidableUnit

Finally, we have to show that controls are finite (remember that
|finiteCtrl| is of type |(x : X t) -> Finite (Y t x)|):

> SequentialDecisionProblems.Utils.finiteCtrl t = finiteLowHigh

and, in order to use the fast, tail-recursive tabulated version of
backwards induction, that states are finite:

> SequentialDecisionProblems.TabBackwardsInduction.finiteState t = finiteTuple4 finiteFin finiteLowHigh finiteAvailableUnavailable finiteGoodBad


\subsection{Optimal policies and possible state-control sequences}

We can now apply the results of the |CoreTheory| and of the |FullTheory|
from |SequentialDecisionProblems| to compute verified optimal policies,
possible state-control sequences, etc. We want to be able to show the
outcome of the decision process. This requires implementing functions to
print states and controls:

> SequentialDecisionProblems.Utils.showState {t} (e, H, U, G)  =  "(" ++ show (finToNat e) ++ ",H,U,G)"
> 
> SequentialDecisionProblems.Utils.showState {t} (e, H, U, B)  =  "(" ++ show (finToNat e) ++ ",H,U,B)"
> 
> SequentialDecisionProblems.Utils.showState {t} (e, H, A, G)  =  "(" ++ show (finToNat e) ++ ",H,A,G)"
> 
> SequentialDecisionProblems.Utils.showState {t} (e, H, A, B)  =  "(" ++ show (finToNat e) ++ ",H,A,B)"
> 
> SequentialDecisionProblems.Utils.showState {t} (e, L, U, G)  =  "(" ++ show (finToNat e) ++ ",L,U,G)"
> 
> SequentialDecisionProblems.Utils.showState {t} (e, L, U, B)  =  "(" ++ show (finToNat e) ++ ",L,U,B)"
> 
> SequentialDecisionProblems.Utils.showState {t} (e, L, A, G)  =  "(" ++ show (finToNat e) ++ ",L,A,G)"
> 
> SequentialDecisionProblems.Utils.showState {t} (e, L, A, B)  =  "(" ++ show (finToNat e) ++ ",L,A,B)"

> SequentialDecisionProblems.Utils.showCtrl {t} {x} L = "L"
> 
> SequentialDecisionProblems.Utils.showCtrl {t} {x} H = "H"

With these in place, we can implement a program that reads the number of
decision steps from the command line, computes a verified optimal policy
sequence and outputs some statistics of possible trajectories and
expected sum of rewards.

> using implementation ShowNonNegDouble
>   
>   partial
>   
>   computation : { [STDIO] } Eff ()
>   
>   computation =
>   
>     do  putStr ("enter number of steps:\n")
>     
>         nSteps <- getNat
>         
>         putStrLn "nSteps (number of decision steps):"
>         
>         putStrLn ("  " ++ show nSteps)
>         
>        
>         putStrLn "computing optimal policies ..."
>         
>         ps <- pure (tabTailRecursiveBackwardsInduction Z nSteps)
>         
>              
>         putStrLn "computing possible state-control sequences ..."
>         
>         mxys <- pure (possibleStateCtrlSeqs (FZ, H, U, G) () () ps)
>         
>         
>         putStrLn "pairing possible state-control sequences with their values ..."
>         
>         mxysv <- pure (possibleStateCtrlSeqsRewards' mxys)
>         
>        
>         putStrLn "computing (naively) the number of possible state-control sequences ..."
>         
>         n <- pure (length (toList mxysv))
>         
>         
>         putStrLn "number of possible state-control sequences:"
>         
>         putStrLn ("  " ++ show n)
>         
>        
>         putStrLn "computing (naively) the most probable state-control sequence ..."
>         
>         xysv <- pure (naiveMostProbableProb mxysv)
>         
>         
>         putStrLn "most probable state-control sequence and its probability:"
>         
>         putStrLn ("  " ++ show xysv)
>         
>                        
>         putStrLn "sorting (naively) the possible state-control sequences ..."
>         
>         xysvs <- pure (naiveSortToList mxysv)
>         
>         
>         putStrLn "most probable state-control sequences (first 3) and their probabilities:"
>         
>         putStrLn (showlong (take 3 xysvs))
>         
>                        
>         putStrLn "measure of possible rewards:"
>         
>         putStrLn ("  " ++ show (meas (SequentialDecisionProblems.CoreTheory.fmap snd mxysv)))
>      
>         putStrLn "done!"

For a more comprehensive implementation, see |EmissionsGame2| in
|SequentialDecisionProblems.applications|.

\begin{shaded}
  \begin{exercise}
    \label{exercise11.1}    
    Compile this program with "make Lecture11.exe" from the command
    line. Run the program for 0, 1, 2, 4, 8 and 9 decision steps and
    annotate the run time. Put forward an hypothesis about the run time
    complexity in the number of decision steps. Check your hypothesis.
  \end{exercise} 
\end{shaded}

> partial
> 
> main : IO ()
> 
> main = run computation

