% -*-Latex-*-

%if False

> module Lecture6

> import Syntax.PreorderReasoning
> import Data.Vect
> import Control.Monad.Identity

> import Fun.Predicates
> import VeriFunctor.VeriFunctor
> import VeriMonad.VeriMonad

> import Lecture5

> %default total
> %auto_implicits off
> %access public export

> %hide Prelude.List.last

%endif

\setcounter{section}{5}

\stepcounter{section}
%\section{Monads, Interfaces, Monadic Dynamical Systems}

\subsection{Natural transformations, monads}

\begin{itemize}

  \item The implementations of |flow| and |trj| for |List| and |Prob|
  are, mutatis mutandis, the same.

  \item Any deterministic system can be represented by an equivalent
  non-deterministic or stochastic system and the other way round!

\end{itemize}

As it turns out, |Identity, List, Prob| are \emph{monads}. In category
theory, a monad is an \emph{endo-functor} |M| on a category ℂ together
with two \emph{natural transformations} |eta| and |mu| such that

\begin{minipage}{0.4\textwidth}
\begin{tikzcd}[row sep=large, column sep=large]
M \, X \arrow[r, "\eta \ (M \, X)"] \arrow[dr, "id"'] & M \, (M \, X) \arrow[d, "\mu \ X"] & M \, X \arrow[l, "M \, (\eta \, X)"'] \arrow[dl, "id"] \\
                                                      & M \, X                             & 
\end{tikzcd}
\end{minipage}
\hfill
\begin{minipage}{0.4\textwidth}
\begin{tikzcd}[row sep=large, column sep=large]
M \, (M \, (M \, X)) \arrow[r, "M \, (\mu \, X)"] \arrow[d, "\mu \, (M \, X)"'] & M \, (M \, X) \arrow[d, "\mu \, X"] \\
      M \, (M \, X)  \arrow[r, "\mu \, X"']                                     & M \, X
\end{tikzcd}
\end{minipage}

commute. A natural transformation |gamma| between two functors F and G
between categories 𝔸 and 𝔹, is a family of arrows in 𝔹 indexed by
objects in 𝔸 such that |(G f) . (eta X) = (eta Y) . (F f)| (left). For
|eta : X -> M X| and |mu : M (M X) -> M X| this condition is captured in
the middle and right diagrams:

\begin{minipage}{0.3\textwidth}
\begin{tikzcd}[row sep=large, column sep=large]
F \, X \arrow[r, "F \, f"] \arrow[d, "\gamma \, X"'] & F \, Y \arrow[d, "\gamma \, Y"] \\
G \, X \arrow[r, "G \, f"']                          & G \, Y
\end{tikzcd}
\end{minipage}
\hfill
\begin{minipage}{0.3\textwidth}
\begin{tikzcd}[row sep=large, column sep=large]
     X \arrow[r, "     f"] \arrow[d, "\eta \, X"'] &      Y \arrow[d, "\eta \, Y"] \\
M \, X \arrow[r, "M \, f"']                        & M \, Y
\end{tikzcd}
\end{minipage}
\hfill
\begin{minipage}{0.3\textwidth}
\begin{tikzcd}[row sep=large, column sep=large]
M \, (M \, X) \arrow[r, "M \, (M \, f)"] \arrow[d, "\mu \, X"'] & M \, (M \, Y) \arrow[d, "\mu \, Y"] \\
      M \, X  \arrow[r, "M \, f"']                              & M \ Y
\end{tikzcd}
\end{minipage}

Thus, a monad is a functor with the additional properties:

\begin{enumerate}

  \item  Naturality of |eta|: |(M f) . (eta X) = (eta Y) . f|.

  \item  Naturality of |mu|: |(M f) . (mu X) = (mu Y) . (M (M f))| .

  \item  Triangle left: |(mu X) . (eta (M X)) = id|.

  \item  Triangle right: |(mu X) . (M (eta X)) = id|.

  \item  Square: |(mu X) . (M (mu X)) = (mu X) . (mu (M X))|.

\end{enumerate}


\subsection{Interfaces, implementations and generic programming}

In Idris, the notions of functor and monad are encoded in a hierarchy of
\emph{interfaces}. Idris interfaces (in Haskell \emph{type classes})
factor in the common features (\emph{methods} and \emph{axioms}) of a
certain class of data types. For instance, the |Num| interface describes
the common features of data types that implement basic numerical
arithmetic:

< interface Num ty where
<
<   (+)  :  ty -> ty -> ty
<
<   (*)  :  ty -> ty -> ty
<
<   fromInteger : Integer -> ty

A data type for which |(+)|, |(*)| and |fromInteger| can be defined, can
be declared to be an \emph{implementation} (or an \emph{instance}) of
|Num|. For example, |Nat|, |Int| and |Double| are all implementation of
|Num|. \\

For a specific data type, this is done by defining |(+)|, |(*)| and
|fromInteger| for that type. For instance, for |Nat|:

< implementation Num Nat where
<
<   (+)  =  plus
<  
<   (*)  =  mult
<
<   fromInteger = fromIntegerNat

where |plus, mult : Nat -> Nat -> Nat| and |fromIntegerNat : Integer ->
Nat| have to be completely defined and in scope. 

Another example of interfaces that we have already encountered in
lecture 5 is |Eq|. This represents the class of types that can be
compared for equality. Like |Num|, |Eq| is defined in the Idris prelude:

< interface Eq ty where
<
<   (==)  :  ty -> ty -> Bool
<
<   (/=)  :  ty -> ty -> Bool
<
<   x /= y  =  not (x == y)
<
<   x == y  =  not (x /= y)

Notice that |Eq| specifies \emph{default methods} for |(==)| and
|(/=)|. Implementations of |Eq| have to define one of them but they can
also define both. Idris interfaces can be \emph{refined}. For instance

< interface Num ty => Neg ty where
<
<   negate  :  ty -> ty
<
<   (-)     :  ty -> ty -> ty

requires implementations of |Neg| to implement the features of |Num|
plus |negate| and |(-)|. Implementations can also be derived
\emph{generically} from other implementations. For instance

< implementation (Eq a, Eq b) => Eq (a, b) where
<
<  (==) (a, c) (b, d) = (a == b) && (c == d)

explains how values of type |(a,b)| (for arbitrary types |a| and |b|)can
be compared for equality, provided that both values of type |a| and
values of type |b| can be compared for equality. \\

Interfaces are a powerful mechanism for \emph{generic programming}. One
can define functions that work for all implementations of one or more
interfaces. We have already seen an example with |nub|:

< *Lecture6> :t nub
<
< Prelude.List.nub : Eq a => List a -> List a

< *Lecture6> :t sum
<
< sum : Foldable t => Num a => t a -> a


\subsection{Functor and monad interfaces}

In category theory, a functor between categories 𝔸 and 𝔹, is a mapping
of objects and arrow of 𝔸 into objects and arrows of 𝔹 that preserves
identity and composition. \\

In discussing the notion of a monad, we have required |M| to be and
endo-functor on a category ℂ and used |X|, |Y|, |M X|, |M Y| (amd |M (M
X)|, |M (M (M Y))|, etc.) to denote objects in ℂ. Similarly, we have
used |M f|, |M (eta X)|, etc. to denote arrows, also in ℂ. \\

In Idris the notion of a monad is encoded in a hierarchy of
interfaces. Both for historical reasons and for reasons that we do not
have time to discuss in this course, this hierarchy is not as
straightforward as the category-theoretical notion. \\

To understanding monadic dynamical system and, more generally, the
computational theory of policy advice and verified, optimal decision
making discussed in \cite{2017_Botta_Jansson_Ionescu}, it is not
necessary to understand the details of this hierarchy. \\

However, it is useful to keep in mind the category-theoretical notions
of functor and monad and be comfortable with the basic interfaces that
encode these notions in Idris. These are, with some simplifications

< interface Functor (F : Type -> Type) where
<
<   map  :  (A -> B) -> F A -> F B

< interface Functor M => Monad (M : Type -> Type) where
<
<   pure   :  A -> M A
<
<   (>>=)  :  M A -> (A -> M B) -> M B
<
<   join   :  M (M A) -> M A

Thus, in Idris, the arrow mapping part of a functor |F| is called |map|
and the natural transformations |eta| and |mu| associated with a monad
|M| are called |pure| (or |return|) and |join|, respectively. The
operation |(>>=)| is usually referred to as ``bind'' and can be
derived from |join| and |map|.

Many Idris type constructors turn out to be monads. In particular, |List|
is a monad and |Prob| is a monad with |map|, |pure|, |(>>=)| and |join|
defined by |mapList|, |retList|, |bindList|, \dots, |joinProb| as discussed in
lecture 5.

Traditionally, the Idris |Functor| and |Monad| interfaces specify only
methods, not properties. These are collected in suitable refinements of
the base interfaces: |VeriFunctor| and |VeriMonad|. Thus, a
|VeriFunctor| is a |Functor| whose |map| preserves identity, composition
and extensional equality:

< interface Functor F => VeriFunctor (F : Type -> Type) where
< 
<   mapPresId     :  ExtEq (map id) id
<   
<   mapPresComp   : (f : A -> B) -> (g : B -> C) -> ExtEq (map (g . f)) (map g . map f)
<                 
<   mapPresExtEq  : (f, g : A -> B) -> ExtEq f g -> ExtEq (map f) (map g)

In the above interface, |ExtEq| is a property of functions of the same
type:

< ExtEq : (f, g : A -> B) -> Type
<
< ExtEq f g = (a : A) -> f a = g a

Thus, |mapPresId| posits that |map id fa = id fa = fa| for arbitrary
|fa|. 

\begin{shaded}
  \begin{exercise}
    \label{exercise6.1}    
    Implement |mapPresId| for |F = List|, |map = mapList| and |mapList|
    defined as in lecture 5.
  \end{exercise}
\end{shaded}

Similarly, |mapPresComp| posits that for arbitrary types |A|, |B| and
|C|, for every |f : A -> B| and |g : B -> C| and for every |fa| of
suitable type

< map (g . f) fa = (map g . map f) fa = map g (map f fa)

\begin{shaded}
  \begin{exercise}
    \label{exercise6.2}    
    What is the type of |fa| in the above equation? What is the type of |map f fa|?
  \end{exercise}
\end{shaded}

The last axiom of |VeriFunctor| requires |map| to preserve extensional
equality. We will come back to this axiom later in this lecture. For the
time being, we remark that all functors encountered so far fulfill this
axiom.

\begin{shaded}
  \begin{exercise}
    \label{exercise6.3}    
    Implement |mapPresExtEq| for |F = List|, |map = mapList| and |mapList|
    defined as in lecture 5.
  \end{exercise}
\end{shaded}

Consistently with the category-theoretical characterization of monads discussed
above, a |VeriMonad| is then a |VeriFunctor| together with two natural
transformations |eta| and |mu| that fulfill the monadic axioms 1-5. In
Idris, |eta| is called |pure| (or |ret|) and |mu| is called |join|:

< interface (VeriFunctor M, Monad M) => VeriMonad (M : Type -> Type) where
<
<   pureNatTrans     :  (f : A -> B) -> ExtEq (map f . pure) (pure . f)
<
<   joinNatTrans     :  (f : A -> B) -> ExtEq (map f . join) (join . map (map f))              
<
<   triangleLeft     :  ExtEq (join . pure) id
<
<   triangleRight    :  ExtEq (join . map pure) id         
<
<   squareLemma      :  ExtEq (join . map join) (join . join)         
<
<   bindJoinMapSpec  :  (f : A -> M B) -> ExtEq (>>= f) (join . map f)

The last axiom of |VeriMonad| posits that |ma >>= f = join (map f ma)|
for all |ma| and |f| of suitable types. The definition of |bindList|
from lecture 5 (with flipped arguments) fulfills this axiom. The axioms
of |VeriMonad| allow to derive a number of important, generic
results. In the rest of this lecture, we will make use of the following
ones:

< ||| ∀ f, ∀ g, (∀ a, f a = g a) => (∀ ma, ma >>= f = ma >>= g)
<
< bindPresExtEq  :  {M : Type -> Type} -> {A, B : Type} -> (VeriMonad M) => 
<
<                   (f, g : A -> M B) -> ExtEq f g -> ExtEq (>>= f) (>>= g) 

< ||| ∀ f, ∀ a,  (pure a) >>= f = f a
<
< leftIdentity  :  {M : Type -> Type} -> {A, B : Type} -> (VeriMonad M) => 
<
<                  (f : A -> M B) -> ExtEq (\ a => (pure a) >>= f) f

< ||| ∀ ma,  ma >>= pure = ma
< 
< rightIdentity  :  {M : Type -> Type} -> {A : Type} -> (VeriMonad M) => 
< 
<                   ExtEq {A = M A} (>>= pure) id

< ||| ∀ f, ∀ g, ∀ ma,  (ma >>= f) >>= g = ma >>= (\ a => (f a) >>= g)
< 
< associativity  :  {M : Type -> Type} -> {A, B, C : Type} -> (VeriMonad M) => 
< 
<                   (f : A -> M B) -> (g : B -> M C) -> 
<                 
<                   ExtEq (\ ma => (ma >>= f) >>= g) (>>= (\ x => (f x) >>= g))

< ||| ∀ f, ∀ g, ∀ ma,  map f (ma >>= g) = ma >>= map f . g
< 
< mapBindLemma  :  {M : Type -> Type} -> {A, B, C : Type} -> (VeriMonad M) => 
< 
<                  (f : B -> C) -> (g : A -> M B) -> 
<                
<                  ExtEq {A = M A} (\ ma => map f (ma >>= g)) (>>= map f . g)

Implementations can be found in the |VeriMonad| component of
\cite{botta20162020}.

The notion of monad is fundamental in computer
science and its usage and applications are ubiquitous in functional
programming languages. Among others, monads support the implementation
of functional programs in an imperative style via the so-called
\emph{do} notation:

> m_add : Maybe Int -> Maybe Int -> Maybe Int
> 
> m_add x y = do  x' <- x         -- Extract value from x
> 
>                 y' <- y         -- Extract value from y
>                
>                 pure (x' + y')  -- Add them

The data type |Maybe| which allows to model partial functions in a
controlled fashion 

< data Maybe : (a : Type) -> Type where
<
<   Nothing  :  Maybe a
<
<   Just     :  (x : a) -> Maybe a 

is a monad, and |(>>=)| for |Maybe| fulfills the specification

<    Nothing  >>= f  =  Nothing
<
< (  Just x)  >>= f  =  f x

and the |do| expression on the RHS of |m_add x y =| above is syntactic
sugar for

< m_add : Maybe Int -> Maybe Int -> Maybe Int
< 
< m_add x y = x >>= (\ x' => (y >>= (\ y' => pure (x' + y'))))

\begin{shaded}
  \begin{exercise}
    \label{exercise6.4}    
    What is the result of |m_add (Just 2) Nothing|? Apply the
    definition of |m_add| to give an semi-formal proof of the result
    by equational reasoning.
  \end{exercise}
\end{shaded}

The do-notation is also the basis for \emph{list comprehension} as for
instance in

< *Lecture6> [2 * i | i <- [3, 0, 1]]
<
< [6, 0, 2] : List Integer

More generally, monads are used to encapsulate various kinds of
``computational effects'' and thereby allow to model computations with
side-effects in a purely functional setting. The idea to use monads
for this purpose goes back to a seminal paper by Moggi
\cite{DBLP:conf/lics/Moggi89} and was further popularized by Wadler
\cite{DBLP:conf/nato/Wadler92}.  


\subsection{Monadic systems}

The notion od \emph{monadic dynamical system} was originally introduced
by C. Ionescu in \cite{ionescu2009}. \\

In a nutshell, the idea is to account for different kinds of uncertainty
in dynamical systems -- deterministic, non-deterministic, stochastic,
etc. as discussed in lecture 5 -- in a seamless way. \\

This also makes it possible to prove important results (like for
instance the representation theorems of lecture 5) for the general case
and avoid tedious and error-prone repetitions. We follow essentially the
pattern of definitions and proofs put forward in lecture 5.\\

\subsubsection{Preliminaries}

A discrete monadic dynamical system on a set |X| is a function
of type |X -> M X| where |M| is a monad:

> MonadicSys : (M : Type -> Type) -> Type -> Type
> 
> MonadicSys M X = X -> M X

The set |X| in |f : MonadicSys M X| is often called the "state space" of the
system |f|:

> StateSpace : {M : Type -> Type} -> {X : Type} -> MonadicSys M X -> Type
> 
> StateSpace = Domain

Every deterministic system on |X| can be represented by a monadic
systems on |X|:

> embed : {M : Type -> Type} -> {X : Type} -> Monad M => DetSys X -> MonadicSys M X
> 
> embed f = pure . f

\subsubsection{Flow}

The flow of a monadic system |f| over |t| steps is another monadic
system:

> flow  :  {M : Type -> Type} -> {X : Type} -> VeriMonad M => 
> 
>          Nat -> MonadicSys M X -> MonadicSys M X
>          
> flow    Z     f x  =  pure x
> 
> flow (  S n)  f x  =  f x >>= flow n f

Trivially, one has |flow Z f = pure|:

> flowLemma1  :  {M : Type -> Type} -> {X : Type} -> VeriMonad M => 
> 
>                (f : MonadicSys M X) -> ExtEq (flow Z f) pure
>                
> flowLemma1 f x = Refl

and also |flow (t + t') f x = flow t f x >>= flow t' f|:

> flowLemma2  :  {M : Type -> Type} -> {X : Type} -> {m, n : Nat} -> VeriMonad M => 
> 
>                (f : MonadicSys M X) -> ExtEq (flow (m + n) f) (\ x => flow m f x >>= flow n f)
>
> flowLemma2 {m = Z} {n} f x  =
>
>       ( flow (Z + n) f x )
>
>   ={  Refl }=
>                            
>       ( flow n f x )
>                              
>   ={  sym (leftIdentity (flow n f) x) }=
>                            
>       ( pure x >>= flow n f )
>                              
>   ={  Refl }=
>                            
>       ( flow Z f x >>= flow n f )
>                              
>   QED

> flowLemma2 {m = S l} {n} f x  =   
> 
>       ( flow (S l + n) f x )
> 
>   ={  Refl }=
>                               
>       ( f x >>= flow (l + n) f )
>                                  
>   ={  bindPresExtEq  (flow (l + n) f) (\ y => flow l f y >>= flow n f) (flowLemma2 f) (f x) }=
>                               
>       ( f x >>= (\ y => flow l f y >>= flow n f) )
>                                  
>   ={  sym (associativity (flow l f) (flow n f) (f x)) }=
>                               
>       ( (f x >>= flow l f) >>= flow n f )
>                                  
>   ={  Refl }=
>                               
>       ( flow (S l) f x >>= flow n f )
>                                  
>   QED

Every monadic system |f : MonadicSys M X| can be represented by an
equivalent deterministic systems on |M X|

> repr : {M : Type -> Type} -> {X : Type} -> VeriMonad M => MonadicSys M X -> DetSys (M X)
> 
> repr f xs = xs >>= f

> flowDetSys : {X : Type} -> Nat -> DetSys X -> DetSys X
> flowDetSys = Lecture5.flow

> reprLemma  :  {M : Type -> Type} -> {X : Type} -> VeriMonad M => 
> 
>               (n : Nat) -> (f : MonadicSys M X) -> 
>               
>               ExtEq {A = M X} (>>= flow n f) (flowDetSys n (repr f))
>                          
> reprLemma Z f mx  =   
> 
>       ( mx >>= flow Z f )
> 
>   ={  bindPresExtEq (flow Z f) pure (flowLemma1 f) mx }=
>                   
>       ( mx >>= pure )
>                      
>   ={  rightIdentity mx }=  
>                   
>       ( mx )
>                      
>   ={  Refl }=  
>                   
>       ( flowDetSys Z (repr f) mx )
>                      
>   QED
>
> reprLemma (S m) f xs  =   
> 
>       ( xs >>= flow (S m) f )
> 
>   ={  bindPresExtEq  (flow (S m) f) (\ x => f x >>= flow m f) (\ x => Refl) xs }=
>                       
>       ( xs >>= (\ x => f x >>= flow m f) )
>                          
>   ={  sym (associativity f (flow m f) xs) }=
>                       
>       ( (xs >>= f) >>= flow m f )
>                          
>   ={  reprLemma m f (xs >>= f) }=
>                       
>       ( (flowDetSys m (repr f)) (xs >>= f) )
>                          
>   ={  Refl }=
>                       
>       ( (flowDetSys m (repr f)) ((repr f) xs) )
>                          
>   ={  Refl }=
>                       
>       ( flowDetSys (S m) (repr f) xs )
>                          
>   QED

\subsubsection{Trajectories}

For a dynamical system |f : MonadicSys M X|, the trajectories of length |n :
Nat| starting at |x : X| are

> trj  :  {M : Type -> Type} -> {X : Type} -> VeriMonad M => 
> 
>         (n : Nat) -> MonadicSys M X -> X -> M (Vect (S n) X)
>         
> trj    Z     f x  =  map (x ::) (pure Nil)
> 
> trj (  S n)  f x  =  map (x ::) ((f x) >>= trj n f) 

Remember that for deterministic systems the last state of the trajectory
of length |n| starting in |x| is |flow n f x|. \\

In the general, monadic case |trj n f x| is an |M|-structure of vectors.
But mapping |last| on |trj n f x| yields, again, |flow n f|:

> flowTrjLemma  :  {M : Type -> Type} -> {X : Type} -> VeriMonad M => 
> 
>                  (n : Nat) -> (f : MonadicSys M X) -> 
>                  
>                  ExtEq (flow n f) (map {a = Vect (S n) X} last . (trj n f))

To prove this result, we first derive the auxiliary lemma 

> mapLastLemma  :  {M : Type -> Type} -> {X : Type} -> {n : Nat} -> VeriMonad M => 
> 
>                  (x : X) -> ExtEq {A = M (Vect (S n) X)} (map last . map (x ::)) (map last)
>                
> mapLastLemma {X} {n} x mvs  =  
> 
>       ( map last (map (x ::) mvs) )
> 
>   ={  sym (mapPresComp {A = Vect (S n) X} (x ::) last mvs) }=
>                             
>       ( map (last . (x ::)) mvs )
>                                
>   ={  mapPresExtEq (last . (x ::)) last (lastLemma x) mvs }=
>                             
>       ( map last mvs )
>                                
>   QED

And finally implement |flowTrjLemma n f| by induction on |n|:

> flowTrjLemma {X} Z f x  =  
> 
>       ( flow Z f x )
> 
>   ={  Refl }=
>                         
>       ( pure x )
>                            
>   ={  Refl }=
>                         
>       ( pure (last (x :: Nil)) )
>                            
>   ={  sym (pureNatTrans last (x :: Nil)) }=
>                         
>       ( map last (pure (x :: Nil)) )
>                            
>   ={  cong {f = map last} (sym (pureNatTrans {A = Vect Z X} (x ::) Nil)) }=
>                         
>       ( map last (map (x ::) (pure Nil)) )
>                            
>   ={  Refl }=
>                         
>       ( map last (trj Z f x) )
>                            
>   QED
>
> flowTrjLemma (S m) f x  =  
> 
>       ( flow (S m) f x )
> 
>   ={  Refl }=
>                         
>       ( f x >>= flow m f )
>                            
>   ={  bindPresExtEq (flow m f) (map last . (trj m f)) (flowTrjLemma m f) (f x) }=
>                         
>       ( f x >>= map last . (trj m f) )
>                            
>   ={  sym (mapBindLemma last (trj m f) (f x)) }=
>                         
>       ( map last ((f x) >>= trj m f) )
>                            
>   ={  sym (mapLastLemma x ((f x) >>= trj m f)) }=
>                         
>       ( map last (map (x ::) ((f x) >>= trj m f)) )
>                            
>   ={  Refl }=
>                         
>       ( map last (trj (S m) f x) )
>                            
>   QED


\subsection{Time dependent dynamical system}

In many important applications, one has to deal with dynamical systems
in which |X| can be different at different iteration steps.

For example, in lecture 1 we have sketched a sequential decision problem
in which

\begin{itemize}

\item At the first decision step, the decision maker observes
  \textcolor{red}{zero} cumulated emissions, high
  \textcolor{red}{current} emissions, \textcolor{red}{unavailable}
  technologies and a \textcolor{red}{good} world.

\item ... if the cumulated emissions increase beyond a
  \textcolor{red}{critical threshold}, the probability that the world
  becomes bad steeply \textcolor{red}{increases}.

\end{itemize}

This suggests that the set of values that cumulated emissions can
take, i.e.\ the \emph{type} of cumulated emissions might change as the
system evolves. \\

For concreteness, assume that at each step the cumulated emissions can
only increase by one. Let |e| denote the cumulated emissions. Then we
have the following situation

< at step 0, |e ∈ {0}|
<
< at step 1, |e ∈ {0,1}|
<
< at step n, |e ∈ {0..n}|

if cumulated emissions are a component of a type |X| that represents the
set of observable states of a system, then |X| will depend on an
iteration counter. Formally

> X     :  Nat -> Type

A monadic dynamical system on |X| could then be specified in terms of a
monad |M|

> M     :  Type -> Type

and of a transition function |next|

> next  :  (t : Nat) -> X t -> M (X (S t))

Here the variable |t| denotes an iteration counter. In case |X t|
entails a notion of time or, in other words, if we have a function

> time : {t : Nat} -> X t -> Nat

that associate a time to state values, we can formalize the idea that
the system evolves forwards (in time) by requiring

> nextMonInc  :  (t, t' : Nat) -> (x : X t) -> (x' : X t') -> t `LTE` t' -> time x `LTE` time x'

Similarly we can require a system to evolve backwards in time. 


\subsection{Decision making under uncertainty}

In decision making problems, one has to do with systems whose evolution
depends both on the system's state and on the options available to the
decision maker. \\

In control theory, the options are called controls. They typically
depend on the systems's state that is, the options available to the
decision maker can be fifferent in different states. \\

\textbf{Example:} A central bank can typically increase or decrease the
interest rates. But the amount by which a central bank is able to do so,
can depend on the current interest rates and perhaps on other economic
observables like for instance growth and unemployment or other measures
and indicators.

\textbf{Example:} A country might be able to increase or decrease the
emissions of certain dangerous pollutants. But the options available to
decision makers might depend on the availability or non-availability of
effective filtering technologies, on the state of the economy or perhaps
on the actual level of pollutant.

It is not difficult to modify the notion of a time-dependent monadic
dynamical system to represent decision making problems. All we need to
do is to introduce the notion of (possibly state-dependent) controls

> Y     :  (t : Nat) -> X t -> Type

The transition function of the system at step |t| will then depend both
on the current state |x : X t| and on the control |y : Y t x| selected

< X     :  Nat -> Type

< Y     :  (t : Nat) -> X t -> Type

< M     :  Type -> Type

< next  :  (t : Nat) -> (x : X t) -> Y t x -> M (X (S t))

In decision making under uncertainty, |X|, |Y|, |M| and |next| are
typically given and the problem is that of finding sequences of controls
such that the resulting trajectories fulfill certain conditions. \\


\subsection{Coming up}

In the next lecture we will start formalizing finite horizon sequential
decision problems for the deterministic case. These problems are at the
core of \emph{dynamic programming} as originally proposed by Bellman in
1957 \cite{bellman1957} and the first step towards optimal decision
making under uncertainty.


% \section*{Solutions}


\pagebreak