% -*-Latex-*-

%if False

> module Lecture5

> import Syntax.PreorderReasoning
> import Data.Vect
> import Control.Monad.Identity

> import Real.Postulates
> import Fun.Predicates
> import Fun.Properties

> %default total
> %auto_implicits off
> %access public export

%endif

\setcounter{section}{4}


\stepcounter{section}
%\section{Time discrete dynamical systems}

With basic ideas about problem specification and the support of a
specification language, we turn back to the problem of understanding
decision making under uncertainty. \\

As a first step, we look at \emph{time discrete deterministic dynamical
systems}. This notion is fundamental in modelling, in particular in
earth system modelling. This is because of two reasons:

\begin{itemize}

  \item Continuous dynamical systems have typically to be
  \emph{approximated}. This is done in terms of discrete systems.

  \item Deterministic dynamical systems are special cases of more
  general non-deterministic, stochastic, fuzzy, etc. systems.

\end{itemize}


\subsection{Discrete deterministic dynamical systems}

A time discrete deterministic dynamical system (in short, a
\emph{deterministic system}) on a set |X| is a function of type |X ->
X|. \\

Remember that in dependently typed languages sets and propositions are
encoded through types. Thus, a natural formalization of the notion is

> DetSys : Type -> Type
>
> DetSys X = X -> X

We can also introduce the notion (of a discrete deterministic dynamical
system) through a data declaration

< data DetSys : Type -> Type where
<
<   MkDetSys : {X : Type} -> (X -> X) -> DetSys X

A specific system on |X| is then declared to be a value of type |DetSys
X|. The domain of |f : DetSys X| is often called the \emph{state space} of
|f|:

> StateSpace : {X : Type} -> DetSys X -> Type
>
> StateSpace = Domain

The most obvious operation that we can do with a system is to iterate it
a certain number of steps. This is often called the \emph{flow} of the
system:

> flow : {X : Type} -> Nat -> DetSys X -> DetSys X
>
> flow    Z     f  x =  x
>
> flow (  S n)  f  x =  flow n f (f x)

Notice that |flow n f| can also be defined in a point-free notation

< flow : {X : Type} -> Nat -> DetSys X -> DetSys X
<
< flow    Z     f  =  id
<
< flow (  S n)  f  =  (flow n f) . f

and that |flow n f| has the same type as |f|. In physics, the
standard notation for |flow n f| is $f^n$.

\begin{shaded}
  \begin{exercise}
    \label{exercise5.1}
    Encode the mathematical specification
    \begin{quote}
      |∀ m, n ∈ Nat|, \ |f : DetSys X|, \ |x ∈ X|, \ |flow (m + n) f x =
      flow n f (flow m f x)|
    \end{quote}
    in Idris through the type of an |flowSpec| value.
  \end{exercise}
\end{shaded}

\begin{shaded}
  \begin{exercise}
    \label{exercise5.2}
    Implement |flowSpec| by pattern matching on |m|.
  \end{exercise}
\end{shaded}

Another fundamental notion in dynamical systems theory is that of
the \emph{trajectory} (of a dynamical system) starting at a certain
\emph{initial} state:

> trj : {X : Type} -> (n : Nat) -> DetSys X -> X -> Vect (S n) X
>
> trj    Z     f  x  = x :: Nil
>
> trj (  S n)  f  x  = x :: trj n f (f x)

\begin{shaded}
  \begin{exercise}
    \label{exercise5.3}
    |trj| fulfills a specification similar to |flowSpec|. Encode this
    specification in the type of a function |trjSpec| using only
    |flow|, |tail : Vect (S n) X -> Vect n X| and vector concatenation.
  \end{exercise}
\end{shaded}

\begin{shaded}
  \begin{exercise}
    \label{exercise5.4}
    Implement |trjSpec| on the basis of

> postulate trjLemma1  :  {X : Type} -> (m : Nat) -> (f : DetSys X) -> (x : X) ->
>
>                         head (trj m f x) = x

> postulate trjLemma2  :  {X : Type} -> (m : Nat) -> (f : DetSys X) -> (x : X) ->
>
>                         tail (trj (S m) f x) = trj m f (f x)

    and

> postulate headTailLemma  :  {n : Nat} -> {A : Type} ->
>
>                             (xs : Vect (S n) A) -> head xs :: tail xs = xs

  \end{exercise}
\end{shaded}

Perhaps not surprisingly, the last element of the trajectory of length
|n| of |f : DetSys X| starting in |x| is just |flow n f x|:

> flowTrjLemma  :  {X : Type} ->
>
>                  (n : Nat) -> (f : DetSys X) ->
>
>                  (x : X) -> flow n f x = last (trj n f x)

\begin{shaded}
  \begin{exercise}
    \label{exercise5.5}
    Implement |flowTrjLemma| on the basis of

> postulate lastLemma  :  {A : Type} -> {n : Nat} ->
>                         (x : A) -> (xs : Vect (S n) A) -> last (x :: xs) = last xs

  \end{exercise}
\end{shaded}


\subsection{Discrete non-deterministic dynamical systems}

What if the outcome of a system is uncertain? In this case, for a given
|x : X| we can have more than one \emph{possible} next state. \\

If we do not have any additional information, we say that the system is
\emph{non-deterministic}. In this case, we can represent all possible
next states by a list:

> NonDetSys : Type -> Type
>
> NonDetSys X = X -> List X

Lists are equipped with so-called |return|, |join| and |map|
operations  

> retList : {A : Type} -> A -> List A
>
> retList x = x :: Nil

> joinList : {A : Type} -> List (List A) -> List A
>
> joinList    Nil         =  Nil
>
> joinList (  xs :: xss)  =  xs ++ joinList xss

> mapList : {A, B : Type} -> (A -> B) -> List A -> List B
>
> mapList f    Nil       = Nil
> mapList f (  x :: xs)  =  f x :: mapList f xs

that fulfill certain \emph{naturality} conditions. For instance,
|mapList f (retList x) = retList (f x)|:

< *Lecture5> retList 3
<
< [3] : List Integer

and

< *Lecture5> mapList (2 +) (retList 1) = retList (2 + 1)
<
< [3] = [3] : Type

and

< *Lecture5> joinList [[1,5,4], [3,4,9]]
<
< [1, 5, 4, 3, 4, 9] : List Integer

\textbf{Remark:} Every deterministic system |f : X -> X| can be
represented by a non-deterministic system:

> embedDetIntoNonDet : {X : Type} -> DetSys X -> NonDetSys X
>
> embedDetIntoNonDet f = retList . f

Using |mapList| and |joinList| one can implement a function

> flowNonDetSys : {X : Type} -> (m : Nat) -> NonDetSys X -> NonDetSys X

that iterates a non-deterministic system:

> flowNonDetSys    Z     f x  =  retList x
>
> flowNonDetSys (  S m)  f x  =  joinList (mapList (flowNonDetSys m f) (f x))

Notice that, if we define

> bindList : {A, B : Type} -> (A -> List B) -> List A -> List B
>
> bindList f as = joinList (mapList f as)

the second clause of |flowNonDetSys| can be written as

< flowNonDetSys (S m)  f x  =  bindList (flowNonDetSys m f) (f x)

A comparison with the flow of deterministic systems

< flow (S m) f  x = flow m f (f x) = (flow m f) (f x) = ((flow m f) . f) x

suggests that |bindList| is a kind of evaluation. Consistently with
this interpretation, one has

\textbf{Lemma:} |∀ m, n ∈ Nat|, \ |f : NonDetSys X|, |x ∈ X|, \ |flow'
(m + n) f x = bindList (flow' n f) (flow' m f x)|  with |flow' = flowNonDetSys|.

We will prove the lemma in a more generic setup in lecture 6. Next,
consider

> repr : {X : Type} -> NonDetSys X -> DetSys (List X)
>
> repr = bindList

The function associates to any non-deterministic system on an arbitrary
type |X|, a deterministic system on |List X|. We say that |repr f| is
the deterministic representation of |f|. This terminology is justified
by the result:

> reprLemma  :  {X : Type} -> (n : Nat) -> (f : NonDetSys X) ->
>
>               (xs : List X) -> repr (flowNonDetSys n f) xs = flow n (repr f) xs

\begin{shaded}
  \begin{exercise}
    \label{exercise5.6} Implement |reprLemma| using:

> bindListPresExtEq  :  {A, B : Type} -> (f, g : A -> List B) ->
>
>                       ((a : A) -> f a = g a) ->
>
>                       ((as : List A) -> bindList f as = bindList g as)

> rightIdentityList : {A : Type} -> (as : List A) -> bindList retList as = as

> bindListAssociative  :  {A, B, C : Type} -> (f : A -> List B) -> (g : B -> List C) ->
>
>                         (as : List A) ->
>
>                         bindList (bindList g . f) as = bindList g (bindList f as)

  \end{exercise}
\end{shaded}

\begin{shaded}
  \begin{exercise}
    \label{exercise5.7}
    Implement |bindListPresExtEq| and |rightIdentityList|. Start by implementing

> mapListPresExtEq  :  {A, B : Type} -> (f, g : A -> List B) ->
>
>                      ((a : A) -> f a = g a) ->
>
>                      ((as : List A) -> mapList f as = mapList g as)

  \end{exercise}
\end{shaded}

\begin{shaded}
  \begin{exercise}
    \label{exercise5.8}
    The funcion |flowNonDetSys| produces a lot of duplicates. For
    instance, for

> f : Nat -> List Nat
>
> f  Z     =  [Z, S Z]
>
> f (S m)  =  [m, S m, S (S m)]

    one obtaines

< *Lecture5> flowNonDetSys 3 f Z
<
< [0, 1, 0, 1, 2, 0, 1, 0, 1, 2, 1, 2, 3] : List Nat

    The function |nub| eliminates list duplicates:

< *Lecture5> nub (flowNonDetSys 3 f Z )
<
< [0, 1, 2, 3] : List Nat

    Using |nub|, write a function |flowNonDetSys'| that produces no
    duplicates and runs faster (for a large enough number of iterations)
    than the original version.

  \end{exercise}
\end{shaded}

In much the same way as we can iterate non-deterministic systems a fixed
number of times, we can compute all the possible trajectories of fixed
length that start at a given initial value:

> trjNonDetSys : {X : Type} -> (n : Nat) -> NonDetSys X -> X -> List (Vect (S n) X)
>
> trjNonDetSys    Z     f  x  =  mapList (x ::) (retList Nil)
>
> trjNonDetSys (  S n)  f  x  =  mapList (x ::) (bindList (trjNonDetSys n f) (f x))

\begin{shaded}
  \begin{exercise}
    \label{exercise5.9}
    |trjNonDetSys| computes all the possible trajectories of a system
    starting from a given initial value. For instance

< *Lecture5> trjNonDetSys 0 f Z
<
< [[0]] : List (Vect 1 Nat)

< *Lecture5> trjNonDetSys 1 f Z
<
< [[0, 0], [0, 1]] : List (Vect 2 Nat)

< *Lecture5> trjNonDetSys 2 f Z
<
< [[0, 0, 0], [0, 0, 1], [0, 1, 0], [0, 1, 1], [0, 1, 2]] : List (Vect 3 Nat)

    Explain the implementation of |trjNonDetSys|. What is the type of |x
    ::|? What is the type of |Nil| on the RHS of |trjNonDetSys Z f x|?
    What are the types of |trjNonDetSys n f|, |f x| and |bindList
    (trjNonDetSys n f) (f x)|?  \end{exercise} \end{shaded}


\subsection{Discrete stochastic dynamical systems}

Sometimes we know enough about a system to be able to estimate its
transition \emph{probabilities}. \\

In this case, we say that the system is \emph{stochastic}. Stochastic
systems can be described by functions of type |X -> Prob X|. Here |Prob
X| represents the type of finite probability distributions on |X|:

> Prob : Type -> Type

We are not going to \emph{define} |Prob| in this lecture. Instead, we
\emph{specify} properties that finite probability distributions are
required to fulfill. \\

Let us first recall the basic notions of elementary probability theory,
that is, of probability theory for finite, non-empty sets. \\

In this context, \emph{events} are subsets of a finite, non-empty set
|X|: |Event X = PS X|. The set |X| represents the possible
\emph{outcomes} of a random process and a \emph{probability} is a function of
type |Event X -> Real| that fulfilsl the axioms (Kolmogorov, 1933):

\begin{enumerate}
  \item |∀ e ∈ Event X|, \ |P e >= 0|.
  \item |P empty = 0| and |P X = 1|.
  \item |∀ e, e' ∈ Event X|, \ |e setIntersect e' = empty ⇒ P (e setJoin e') = P e + P e'|.
\end{enumerate}

In elementary probability theory, a probability distribution on a
finite, non-empty set |X| is a function |PI : X -> Real| such that
$\sum_{x \in X}$ |PI x = 1|. \\

Thus, a probability distribution |PI : X -> Real| induces a probability
function |probPI : Event X -> Real| via |probPI e| = $\sum_{x \in e}$ |PI x|. \\

We can formalize this fragment of probability theory in Idris by
representing probability distributions on values of a type |X| by values
of type |Prob X|. \\

In this formalization, |X| does not need to be a finite type. But the
set of values of type |X| whose probability is non-zero has to be
finite. For |pd : Prob X|, we call this set the \emph{support} of |pd|:

> supp : {A : Type} -> Prob A -> List A

The probability associated with a probability distribution |pd : Prob X|
is then given by |prob pd| with

> prob : {A : Type} -> Prob A -> (A -> Bool) -> Real

where |prob pd e| represents the probability of the event |e| according
to |pd|. As in the case of non-deterministic systems, we require |Prob
X| to be equipped with |return|, |join| and |map| operations:

> retProb   :  {A : Type} -> A -> Prob A

> joinProb  :  {A : Type} -> Prob (Prob A) -> Prob A

> mapProb   :  {A, B : Type} -> (A -> B) -> Prob A -> Prob B

These have natural interpretations in probability theory. Thus,
|retProb| is the function that associates to any value |x| of an
arbitrary type |X| the probability distribution concentrated on |x|:

< prob (retProb x) e = 1 ifandonlyif e x = True
<
< prob (retProb x) e = 0 ifandonlyif e x = False

|joinProb| is the function that reduces probability distributions over
 probability distributions over |X| to probability distributions over
 |X|. It fulfills

< prob (joinProb pd2) e = sum [prob pd2 (is pd) * prob pd e | pd <- supp pd2]

which can be interpreted as the "law of total probability": here |is pd| is
the characteristic function of |pd|

> is : {A : Type} -> Eq A => A -> A -> Bool
> is a a' = a == a'

and thus, for |x != y|, |is x| and |is y| are disjoint events. With
|retProb|, |joinProb|, |mapProb| and

> StochSys : Type -> Type
>
> StochSys X = X -> Prob X

we can implement a function

> flowStochSys : {X : Type} -> (m : Nat) -> StochSys X -> StochSys X

that computes all the states that can be obtained by iterating a
stochastic system starting from an initial |x : X|. The implementation
can be derived by copy \& paste from |flowNonDet|:

> flowStochSys  Z     f x  =  retProb x
>
> flowStochSys (S m)  f x  =  joinProb (mapProb (flowStochSys m f) (f x))

Similarly, one can implement |bind|, |repr|, |reprLemma|, |trj|,
etc. for stochastic systems with obvious interpretations. \\

In the next lecture we will amalgamate the commonalities between
deterministic, non-deterministic and stochastic systems in the notion of
\emph{monadic} dynamical systems. \\

Monadic dynamical systems allow one to account for different kinds of
uncertainties in a simple and seamless way.


\section*{Solutions}


\subsubsection*{Exercise \ref{exercise5.1}:}

> flowSpec  :  {X : Type} -> (m : Nat) -> (n : Nat) -> (f : DetSys X) -> (x : X) ->
>
>              flow (m + n) f x = flow n f (flow m f x)


\subsubsection*{Exercise \ref{exercise5.2}:}

> flowSpec Z n f x  =   ( flow (Z + n) f x )
>
>                   ={  Refl }=
>
>                       ( flow n f x )
>
>                   ={  Refl }=
>
>                       ( flow n f (flow Z f x) )
>
>                   QED

> flowSpec (S m) n f x  =   ( flow ((S m) + n) f x )
>
>                       ={  Refl }=
>
>                           ( flow (S (m + n)) f x )
>
>                       ={  Refl }=
>
>                           ( flow (m + n) f (f x) )
>
>                       ={  flowSpec m n f (f x) }=
>
>                           ( flow n f (flow m f (f x)) )
>
>                       ={  Refl }=
>
>                           ( flow n f (flow (S m) f x) )
>
>                       QED


\subsubsection*{Exercise \ref{exercise5.3}:}

> trjSpec  :  {X : Type} -> (m : Nat) -> (n : Nat) -> (f : DetSys X) -> (x : X) ->
>
>             trj (m + n) f x = trj m f x ++ tail (trj n f (flow m f x))


\subsubsection*{Exercise \ref{exercise5.4}:}

> trjSpec Z n f x  =  ( trj (Z + n) f x )
>
>                  ={ Refl }=
>
>                     ( trj n f x )
>
>                  ={ sym (headTailLemma (trj n f x)) }=
>
>                     ( head (trj n f x) :: tail (trj n f x) )
>
>                  ={ replace {P = \ X  =>  head (trj n f x) :: tail (trj n f x)
>                                             =
>                                           X :: tail (trj n f x)}
>
>                                           (trjLemma1 n f x) Refl }=
>
>                     ( x :: tail (trj n f x) )
>
>                  ={ Refl }=
>
>                     ( (x :: Nil) ++ tail (trj n f x) )
>
>                  ={ Refl }=
>
>                     ( trj Z f x ++ tail (trj n f (flow Z f x)) )
>
>                  QED

> trjSpec (S m) n f x  =  ( trj ((S m) + n) f x )
>
>                      ={ Refl }=
>
>                         ( trj (S (m + n)) f x )
>
>                      ={ Refl }=
>
>                         ( x :: trj (m + n) f (f x) )
>
>                      ={ replace {P = \ X => x :: trj (m + n) f (f x) = x :: X}
>
>                                 (trjSpec m n f (f x)) Refl }=
>
>                         ( x :: (trj m f (f x) ++ tail (trj n f (flow m f (f x)))) )
>
>                      ={ Refl }=
>
>                         ( (x :: trj m f (f x)) ++ tail (trj n f (flow m f (f x))) )
>
>                      ={ Refl }=
>
>                         ( trj (S m) f x ++ tail (trj n f (flow m f (f x))) )
>
>                      ={ Refl }=
>
>                         ( trj (S m) f x ++ tail (trj n f (flow (S m) f x)) )
>
>                      QED


\subsubsection*{Exercise \ref{exercise5.5}:}

> flowTrjLemma Z f x  =  ( flow Z f x )
>
>                     ={ Refl }=
>
>                        ( x )
>
>                     ={ Refl }=
>
>                        ( last (trj Z f x) )
>
>                     QED

> flowTrjLemma (S m) f x  =  ( flow (S m) f x )
>
>                         ={ Refl }=
>
>                            ( (flow m f) (f x) )
>
>                         ={ flowTrjLemma m f (f x) }=
>
>                            ( last (trj m f (f x)) )
>
>                         ={ sym (lastLemma x (trj m f (f x))) }=
>
>                            ( last (x :: trj m f (f x)) )
>
>                         ={ Refl }=
>
>                            ( last (trj (S m) f x) )
>
>                         QED


\subsubsection*{Exercise \ref{exercise5.6}:}

> reprLemma Z f xs  =  ( repr (flowNonDetSys Z f) xs )
>
>                   ={ Refl }=
>
>                      ( bindList (flowNonDetSys Z f) xs )
>
>                   ={ bindListPresExtEq (flowNonDetSys Z f) retList (\ a => Refl) xs }=
>
>                      ( bindList retList xs )
>
>                   ={ rightIdentityList xs }=
>
>                      ( xs )
>
>                   ={ Refl }=
>
>                      ( flow Z (repr f) xs )
>
>                   QED

> reprLemma (S m) f xs  =  ( repr (flowNonDetSys (S m) f) xs )
>
>                       ={ Refl }=
>
>                          ( bindList (flowNonDetSys (S m) f) xs )
>
>                       ={ bindListPresExtEq  (flowNonDetSys (S m) f)
>                                             (\ x => bindList (flowNonDetSys m f) (f x))
>                                             (\ x => Refl)
>                                             xs }=
>
>                          ( bindList (\ x => bindList (flowNonDetSys m f) (f x)) xs )
>
>                       ={ bindListAssociative f (flowNonDetSys m f) xs }=
>
>                          ( bindList (flowNonDetSys m f) (bindList f xs) )
>
>                       ={ Refl }=
>
>                          ( repr (flowNonDetSys m f) (bindList f xs) )
>
>                       ={ reprLemma m f (bindList f xs) }=
>
>                          ( flow m (repr f) (bindList f xs) )
>
>                       ={ Refl }=
>
>                          ( flow m (repr f) (repr f xs) )
>
>                       ={ Refl }=
>
>                          ( flow (S m) (repr f) xs )
>
>                       QED
>

\subsubsection*{Exercise \ref{exercise5.7}:}

< mapListPresExtEq  :  {A, B : Type} -> (f, g : A -> List B) ->
<
<                      ((a : A) -> f a = g a) ->
<
<                      ((as : List A) -> mapList f as = mapList g as)

> mapListPresExtEq f g p    Nil       =  Refl
>
> mapListPresExtEq f g p (  a :: as)  =  ( mapList f (a :: as) )
>
>                                     ={ Refl }=
>
>                                        ( f a :: mapList f as )
>
>                                     ={ cong {f = \ α => α :: mapList f as} (p a) }=
>
>                                        ( g a :: mapList f as )
>
>                                     ={ cong (mapListPresExtEq f g p as) }=
>
>                                        ( g a :: mapList g as )
>
>                                     ={ Refl }=
>
>                                        ( mapList g (a :: as) )
>
>                                     QED

< bindListPresExtEq  :  {A, B : Type} -> (f, g : A -> List B) ->
<
<                       ((a : A) -> f a = g a) ->
<
<                       ((as : List A) -> bindList f as = bindList g as)

> bindListPresExtEq  f g p as  =  ( bindList f as )
>
>                              ={ Refl }=
>
>                                 ( joinList (mapList f as) )
>
>                              ={ cong (mapListPresExtEq f g p as) }=
>
>                                 ( joinList (mapList g as) )
>
>                              ={ Refl }=
>
>                                 ( bindList g as )
>
>                              QED

< rightIdentityList : {A : Type} -> (as : List A) -> bindList retList as = as

> rightIdentityList    Nil       =  Refl
>
> rightIdentityList (  a :: as)  =  ( bindList retList (a :: as) )
>
>                                ={ Refl }=
>
>                                   ( joinList (mapList retList (a :: as)) )
>
>                                ={ Refl }=
>
>                                   ( joinList (retList a :: mapList retList as) )
>
>                                ={ Refl }=
>
>                                   ( retList a ++ joinList (mapList retList as) )
>
>                                ={ Refl }=
>
>                                   ( retList a ++ bindList retList as )
>
>                                ={ cong (rightIdentityList as) }=
>
>                                   ( retList a ++ as )
>
>                                ={ Refl }=
>
>                                   ( (a :: Nil) ++ as )
>
>                                ={ Refl }=
>
>                                   ( a :: (Nil ++ as) )
>
>                                ={ Refl }=
>
>                                   ( a :: as )
>
>                                QED


\subsubsection*{Exercise \ref{exercise5.8}:}

> flowNonDetSys' : {X : Type} -> (Eq X) => (m : Nat) -> NonDetSys X -> NonDetSys X
>
> flowNonDetSys'  Z     f x  =  retList x
>
> flowNonDetSys' (S m)  f x  =  nub (joinList (mapList (flowNonDetSys' m f) (f x)))


% \subsubsection*{Exercise \ref{exercise5.9}:}
