% -*-Latex-*-

%if False

> module Lecture2

> %default total
> %auto_implicits on
> %access public export

%endif


\setcounter{section}{1}

\stepcounter{section}
%\section{Mathematical specifications}
\label{section:mathematical_specifications}

\paragraph{Objectives of this lecture}
\begin{itemize}
\item Look at semi-formal mathematical problem specification 
\item Learn to carry out simple specification tasks through exercises 
\item Gain some elementary knowledge of structural induction and
  equational reasoning as proof method
\end{itemize}


\subsection{Equations, problems and solutions}

In mathematics we say that

\begin{quote}

  |x  =  1|

  |x  = -1|

\end{quote}

are solutions of

\begin{quote}

  |x²  =  1|

\end{quote}

What does this precisely mean? |x = 1|, |x = -1| and |x² = 1| are all
equations. But they are in certain relations to each other. We have

\begin{quote}

  |x =  1  =>  x² = 1|

\end{quote}

and also

\begin{quote}

  |x = -1  =>  x² = 1|

\end{quote}

These implications are what justifies saying that |x = 1| and |x = -1|
\emph{solve} |x² = 1|. \\

The equation |x = 1| (|x = -1|) is different from |x² = 1| also from another
point of view: the first equation determines \emph{the} value of |x|
directly, without computations. \\

The equation |x² = 1| specifies a problem: that of finding \emph{values}
whose square is one. We can specify the problem a little bit more
explicitly:

\begin{quote}
  
  \textbf{Find} |x ∈ ℝ| \textbf{s.t.} |x² = 1|

\end{quote}

This is a first example of a \emph{mathematical specification}. As we
have seen, the problem has two solutions. We can go one step further and
specify the problem of finding the square root of an arbitrary number:

\begin{quote}
  
  \textbf{Given} |y ∈ ℝ|, \textbf{find} |x ∈ ℝ| \textbf{s.t.} |x² = y|

\end{quote}
  
This problem is not solvable. For instance, it is not solvable for |y =
-1|. We say that the specification is \emph{infeasible}. The problem
here is that the requirements on |y| are too weak. We can obtain a
\emph{feasible} specification if we require |y| to be non-negative:

\begin{quote}
  
  \textbf{Given} |y ∈ ℝ|, |0 ≤ y|, \textbf{find} |x ∈ ℝ| \textbf{s.t.} |x² = y|

\end{quote}
  
The problem can now be solved, at least in principle. In practice,
computing square roots of arbitrary numbers can be very difficult if we
pretend to fulfill |x² = y| exactly. When doing real computations, we
typically accept that this equation will only be satisfied up to a
certain tolerance. We do not want to deal with these kind of problems
here.


\subsection{Functions as solutions}

The last specification does not say anything about which root shall be
found for a given |y|. For instance, if we just want to look at four
input values, say |y| takes the values |[1, 0, 9, 4]|, then all of

\begin{quote}
  
  |[-1, 0, 3, 2]|, |[1, 0, -3, -2]|, |[1, 0, 3, 2]|, |[-1, 0, -3, -2]|

\end{quote}
  
are acceptable results according to that specification. Sometimes
we want to be more precise and require the solution of a problem to be a
\emph{function}. In mathematics, we specify a function by giving its
signature and its definition. For instance

< double : Nat -> Nat 
< double n = 2 * n

We say that double is of type |ℕ → ℕ| or that double maps natural numbers
to natural numbers. For |f : A → B|, |A| and |B| are called the \emph{domain}
and the \emph{codomain} of |f|. \\

\textbf{Notation:} we denote function application |f (x)| by juxtaposition |f x|! \\

We can specify the problem of finding a function that computes a square
root of arbitary non-negative numbers as e.g.

\begin{quote}

  \textbf{Find} |sqrt(!) : Real -> Real| \ \textbf{s.t.}
  |∀ y ∈ Real|, \ |0 ≤ y => square ((sqrt(y))) = y|

\end{quote}

or equivalently as

\begin{quote}

  \textbf{Find} |sqrt(!) : RPosz -> Real| \ \textbf{s.t.}
  |∀ y ∈ RPosz|, \ |((sqrt y)) * ((sqrt y)) = y|

\end{quote}

This problem has two solutions: one function that always computes the
negative root and one that always computes the positive square root.

\begin{shaded}
\begin{exercise}
\label{exercise2.1}
  Specify the problem of finding a function that computes both square roots.
\end{exercise}
\end{shaded}


\paragraph{Remark:} In this subsection, we followed Morgan's
introduction to programming from specification
\cite{DBLP:books/daglib/0067018}. There you can 
also find more examples and exercises. 

\subsection{Domain-specific notions}

Mathematical specifications can also be applied to clarify notions that
are specific to a given application domain. For example:

\begin{itemize}

  \item What does it mean for |f : X -> Y| to be a function?

  \item What does it mean for a strategy to be dominant?

  \item What does it mean for a climate state to be avoidable?
    
\end{itemize}
  
Often, giving precise answers is not easy. Sometimes, it turns out that
we want a whole \emph{family of notions}, not just one notion. The
context of the emission problem discussed in lecture 1, for instance, is

\begin{itemize}
  
  \item Emission reductions imply different costs and benefits for
    different countries.

  \item The highest global benefits are obtained if most countries
    reduce emissions by certain (optimal, fair, ...) country-specific
    amounts.

  \item In this situation most countries have a free-ride opportunity!

\end{itemize}

The most paradigmatic example of this situation is perhaps the
two-players prisoner's dilemma

\begin{table}[h]
\centering
\begin{tabular}{||c||c||c||}
  \hline
  &   D   &   C     \\
  \hline
  D & (1,1) & (3,0) \\  
  \hline
  C & (0,3) & (2,2) \\
  \hline
\end{tabular}
\caption{Payoff matrix \label{table:one}}
\end{table}

Which property makes |(D,D)| stable and yet undesirable strategies? 

\begin{quote}
  
  Let |S = {D,C}| and |p1, p2 : cross S S → ℝ| payoff functions. A
  \textbf{strategy profile} |(x,y) ∈ cross S S| is a \textbf{Nash
    equilibrium} iff |∀ x', y' ∈ S|, |p1 (x', y) ≤ p1 (x, y)| and |p2
  (x, y') ≤ p2 (x, y)|.

\end{quote}

\paragraph{Remark:} Note that the definition of Nash equilibrium
depends on a binary operator |≤|. (Which properties should this binary
operator reasonably have?)


\begin{shaded}
  \begin{exercise}
    \label{exercise2.2}  
    Modify the payoffs of |(C,C)| in Table \ref{table:one} for |(C,C)|
    to become a Nash equilibrium.
  \end{exercise}
\end{shaded}

\begin{shaded}
  \begin{exercise}
    \label{exercise2.3}  
    Generalize the notion of Nash equilibrium to an arbitrary number of
    players.
  \end{exercise}
\end{shaded}  

Let |X| denote a set of states that a decision maker can observe. For
instance, |X| could be a tuple of numbers that represent aggregated
measures or indicators of wealth, inequality, environmental stress,
etc. \\

Let |Y| denote the options available to the decision maker. For
simplicity, we assume that she has the same options in all states |x ∈
X|. \\

Functions that associate an option to every state are called
\emph{policies}. \\

Let |val : X -> Y -> Real| be a \emph{value function}: |val x y| denotes
the value of taking decision |y| in state |x|. \\

A policy |p : X -> Y| is called \emph{optimal} w.r.t to |val| if it
yields controls that are better or as good as any other control for
all states. 

\begin{shaded}
  \begin{exercise}
    \label{exercise2.4}
    Give a mathematical specification of the notion of
    optimality for policies.
  \end{exercise}
\end{shaded}  

If |Y| is finite and non-empty, one can implement
  
< max     :  (Y -> Real) -> Real
<
< argmax  :  (Y -> Real) -> Y

that fulfill

\begin{quote}

  |∀ f : Y -> Real|, \ |∀ y ∈ Y|, \ |f y <= max f|

  |∀ f : Y -> Real|, \ |f (argmax f) = max f|

\end{quote}

\begin{shaded}
  \begin{exercise}
    \label{exercise2.5}
    Find a function |p : (X -> Y -> Real) -> (X -> Y)| that such that
    |p val| is an optimal policy w.r.t to |val| for arbitrary
    |val|. Prove that |p val| is indeed optimal.
  \end{exercise}
\end{shaded}  

\begin{shaded}
  \begin{exercise}
    \label{exercise2.6}
    Let |Fin n| be the set of natural numbers smaller
    than |n|:
    \begin{quote}

      |Fin 0 = {}|
  
      |Fin 1 = {0}|

      |Fin 2 = {0, 1}|
 
      \dots

      |Fin n = {0 ... n - 1}|

    \end{quote}
    Give a mathematical specification of the notion of finiteness for a set
    |X|. Begin with

    \begin{quote}
      A set |X| is \textbf{finite} iff \dots
    \end{quote}
  \end{exercise}
\end{shaded}  

\begin{shaded}
  \begin{exercise}
    \label{exercise2.7}
    Apply the specification of finiteness from Exercise
    \ref{exercise2.6} to show that the two elements set |X = {Up, Down}|
    is finite.
  \end{exercise}
\end{shaded}  


\subsection{Mathematical specifications and modelling}
\label{subsection:modelling}

In agent-based models of green growth (opinion formation, consume, etc.)
it is common to equip a set of agents with certain features. Thus, for
instance, agents can be employed or unemployed

\begin{quote}  
  |status : Agent -> {Employed, Unemployed}|
\end{quote}

\dots have certain incomes and

\begin{quote}  
  |income : Agent -> RPosz|
\end{quote}

\dots consume behaviors

\begin{quote}  
  |buy : Agent -> Prob {GreenCar, BrownCar, NoCar}|
\end{quote}

Here |Prob X| represents finite probability distributions over an
arbitrary set |X|. Let |Event X = X -> Bool| and

\begin{quote}  
  |prob : Prob X -> Event X -> [0,1]|
\end{quote}

be the generic function that computes the probability of an event |e :
Event X| according to a given probability distribution. Thus, for |d ∈
Prob X| and |e ∈ Event X|

\begin{quote}  
  |prob d e|
\end{quote}

represents the probability of |e| according to |d|. We want to implement
an agent-based model in which agents with higher incomes are more likely
to buy green cars than agents with lower incomes. 

We also would like to specify that unemployed agents are less likely to
buy a brown car than employed agents.

\begin{shaded}
  \begin{exercise}
    \label{exercise2.8}  
    Express these model requirements as mathematical specifications
    using the model-specific functions |status|, |income|, |buy| and the
    generic function |prob|.
  \end{exercise}
\end{shaded}


\subsection{Equational reasoning}
\label{subsection:equational_reasoning}

\emph{Equational Reasoning} is the proof method encouraged by the
``Algebra of Programming'' community \cite{DBLP:books/daglib/0096998}
($\rightsquigarrow$ see L3E2) for reasoning about systematic,
correctness preserving program transformations.
Originally this form of calculation with programs was
done on a semi-formal meta-level (by semi-formal we mean: on paper,
not inside an implemented type theory/proof assistant).

It comes with a distinctive style of presenting proofs with
justification of every transformation step (just as one would do in
school when solving equations).

A very important ingredient of this algebraic approach to program
correctness is \emph{structural induction}. Here, we will look at a
simple example using this technique, presented in equational reasoning
style.

We will prove a property of exponentiation.

The exponentiation with natural numbers fulfills the properties

\begin{quote}

  (1) |∀ x ∈ ℝ|, |pow x 0 = 1|

  (2) |∀ x ∈ ℝ|, |m ∈ ℕ|, |pow x (1 + m) = x * pow x m|
  
\end{quote}
  
From (1), (2) and the algebraic properties of |*| and |+|
we can show that 

\begin{quote}

  |∀ x ∈ ℝ|, |m,n ∈ ℕ|, |pow x (m + n) = pow x m * pow x n|

\end{quote}
  
The proof is by induction on |m|. We first show the base case (|m = 0|) 

\begin{quote}

  |∀ x ∈ ℝ|, |n ∈ ℕ|, |pow x (0 + n) = (pow x 0) * (pow x n)|

\end{quote}
  
Then we prove the induction step (|m => 1 + m|)

\begin{quote}
  
  |∀ x ∈ ℝ|, |n ∈ ℕ|, |pow x (m + n) = (pow x m) * (pow x n)|

  |=>|

  |∀ x ∈ ℝ|, |n ∈ ℕ|, |pow x ((1 + m) + n) = (pow x (1 + m)) * (pow x n)|  

\end{quote}  
  
The proofs are obtained by \emph{equational reasoning}.
Let's start with the ``difficult'' (induction step) case:

< pow x ((1 + m) + n)
<
<   = {- Associativity of |+| -}
<
< pow x (1 + (m + n))
<
<   = {- Property (2) -}
<
< x * (pow x (m + n))
<
<   = {- Induction hypothesis -}
<
< x * (pow x m * pow x n)
<
<   = {- Associativity of |*| -}
<
< (x * pow x m) * pow x n
<
<   = {- Property (2) -}
<
< (pow x (1 + m)) * pow x n

\begin{shaded}
  \begin{exercise}
    \label{exercise2.9}  
    Prove the base case.
  \end{exercise}
\end{shaded}
 

\subsection{Coming up}

The next lecture is an introduction to functional programming. \\

We use Idris \cite{idristutorial} as a specification and programming
language. \\



\pagebreak
\section*{Solutions}


\subsubsection*{Exercise \ref{exercise2.1}:}

  The specification of a function |allsqrts| that returns the set of
  all possible square roots of a positive real number: 
  
  \begin{quote}  
    \textbf{Find} |allsqrts: ℝ -> PS ℝ| \ \textbf{s.t.} |∀ y ∈ RPosz|,
    \ |∀ x ∈ ℝ, x² = y ⇔ x ∈ allsqrts(y)|
  \end{quote}    


\subsubsection*{Exercise \ref{exercise2.2}:}    
\begin{table}[h]
  \centering
  \begin{tabular}{||c||c||c||}
    \hline
    &   D   &   C     \\
    \hline
    D & (1,1) & (3,0) \\  
    \hline
    C & (0,3) & (3,3) \\
    \hline
  \end{tabular}
\end{table}


\subsubsection*{Exercise \ref{exercise2.3}:}    
Let |n ∈ Nat| be the number of players and |S_i|, |i ∈ {1, ..., n}|
the strategy set of the |i|-th player. Let |p_i : S_1 xx S_2 xx
... xx S_n → ℝ|, |i ∈ {1, ..., n}| the payoff function of the |i|-th
player. A \textbf{strategy profile} |(x_1, ..., x_n) ∈ S_1 xx ... xx
S_n| is a \textbf{Nash equilibrium} iff |∀ i ∈ {1, ..., n}| and |∀
xp_i ∈ S_i|, |p_i (x_1, ..., x_im1, xp_i, x_ip1, ... x_n) ≤ p_i
(x_1, ..., x_im1, x_i, x_ip1, ... x_n)|.


\subsubsection*{Exercise \ref{exercise2.4}:}    
\begin{quote}
  |p : X -> Y| \textbf{optimal} iff |∀ x : X|, |∀ y : Y|, |val x y ≤ val x (p x)|
\end{quote}


\subsubsection*{Exercise \ref{exercise2.5}:}    
With

< p : (X -> Y -> Real) -> (X -> Y)
< p val x = argmax (val x)

and for an arbitrary |x : X|, one has

\begin{quote}
  |∀ ! y ∈ Y|, \ |(val x) y <= max (val x)|
\end{quote}

because of (a) with |f = val x|. Because of (b) and, again, for |f =
val x|, one has
    
\begin{quote}
  |max (val x) = (val x) (argmax (val x))|
\end{quote}

Thus, we conclude

\begin{quote}
  |∀ ! x : X|, |∀ ! y ∈ Y|, \ |(val x) y <= (val x) (argmax (val x))|
\end{quote}    

But |argmax (val x) = p val x| by definition of |p| and thus we have

\begin{quote}
  |∀ ! x : X|, |∀ ! y ∈ Y|, \ |val x y <= val x (p val x)|
\end{quote}    

that is, |p val| is optimal w.r.t.\ |val| as required.


\subsubsection*{Exercise \ref{exercise2.6}:} 

A set |X| is \textbf{finite} iff \dots

\begin{quote}

  $\dots$ |∃ ! n ∈ ℕ,  ∃ ! f: X -> Fin n , ! isIso f|

\end{quote}

  where for a function |f : A -> B|

\begin{quote}

  |isIso f <=> ∃ ! g : B -> A, ! f . g = id ∧ g . f = id | 

\end{quote}

\subsubsection*{Exercise \ref{exercise2.7}:} 

In order to show | finite { Up, Down } |, we first have to choose a
natural number:

\begin{quote}

Choose | n := 2 |

\end{quote}

then choose a function  | f : { Up, Down } -> Fin 2 |:

\begin{quote}

  Choose

  | f : { Up, Down } -> Fin 2 |, where

  | f Up   = 0 |
  | f Down = 1 |
  
\end{quote}
 and show that | f | is an isomorphism according to definition given
 above.
 I.e. we have to choose a function | g : Fin 2 -> { Up, Dpwn } |
 and show that | f | and | g | are mutual inverses.
  
\begin{quote}

  Choose | g : Fin 2 ->  { Up, Down } |, where

  | g 0 = Up   |
  | g 1 = Down |

\end{quote}

  Now show (pointwise) that

\begin{quote}
  
  | f ∘ g = id | by

  | ∀ x ∈ Fin 2, f (g x) = x |

\end{quote}

  Case | x = 0 |:

\begin{quote}
  
  | f (g 0)) = f Up = 0 | by definition of f and g

\end{quote}

  Case | x = 1 |:

\begin{quote}
    
  | f (g 1) = f Down = 1 | by definition of f and g
  
  and

  | g ∘ f = id |  by

  | ∀ x ∈ { Up, Down}, g (f x) = x |

\end{quote}
  
  Case | x = Up |:

\begin{quote}
    
  | g (f Up)) = g 0 = Up | by definition of f and g

\end{quote}
  
  Case | x = Down |:

\begin{quote}
    
  | g (f Down) = g 1 = Down | by definition of f and g
  
\end{quote}


\subsubsection*{Exercise \ref{exercise2.8}:} 

Let | eGreen: {GreenCar, BrownCar, NoCar} -> Bool | be an event such that

\begin{quote}
  
< eGreen GreenCar = true 

\end{quote}

and

| eBrown: {GreenCar, BrownCar, NoCar} -> Bool | be an event such that

\begin{quote}

< eBrown BrownCar = true

\end{quote}

Then we require that the following implications hold:

\begin{quote}

  | ∀ a1, a2 ∈ Agent, |
  
  | income(a1) > income(a2) |
  
    | => prob (buy(a1)) eGreen > prob (buy(a2)) eGreen |

\end{quote}

\begin{quote}

  | ∀ a1, a2 ∈ Agent, |

  | status(a1) = Unemployed ∧ status(a2) = Employed |

    | => prob (buy(a1)) eBrown < prob (buy(a2)) eBrown |

\end{quote}


\subsubsection*{Exercise \ref{exercise2.9}:} 
< pow x (0 + n)
<
<   = {- Zero left neutral element of |+| -}
<
< pow x n
<
<   = {- One left neutral element of |*| -}
<
< 1 * pow x n
<
<   = {- Property (1) -}
<
< pow x 0 * pow x n

\pagebreak

