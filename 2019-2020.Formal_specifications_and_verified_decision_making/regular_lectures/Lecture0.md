---
title: Aims and plans
author: Nicola Botta, Nuria Brede, Potsdam Institute for Climate Impact Research
date: 2019-10-28
geometry: margin=2cm
fontsize: 10pt
---

# Objectives of the course

  * Learn to specify and solve decision problems in climate research

  * Understand the basics of decision making under uncertainty

  * Understand optimal policies and accountable decision making 


# Structure

  * A series of basic lectures + exercises to achieve the objectives of
    the course (L1 - L10)

  * Extra lectures (L1E1, L2E1, L3E1, ...) to deepen, point out
    connections, ... have more fun!


# Tentative course plan

  *  L1: Decision problems in climate research
  
	  - L1E1:  What are formal methods? 
	    ("Modelling vs. formalization", Example from DSL of math)

  *  L2: Mathematical specifications
  
	  - L2E1: Newcomb's dilemma

  *  L3: A crash-course in functional programming 
  
	  - L3E1: Install and use Idris and emacs' Idris mode 
		
	  - L3E2: Folds and initial algebras

  *  L4: Dependent types and machine checkable specifications 
  
      - L4E1: FOL and propositions-as-types

  *  L5: Time discrete dynamical systems
  
      - L5E1: Functors and monads from a CT angle

  *  L6: Time discrete monadic dynamical systems

  *  L7: Deterministic sequential decision problems, naive theory

  *  L8: Viability and reachability

  *  L9: Generic optimal extensions, viability and reachability tests

  * L10: Extending the theory to monadic SDPs 

  * L11: Specifying an emission problem


# Tentative time plan

  * Day 1: Make a time plan, L1, L2 and perhaps L1E1?

  * Day 2: L1 and L2 exercises, install and use Idris, L2E1?

  * Day 3: L3, L3 exercises and L3E2?

  * Day 4: L4, L4 exercises and L4E1? 
  
  * Day 5: L5

  * Day 6: L5E1, L6
  
  * Day 7: L7, L8

  * Day 8: L9
  
  * Day 9: L10
  
  * Day 10: L11
