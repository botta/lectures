> module SequentialDecisionProblems.applications.LowHigh

> import Data.Fin
> import Control.Isomorphism

> import Finite.Predicates
> import Finite.Operations
> import Finite.Properties
> import Sigma.Sigma

> %default total
> %auto_implicits off
> %access public export


* Low / High emissions:

> data LowHigh = L | H 


* |LowHigh| is finite and non-empty:

> to : LowHigh -> Fin 2
> to L  =     FZ
> to H  =  FS FZ

> from : Fin 2 -> LowHigh
> from             FZ   = L
> from         (FS FZ)  = H
> from     (FS (FS  k)) = absurd k

> toFrom : (k : Fin 2) -> to (from k) = k
> toFrom             FZ   = Refl
> toFrom         (FS FZ)  = Refl
> toFrom     (FS (FS  k)) = absurd k

> fromTo : (a : LowHigh) -> from (to a) = a
> fromTo L = Refl
> fromTo H = Refl

> ||| 
> finiteLowHigh : Finite LowHigh
> finiteLowHigh = MkSigma 2 (MkIso to from toFrom fromTo)

> |||
> cardNotZLowHigh : CardNotZ finiteLowHigh
> cardNotZLowHigh = cardNotZLemma finiteLowHigh L


* |LowHigh| is in |DecEq|:

> lowNotHigh : L = H -> Void
> lowNotHigh Refl impossible

> implementation [DecEqLowHigh] DecEq LowHigh where
>   decEq L L = Yes Refl
>   decEq L H = No lowNotHigh
>   decEq H L = No (negEqSym lowNotHigh)
>   decEq H H = Yes Refl


* |LowHigh| is in |Eq|:

> implementation Eq LowHigh where
>   (==) L L = True
>   (==) L H = False
>   (==) H L = False
>   (==) H H = True


> {-

> ---}


 
