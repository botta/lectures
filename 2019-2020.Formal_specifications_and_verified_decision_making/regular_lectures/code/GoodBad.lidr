> module SequentialDecisionProblems.applications.GoodBad

> import Data.Fin
> import Control.Isomorphism

> import Finite.Predicates
> import Finite.Operations
> import Finite.Properties
> import Sigma.Sigma

> %default total
> %auto_implicits off
> %access public export


* Good / Bad options:

> data GoodBad = G | B 


* |GoodBad| is finite and non-empty:

> to : GoodBad -> Fin 2
> to G =    FZ
> to B = FS FZ

> from : Fin 2 -> GoodBad
> from             FZ   = G
> from         (FS FZ)  = B
> from     (FS (FS  k)) = absurd k

> toFrom : (k : Fin 2) -> to (from k) = k
> toFrom             FZ   = Refl
> toFrom         (FS FZ)  = Refl
> toFrom     (FS (FS  k)) = absurd k

> fromTo : (a : GoodBad) -> from (to a) = a
> fromTo G = Refl
> fromTo B = Refl

> ||| 
> finiteGoodBad : Finite GoodBad
> finiteGoodBad = MkSigma 2 (MkIso to from toFrom fromTo)

> |||
> cardNotZGoodBad : CardNotZ finiteGoodBad
> cardNotZGoodBad = cardNotZLemma finiteGoodBad G


* |GoodBad| is in |DecEq|:

> goodNotBad : G = B -> Void
> goodNotBad Refl impossible

> implementation [DecEqGoodBad] DecEq GoodBad where
>   decEq G G = Yes Refl
>   decEq G B = No goodNotBad
>   decEq B G = No (negEqSym goodNotBad)
>   decEq B B = Yes Refl


* |GoodBad| is in |Eq|:

> implementation Eq GoodBad where
>   (==) G G = True
>   (==) G B = False
>   (==) B G = False
>   (==) B B = True


> {-

> ---}


 
