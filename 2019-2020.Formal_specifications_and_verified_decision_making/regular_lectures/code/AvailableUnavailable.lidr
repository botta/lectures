> module SequentialDecisionProblems.applications.AvailableUnavailable

> import Data.Fin
> import Control.Isomorphism

> import Finite.Predicates
> import Finite.Operations
> import Finite.Properties
> import Sigma.Sigma

> %default total
> %auto_implicits off
> %access public export


* Available / Unavailable emissions:

> data AvailableUnavailable = A | U 


* |AvailableUnavailable| is finite and non-empty:

> to : AvailableUnavailable -> Fin 2
> to A =    FZ
> to U = FS FZ

> from : Fin 2 -> AvailableUnavailable
> from             FZ   = A
> from         (FS FZ)  = U
> from     (FS (FS  k)) = absurd k

> toFrom : (k : Fin 2) -> to (from k) = k
> toFrom             FZ   = Refl
> toFrom         (FS FZ)  = Refl
> toFrom     (FS (FS  k)) = absurd k

> fromTo : (a : AvailableUnavailable) -> from (to a) = a
> fromTo A = Refl
> fromTo U = Refl

> ||| 
> finiteAvailableUnavailable : Finite AvailableUnavailable
> finiteAvailableUnavailable = MkSigma 2 (MkIso to from toFrom fromTo)

> |||
> cardNotZAvailableUnavailable : CardNotZ finiteAvailableUnavailable
> cardNotZAvailableUnavailable = cardNotZLemma finiteAvailableUnavailable A


* |AvailableUnavailable| is in |DecEq|:

> availableNotUnavailable : A = U -> Void
> availableNotUnavailable Refl impossible

> implementation [DecEqAvailableUnavailable] DecEq AvailableUnavailable where
>   decEq A A = Yes Refl
>   decEq A U = No availableNotUnavailable
>   decEq U A = No (negEqSym availableNotUnavailable)
>   decEq U U = Yes Refl


* |AvailableUnavailable| is in |Eq|:

> implementation Eq AvailableUnavailable where
>   (==) A A = True
>   (==) A U = False
>   (==) U A = False
>   (==) U U = True


> {-

> ---}


 
