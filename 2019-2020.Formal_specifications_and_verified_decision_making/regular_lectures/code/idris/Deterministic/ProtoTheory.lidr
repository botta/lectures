% -*-Latex-*-

> module Deterministic.ProtoTheory

> %default total
> %auto_implicits on
> %access public export


> X     :  (t : Nat) -> Type

> Y     :  (t : Nat) -> (x : X t) -> Type

> next  :  (t : Nat) -> (x : X t) -> (y : Y t x) -> X (S t)

> Val : Type

> reward : (t : Nat) -> (x : X t) -> (y : Y t x) -> (x' : X (S t)) -> Val

> oPlus : Val -> Val -> Val

> zero : Val

> LTE : Val -> Val -> Type

> reflexiveLTE : (a : Val) -> a `LTE` a

> transitiveLTE : (a : Val) -> (b : Val) -> (c : Val) -> a `LTE` b -> b `LTE` c -> a `LTE` c

> monotonePlusLTE : {a, b, c, d : Val} -> a `LTE` b -> c `LTE` d -> (a `oPlus` c) `LTE` (b `oPlus` d)
