> module Sigma.Sigma

> %default total
> %access public export
> %auto_implicits on

> data Sigma : (A : Type) -> (P : A -> Type) -> Type where
>   MkSigma : {A : Type} -> .{P : A -> Type} -> (x : A) -> (pf : P x) -> Sigma A P

> outl : {A : Type} -> {P : A -> Type} -> Sigma A P -> A
> outl (MkSigma a _) = a

> outr : {A : Type} -> {P : A -> Type} -> (s : Sigma A P) -> P (outl s)
> outr (MkSigma _ p) = p
