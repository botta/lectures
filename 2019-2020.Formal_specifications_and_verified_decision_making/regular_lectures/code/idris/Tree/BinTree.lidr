> module Tree.BinTree

> %default total
> %auto_implicits on
> %access public export


> ||| Tree
> data BinTree : Type -> Type where
>   Leaf   : a -> BinTree a
>   Branch : BinTree a -> BinTree a -> BinTree a


> {-

> ---}
 
 
 
 
