% -*-Latex-*-

%if False

> module Lecture4

> import Data.Vect
> import Syntax.PreorderReasoning

> import code.idris.Real.Postulates
> import code.idris.Tree.BinTree



> %default total
> %auto_implicits on
> %access public export

%endif

\setcounter{section}{3}

\stepcounter{section}
%\section{Dependent types and machine checkable specifications}

\paragraph{Objectives of this lecture}
\begin{itemize}
\item Get acquainted with dependent types
\item Learn how to formulate mathematical specifications using
  dependent types and do first proofs in Idris
\item Deepen theoretical background about correspondence between
  logic and type theory, the importance of totality and termination,
  and the issue of extensional equality of functions.
\end{itemize}

\subsection{Dependent types}

In a nutshell, \emph{dependent types} are types that depend on values.
We have already seen examples of dependent types in lecture 3. For
instance, both the type of the argument of

> tail : Vect (S n) a -> Vect n a

and the type of its result depend on the \emph{values} |n : Nat| and |a
: Type|: the type |Vect| is a dependent type.

\begin{shaded}

\textbf{Notation:} Remember that the above is in fact an abbreviation
for

< tail : {n : Nat} -> {a : Type} -> Vect (S n) a -> Vect n a 

(It is save to think about this as a logical statement
 |∀ n : Nat, ∀ a : Type, Vect (S n) a ⇒ Vect n a|.)
\end{shaded}

Other examples of dependently typed functions from lecture 3 are |(++)|,
|concat| and |map|. We have also seen dependently typed data
constructors.


\subsection{Equality types}

Important and natural examples of dependent types are equality types.

Idris has a built-in type for \emph{propositional equality}.

But let us look at \emph{boolean equality tests} first.

In Idris, many predefined types come equipped with equality \emph{tests}:

< Idris> 2 + 1 == 3
< True : Bool

< Idris> [1,2,3] == [2,1,3]
< False : Bool

< Idris> True == False
< False : Bool

The tests are all called |(==)| and return Boolean values. Not all Idris
types can be test compared for equality.

\begin{shaded}
  \begin{exercise}
    \label{exercise4.1}    
    Give an example of a type whose values cannot be compared for
    equality.
  \end{exercise}
\end{shaded}

For all predefined types that can be compared for equality, |(==)| is
defined as one would expect. But nothing would prevent one to define an
equality test for Booleans like for instance

> (==) : Bool -> Bool -> Bool
> (==) b1 b2 = False

This would yield

< Idris> True == True
< False : Bool

Equality tests can only be evaluated at \emph{run time} and their
results may or may not reflect the equality (or inequality) of their
arguments. \\

But Idris also supports logical reasoning about the equality or the
inequality of expressions at \emph{type check time}. This is the role
of the built-in propositional equality type briefly mentioned above.
Type checking is done before a program is actually compiled and, thus,
well before the program can be executed.

For instance

> p : 2 + 1 = 3 

is a legal Idris declaration. It represents a claim that the expression
|2 + 1| is equal to the expression |3|. A proof of such claim is just an
implementation of |p|:

> p = Refl

The claim that an expression |x| of type |a| is equal to an expression
|y| of type |b| represented by the type |(x = y)|. \\

The infix operator |(=)| used here has type |a -> b -> Type|. For every
|x : a|, and |y : b| we have a type |(x = y)|. This type depends on the
values |x| and |y|. Thus, it is a dependent type. Conceptually, it is
defined as:\\

< data (=) : a -> b -> Type where
<
<   Refl : x = x

where |Refl| stands for ``reflexivity''.
For most |(x = y)| types there are no values: they are \emph{empty}
types. But a few have one value written |Refl : (a = a)|. \\

|Refl| can be used to constuct a value of type |(x = y)| iff |x| and |y|
 can be reduced to the same expression. Thus, for instance, the claim

> q : 2 + 1 = 0

can be formulated but it cannot be implemented. The only way of
implementing a proof would be by

< q = Refl

and this triggers a \emph{type check error}. The program cannot be
compiled. 


\subsection{Negation, logical impossibility}

While Idris does not allow to implement a proof |q| that 2 + 1 equals
0, it makes it easy to show that such a |q| is an absurdity:

> notq : Not (2 + 1 = 0)
> notq Refl impossible

Here |Not| is the function

< Not : Type -> Type
< Not a = a -> Void

and |Void| is a type with no constructors:

< data Void : Type where

Thus, a value of type |Void| represents a logical impossibility. Idris
provides a built-in rule for "ex falso sequitur quodlibet" called
|void|:

< void : Void -> a

Thus, if we have a value of type |T| and one of type |Not T|, we can
prove everything:

> T   :  Type
> t   :  T
> nt  :  Not T

> oneEqZero : 1 = 0
> oneEqZero = void (nt t)

Back to the implementation of |notq|. There, |impossible| is a
keyword. \\

It recognizes an impossible pattern matching (remember that |2 + 1| is
just an abbreviation for |plus (S (S Z)) (S Z)| which, in turn, reduces
to |S (S (S Z))| and that constructors are disjoint) and
yields a contradiction – that is, a value of type |Void|.


\subsection{Properties, propositions and types}

Dependent types can also be used to encode properties and propositions.
For instance, with

> Domain : (a -> b) -> Type
> Domain {a} f = a          
 
we can express what it means for an arbitrary function |f : a -> b| to
be injective

> Injective : (a -> b) -> Type
> Injective f = (x, y : Domain f) -> f x = f y -> x = y

This is almost a word-by-word translation of the corresponding
mathematical specification:

\begin{quote}
  |f : a -> b| \emph{injective} iff |∀ x, y ∈ Dom f|, |f x = f y => x = y|.
\end{quote}

\begin{shaded}
  \begin{exercise}
    \label{exercise4.2}    
    Recall the notion of \emph{optimality of policies} from
    lecture 2:
    \begin{quote}  
      |p : X -> Y| \textbf{optimal} iff |∀ x : X|, |∀ y : Y|, |val x y ≤ val x (p x)|
    \end{quote}
    Here |X| and |Y| were sets (states, options) and |val : X -> Y -> Real|
    denoted a value function. Take

> X    :  Type            -- the type of states
> Y    :  Type            -- the type of options
> val  :  X -> Y -> Real  -- a value function

    and implement a dependently typed specification of the notion of
    optimality for policies through an Idris function of type |(X -> Y)
    -> Type|. 
  \end{exercise}  
\end{shaded}


\subsection{Existential types}

In many mathematical specifications we find fragments of the form |∃ x ∈
X| \textbf{s.t.} ... For instance

\begin{quote}
  
  Let |n ∈ Nat|. |d ∈ Nat| is a \textbf{divisor} of |n| iff |∃ q ∈ Nat|,
  \textbf{s.t.} |d * q = n|.

\end{quote}

Because in DTLs we can encode propositions as types, we can define a
data type that represents the statement "there exists an |x| such that
|prop x| holds".

> data Exists : (a : Type) -> (pro : a -> Type) -> Type where
>   Evidence : (wit : a) -> (prf : pro wit) -> Exists a pro

Now we can specify what it means for a natural number to be a divisor:

> Divisor : (d : Nat) -> (n : Nat) -> Type
> Divisor d n = Exists Nat (\ q => d * q = n)

and give a proof that 3 is a divisor of 6

> threeDivisorSix : Divisor 3 6
> threeDivisorSix = Evidence 2 Refl


\begin{shaded} 
  \begin{exercise}
    \label{exercise4.3}    
    In |Evidence 2 Refl|, |Refl| asserts the equality of two
    expressions. What are the expressions on the LHS and on the RHS of
    this equality? Proceed by unfolding the definitions in |Divisor 3
    6| and |Evidence 2 refl|.
  \end{exercise}
\end{shaded}

\paragraph{Remark:} The notion of \emph{existence} encoded by |Exists| is
constructive (evidential). A value of type |Exists a pro| can only be
constructed by giving a concrete witness |wit : a| and a proof |prf :
pro wit| that |pro| holds at |wit|.

\textbf{Remark:} The Idris definition of |Exists| is slightly different:
the first argument of |Exists| is implicit.

In addition to the logical reading one can also see values of type
|Exists a pro| as \emph{dependent pairs} where |Evidence| is the pair
constructor and the two projections are |getWitness| and |getProof|:

> getWitness  :  Exists a pro          ->  a
> getWitness     (Evidence wit prf)    =   wit

> getProof    :  (evi : Exists a pro)  ->  pro (getWitness evi)
> getProof       (Evidence wit prf)    =   prf

Note that the second projection (|getProof|) returns a value whose type
depends on the value of the first component of the pair. \\

When we want to underline the dependent pair interpretation we use a
data type called |Sigma|. 

\textbf{Remark:} Idris treats values of its pre-defined types |Exists
a pro| and |Sigma a pro| slightly differently but we do not need to be
concerned with these differences here. 


\subsection{Specifications and program correctness}

One important application of dependently typed programming is for
writing programs that are \emph{correct by construction}. \\

There are two methodologies for assessing the correctness of programs:
\emph{testing} and \emph{proving}
\cite{ionescujansson:LIPIcs:2013:3899}. \\

Testing is well suited for showing the \emph{presence} of
errors. Proving is good at showing their \emph{absence}. Thus, the
methodologies are complementary. \\

Proving the correctness of a program requires two steps. First, one has
to specify what it means for the program to be correct. Second, one has
to exhibit a proof of correctness. \\

In non dependently typed languages, both steps have to be undertaken in
a suitable formal language (external to the programming
language). Specification languages \cite{DBLP:conf/nato/1992pdc,
  DBLP:books/daglib/0096998, DBLP:books/daglib/0067018} and formal
methods of program derivation provide specific support for one or both
steps. 

In dependently typed languages, we do not need to rely on external
specification languages. We can use the same language to

\begin{itemize}

  \item Specify a program |P|.

  \item Implement |P|.

  \item Prove that |P| fulfills its specification.

\end{itemize}


Let us look at an example for the type of binary trees defined by

< data BinTree : Type -> Type where
<  Leaf   : a -> BinTree a
<  Branch : BinTree a -> BinTree a -> BinTree a

In Idris we can \emph{specify what it means} for the following function

> mapBinTree : (a -> b) -> BinTree a -> BinTree b

\emph{to be correct}

> mapBinTreeSpec1  :  (bt : BinTree a) -> mapBinTree id bt = id bt
>
>
> mapBinTreeSpec2  :  (f : b -> c) -> (g : a -> b) -> 
> 
>                     mapBinTree (f . g) = mapBinTree f . mapBinTree g

, \emph{implement} |mapBinTree|

> mapBinTree f (Leaf x)      =  Leaf (f x) 
> 
> mapBinTree f (Branch l r)  =  Branch (mapBinTree f l) (mapBinTree f r) 

and \emph{prove that the implementation fulfills its specification}:

> cong2  :  {a1, a2 : a} -> {b1, b2 : b} -> {f : a -> b -> c} ->
> 
>           (a1 = a2) -> (b1 = b2) -> f a1 b1 = f a2 b2
>         
> cong2 Refl Refl = Refl

> mapBinTreeSpec1base : (x : a) -> mapBinTree id (Leaf x) = id (Leaf x)
> 
> mapBinTreeSpec1base x  =  ( mapBinTree id (Leaf x) )
> 
>                        ={ Refl }=
>                       
>                           ( Leaf (id x) )
>                         
>                        ={ Refl }=
>                       
>                           ( Leaf x )
>                         
>                        ={ Refl }=
>                       
>                           ( id (Leaf x) )
>                         
>                        QED

> mapBinTreeSpec1step  :  (l : BinTree a) -> (r : BinTree a) -> 
> 
>                         (ihl : mapBinTree id l = id l) -> (ihr : mapBinTree id r = id r) -> 
>                         
>                         mapBinTree id (Branch l r) = id (Branch l r)
> 
> mapBinTreeSpec1step l r ihl ihr  =  ( mapBinTree id (Branch l r) )
> 
>                              ={ Refl }=
>                             
>                                 ( Branch (mapBinTree id l) (mapBinTree id r) )
>                                 
>                              ={ cong2 ihl ihr }=
>                             
>                                 ( Branch (id l) (id r) )
>                               
>                              ={ Refl }= 
>                             
>                                 ( Branch l r )
>                               
>                              ={ Refl }=
>                             
>                                 ( id (Branch l r) ) 
>                               
>                              QED 

> mapBinTreeSpec1 (Leaf x)      =  mapBinTreeSpec1base x
> 
> mapBinTreeSpec1 (Branch l r)  =  mapBinTreeSpec1step l r ihl ihr where
> 
>   ihl  =  mapBinTreeSpec1 l 
>   
>   ihr  =  mapBinTreeSpec1 r

There is, however, a subtle difference between the statement of
  |mapBinTreeSpec1| and |mapBinTreeSpec2| above which we need to
  address. The first is stated as a \emph{pointwise} property, while
  the second is stated as an \emph{extensional equality of
  functions}. Abstractly, for given functions  
  |f,g : a -> b|, the first is a statement of the form

(1) |∀ x: b, f x = g x|

while the second has the form

(2) |f = g|.

In the intuitionistic logic underlying Idris, |(2) ⇒ (1)| is provable,
  but |(1) ⇒ (2)| is not (as the theory has models in which the latter
  implication does not hold).

If we want to prove a statement such as this, we thus cannot avoid to
  postulate that pointwise equality of functions implies extensional
  equality of functions:

> funext : (f, g : a -> b) -> ((x : a) -> f x = g x) -> f = g

Using this postulate, we can first prove the pointwise statement

> mapBinTreeSpec2a  :  (bt : BinTree a) -> (f : b -> c) -> (g : a -> b) -> 
> 
>                     mapBinTree (f . g) bt = (mapBinTree f . mapBinTree g) bt

and then use |funext| to derive |maBinTreeSpec2| as stated above.

\begin{shaded} 
  \begin{exercise}
    \label{exercise4.4}    
    Implement first |mapBinTreeSpec2a|, then use |funext| to prove
    |mapBinTreeSpec2|. 
  \end{exercise}
\end{shaded}


\subsection{Programs, proofs, totality and termination}

We have seen that we can represent properties as types and in this view
proofs are just values of these types. \\

We sum up (a part of) the correspondence between Idris and logic in
Table~\ref{fig:CurryHoward}. (This correspondence goes in fact much deeper
  than conveyed by the table. $\rightsquigarrow$ see L4E1)

\begin{table}
\begin{tabular}{l@@{\quad}l}
\hline
   Idris                    & Logic
\\ \hline
   |p : pro|                & |p| is a proof of |pro|
\\ |Void| (empty type)      & False
\\ |()| (singleton type)    & True 
\\ |p -> q|                 & |p| implies |q|
\\ |Exists a pro|           & there exists a |wit| such that |pro wit| holds
\\ |(x : a) -> pro x|       & forall |x| of type |a|, |pro x| holds
\\ \hline
\end{tabular}
  \caption{Curry-Howard correspondence relating Idris and logic on the
    type level.}
  \label{fig:CurryHoward}
\end{table} 

When we embed logic in a programming language, we have to be careful
about two notions: \emph{totality} and
  \emph{termination}. (Non-termination of programs can be seen as
  counterpart to logical paradoxes, $\rightsquigarrow$ see L4E1.) \\

A total function |f : a -> b| is defined for all type correct inputs. \\

A partial function would be undefined on some of |x : a|. \\

Partial functions can be very useful. But proofs shall always be total.

If we allowed partial functions to silently compromise the totality
of proofs, we could easily prove any theorem, including patently false
ones. Consider the function |headL : List a -> a|

> partial 
> headL : List a -> a
> headL (x :: xs) = x

This is a partial function because it is not defined for the empty
list. Using |headL| we could easily "prove" that every natural number is
zero:

> aNecessarilyEmptyList : List Void
>
> aNecessarilyEmptyList = []

> partial
> surprise : (n : Nat) -> n = 0
> 
> surprise n = void (headL aNecessarilyEmptyList)

Try to typecheck the file after uncommenting the |partial| flag above --
The Idris type checker realizes that we are trying to fool the system
and that |surprise| cannot be total. 

The second potential problem is non-termination. A function may cover
all cases, but still fail to terminate. The extreme case is a completely
circular definition

> -- circular : Void
> -- circular = circular

Try to typecheck the file after uncommenting the definition of |circular| --
Idris will warn about missing cases and potentially non-terminating
loops in definitions that are required to be |total|.


\subsection{Type checking and correctness}

With dependently typed languages, we can require the type checker to
verify that a certain program implementation is correct with respect
to its specification. \\ 

This methodology yields programs that are \emph{correct by
construction}. This is the highest standard we can aim for in
programming. \\

Crucial components of the methodology are \emph{totality} and
\emph{termination} checks. \\

Termination checks are necessarily conservative: failures to pass the
tests mean that the program might not terminate, not that it will not
terminate. \\

Conversely, a program that passes a termination test will always
terminate, at least in principle. Of course, memory limitations and
hardware failures can always in practice prevent a computation from
terminating. \\

Beyond providing dependent types and totality and termination checks,
Idris supports a programming methodology that aims at increasing the
correctness of programs \emph{incrementally}. \\

The idea is to first fulfill program specifications \emph{conditionally}
on the basis of suitable postulates. These are then eliminated stepwise,
eventually leading to unconditional correctness proofs.


\subsection{Coming up}

In the next lecture, we will start looking at the formalization of
  dynamical systems and the problem of decision making under uncertainty.


 \pagebreak 
\section*{Solutions}

\subsubsection*{Exercise \ref{exercise4.1}:}
The type of functions from natural numbers to |Bool|.


\subsubsection*{Exercise \ref{exercise4.2}:}

> Optimal : (X -> Y) -> Type
> Optimal p = (x : X) -> (y : Y) -> val x y `LTE` val x (p x) 

What is the type of |`LTE`|?


\subsubsection*{Exercise \ref{exercise4.3}:}
The expressions are |3 * 2| on the LHS and |6| on the RHS


\subsubsection*{Exercise \ref{exercise4.4}:}

< mapBinTreeSpec2a  :  (bt : BinTree a) -> (f : b -> c) -> (g : a -> b) -> 
< 
<                      mapBinTree (f . g) bt = (mapBinTree f . mapBinTree g) bt

> mapBinTreeSpec2base  :  (x : a) -> (f : b -> c) -> (g : a -> b) ->
> 
>                         mapBinTree (f . g) (Leaf x) = 
>                        
>                         (mapBinTree f . mapBinTree g) (Leaf x)
> 
> mapBinTreeSpec2base x f g  =  ( mapBinTree (f . g) (Leaf x) )
> 
>                            ={ Refl }=
>                       
>                               ( Leaf ((f . g) x) )
>                         
>                            ={ Refl }=
>                       
>                               ( Leaf (f (g x)) )
>                         
>                            ={ Refl }=
>                       
>                               ( mapBinTree f (Leaf (g x)) )
>                         
>                            ={ Refl }=
>                       
>                               ( mapBinTree f (Leaf (g x)) )
>                         
>                            ={ Refl }=
>                       
>                               ( mapBinTree f (mapBinTree g (Leaf x)) )
>                                                                       
>                            ={ Refl }=
>                       
>                               ( (mapBinTree f . mapBinTree g) (Leaf x) )                        
>                            QED
>
> mapBinTreeSpec2step  :  (l : BinTree a) -> (r : BinTree a) -> (f : b -> c) -> (g : a -> b) -> 
> 
>                         (iHl : mapBinTree (f . g) l = (mapBinTree f . mapBinTree g) l) ->
>
>                         (iHr : mapBinTree (f . g) r = (mapBinTree f . mapBinTree g) r) ->
>
>                         mapBinTree (f . g) (Branch l r) = (mapBinTree f . mapBinTree g) (Branch l r)
> 
> mapBinTreeSpec2step l r f g ihl ihr
>                         =  ( mapBinTree (f . g) (Branch l r) )
> 
>                         ={ Refl }=
>                             
>                            ( Branch (mapBinTree (f . g) l) (mapBinTree (f . g) r) )
>                               
>                         ={ cong2 ihl ihr }=
>                          
>                            ( Branch
>
>                                 ((mapBinTree f . mapBinTree g) l)
>
>                                 ((mapBinTree f . mapBinTree g) r) )
>                               
>                         ={ Refl }= 
>                             
>                            ( mapBinTree f (Branch (mapBinTree g l) (mapBinTree g r)) )
>                               
>                         ={ Refl }= 
>                             
>                            ( (mapBinTree f . mapBinTree g) (Branch l r) )
>                               
>                         QED
>
> mapBinTreeSpec2a (Leaf x)      f g  =  mapBinTreeSpec2base x f g
> 
> mapBinTreeSpec2a (Branch l r)  f g  =  mapBinTreeSpec2step l r f g ihl ihr where
> 
>   ihl  =  mapBinTreeSpec2a l f g  
>   
>   ihr  =  mapBinTreeSpec2a r f g 


> helper : (f : b -> c) -> (g : a -> b) -> (bt : BinTree a) ->  
>          mapBinTree (f . g) bt = (mapBinTree f . mapBinTree g) bt
> 
> helper f g bt = mapBinTreeSpec2a bt f g

> mapBinTreeSpec2 f g = funext  (mapBinTree (f . g)) (mapBinTree f . mapBinTree g)  
>                       (helper f g)
>                      

\pagebreak
