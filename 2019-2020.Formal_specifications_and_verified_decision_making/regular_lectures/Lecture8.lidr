% -*-Latex-*-

%if False

> -- module Lecture8
> module Main

> import Sigma.Sigma
> import Sigma.Operations

> %default total
> %auto_implicits off
> %access public export

> %hide lteRefl

%endif

\setcounter{section}{7}

\vfill
\stepcounter{section}
%\section{Viability and reachability}

\paragraph{Objectives of this lecture}
\begin{itemize}
\item Get acquainted with the notions of \emph{viability} and
  \emph{reachability}  
\item Use these notions to revisit and improve the notion of policy
  sequence 
\item Adapt the notion of optimality and the generic backward
  induction to the improved theory
\item Revisit the problematic example from the last lecture within
    the new setting
\end{itemize}

Consider an SDP as in the following sketch \\

\begin{figure}[h]
  % 
  \begin{center}
    % 
    \begin{tabular}{c c c}
      % 
      \includegraphics{Lecture8-1}
      &
        \includegraphics{Lecture8-2}
      &
        \includegraphics{Lecture8-3}
        % 
    \end{tabular}
    % 
  \end{center} 

% \begin{asy}
%   size(6cm);
%   include graph;
%   string[] xs = {"a","b","c","d","e"};
%   string[] path = {"b","c","d","e","e","d","c","d"};
%   int nc = xs.length;
%   int nt = path.length;
%   real x0 = 0.0;
%   real t0 = 0.0;
%   real dx = 0.1;
%   real dt = 0.1;
%   real x;
%   real t;
%   pair A, B, C, D;
%   defaultpen(2);
%   for (int j = 0; j < nc; ++j) {
%     x = x0 + j * dx;
%     label(xs[j], position=(x+dx/2,t0-1.5*dt), align=N);
%   }
%   for (int i = 0; i < nt; ++i) {
%     x = x0;
%     t = t0 + i * dt;
%     label((string) i, (x-dx,t+dt/2));
%     for (int j = 0; j < nc; ++j) {
%       x = x0 + j * dx;
%       A=(x,t); B=(x+dx,t); C=(x+dx,t+dt); D=(x,t+dt);
%       if (i == 3 && j < nc - 1) fill(A--B--C--D--A--cycle);
%       if (i == 6 && j > nc - 3) fill(A--B--C--D--A--cycle);
%       draw(A--B--C--D--A, grey);
%     }
%   }
%   x = x0;
%   t = t0 + (0 + nt) * dt + dt/2;
%   label("...", (x-dx,t+dt/2));
%   label("...", (x+nc*dx/2,t+dt/2));
%   x = x0;
%   t = t0 + (1 + nt) * dt + dt;
%   label("n", (x-dx,t+dt/2));
%   for (int j = 0; j < nc; ++j) {
%     x = x0 + j * dx;
%     A=(x,t); B=(x+dx,t); C=(x+dx,t+dt); D=(x,t+dt);
%     draw(A--B--C--D--A, grey);
%   }
%   int steps = 7;
%   for (int i = 0; i < steps; ++i) {
%     t = t0 + i * dt;
%     int j = search(xs,path[i]);
%     x = x0 + j * dx;
%     A=(x,t); B=(x+dx,t); C=(x+dx,t+dt); D=(x,t+dt);
%     // draw(A--B--C--D--A, red);
%     fill(A--B--C--D--A--cycle, red);
%     draw(A--B--C--D--A, grey);
%   }
%   t = t0 + steps * dt;
%   int j = search(xs,path[steps]);
%   x = x0 + j * dx;
%   A=(x,t); B=(x+dx,t); C=(x+dx,t+dt); D=(x,t+dt);
%   // draw(A--B--C--D--A, red);
%   fill(A--B--C--D--A--cycle, red);
%   draw(A--B--C--D--A, grey);
%  \end{asy}
% %
%  \hspace*{1cm}
% %
%  \begin{asy}
%   size(6cm);
%   include graph;
%   string[] xs = {"a","b","c","d","e"};
%   string[] path = {"b","c","d","e","e","d","c","d"};
%   int nc = xs.length;
%   int nt = path.length;
%   real x0 = 0.0;
%   real t0 = 0.0;
%   real dx = 0.1;
%   real dt = 0.1;
%   real x;
%   real t;
%   pair A, B, C, D;
%   defaultpen(2);
%   for (int j = 0; j < nc; ++j) {
%     x = x0 + j * dx;
%     label(xs[j], position=(x+dx/2,t0-1.5*dt), align=N);
%   }
%   for (int i = 0; i < nt; ++i) {
%     x = x0;
%     t = t0 + i * dt;
%     label((string) i, (x-dx,t+dt/2));
%     for (int j = 0; j < nc; ++j) {
%       x = x0 + j * dx;
%       A=(x,t); B=(x+dx,t); C=(x+dx,t+dt); D=(x,t+dt);
%       if (i == 0 && j < nc - 1 - 3) fill(A--B--C--D--A--cycle, lightred);
%       if (i == 1 && j < nc - 1 - 2) fill(A--B--C--D--A--cycle, lightred);
%       if (i == 2 && j < nc - 1 - 1) fill(A--B--C--D--A--cycle, lightred);
%       if (i == 3 && j < nc - 1) fill(A--B--C--D--A--cycle);
%       if (i == 5 && j > nc - 3 + 1) fill(A--B--C--D--A--cycle, lightred);
%       if (i == 6 && j > nc - 3) fill(A--B--C--D--A--cycle);
%       draw(A--B--C--D--A, grey);
%     }
%   }
%   x = x0;
%   t = t0 + (0 + nt) * dt + dt/2;
%   label("...", (x-dx,t+dt/2));
%   label("...", (x+nc*dx/2,t+dt/2));
%   x = x0;
%   t = t0 + (1 + nt) * dt + dt;
%   label("n", (x-dx,t+dt/2));
%   for (int j = 0; j < nc; ++j) {
%     x = x0 + j * dx;
%     A=(x,t); B=(x+dx,t); C=(x+dx,t+dt); D=(x,t+dt);
%     draw(A--B--C--D--A, grey);
%   }
%  \end{asy}
% %
%  \hspace*{1cm}
% %
%  \begin{asy}
%   size(6cm);
%   include graph;
%   string[] xs = {"a","b","c","d","e"};
%   string[] path = {"b","c","d","d","d","c","b","a"};
%   int nc = xs.length;
%   int nt = path.length;
%   real x0 = 0.0;
%   real t0 = 0.0;
%   real dx = 0.1;
%   real dt = 0.1;
%   real x;
%   real t;
%   pair A, B, C, D;
%   defaultpen(2);
%   for (int j = 0; j < nc; ++j) {
%     x = x0 + j * dx;
%     label(xs[j], position=(x+dx/2,t0-1.5*dt), align=N);
%   }
%   for (int i = 0; i < nt; ++i) {
%     x = x0;
%     t = t0 + i * dt;
%     label((string) i, (x-dx,t+dt/2));
%     for (int j = 0; j < nc; ++j) {
%       x = x0 + j * dx;
%       A=(x,t); B=(x+dx,t); C=(x+dx,t+dt); D=(x,t+dt);
%       if (i == 6 && j < nc - 1 - 3) fill(A--B--C--D--A--cycle, lightgrey);
%       if (i == 5 && j < nc - 1 - 2) fill(A--B--C--D--A--cycle, lightgrey);
%       if (i == 4 && j < nc - 1 - 1) fill(A--B--C--D--A--cycle, lightgrey);
%       if (i == 3 && j < nc - 1) fill(A--B--C--D--A--cycle);
%       if (i == 7 && j > nc - 3 + 1) fill(A--B--C--D--A--cycle, lightgrey);
%       if (i == 6 && j > nc - 3) fill(A--B--C--D--A--cycle);
%       draw(A--B--C--D--A, grey);
%     }
%   }
%   x = x0;
%   t = t0 + (0 + nt) * dt + dt/2;
%   label("...", (x-dx,t+dt/2));
%   label("...", (x+nc*dx/2,t+dt/2));
%   x = x0;
%   t = t0 + (1 + nt) * dt + dt;
%   label("n", (x-dx,t+dt/2));
%   for (int j = 0; j < nc; ++j) {
%     x = x0 + j * dx;
%     A=(x,t); B=(x+dx,t); C=(x+dx,t+dt); D=(x,t+dt);
%     draw(A--B--C--D--A, grey);
%   }
%  \end{asy}
\end{figure}

Here, the state space at decision steps 0, 1, 2, 4, 5 and for $t \geq 7$
consists of the cells |a|, |b|, |c|, |d| and |e|. \\

But at decision steps 3 and 6, the black cells do not belong to the
state space space: |X 3 = {e}| and |X 6 = {a, b, c}|. \\

Further, the controls available to the decision maker only support
moving to a adjacent cells. \\

For example, at decision step 0, the decision maker can move from cell
|a| to cell |a| or |b|; from |b|, she can move to |a|, |b| or |c|. And so
on. \\

The set of controls in cells |a|, |b| and |c| at decision step 2 is
empty: |Y 2 a = Y 2 b = Y 2 c = {}|. The controls in |d| and |e| admit
transitions to the only cell contained in |X 3|, namely |e|. \\

Similarly, the set of controls for cell |e| at decision step 5 is empty
and from |d| one can only move to |c|. \\

On the left of the figure you can see a trajectory compatible with these
assumptions in red. \\

In the middle of the figure, the cells from which less than three
decision steps can be done are flagged in light red: in particular, no
steps can be done starting from cells |a|, |b| and |c| at |t = 2| and
from cell |e| at |t = 5|. Only one step can be done starting from cells
|a| and |b| at |t = 1| and two steps can be done from cell |a| at |t =
0|. \\

On the right of the figure, the cells that cannot be reached no matter
what the initial cell at step 0 is and which controls are selected are
greyed.


\subsection{Viability}

In lecture 7 we have specified an SDP in terms of 

> X         :  (t : Nat) -> Type
> 
> Y         :  (t : Nat) -> (x : X t) -> Type
> 
> next      :  (t : Nat) -> (x : X t) -> (y : Y t x) -> X (S t)
>
> Val       :  Type
>
> reward    :  (t : Nat) -> (x : X t) -> (y : Y t x) -> (x' : X (S t)) -> Val
> 
> (<+>)     :  Val -> Val -> Val
>
> zero      :  Val
>
> (<=)      :  Val -> Val -> Type
> 
> lteRefl   :  {a : Val} -> a <= a
>
> lteTrans  :  {a, b, c : Val} -> a <= b -> b <= c -> a <= c
>
> plusMon   :  {a, b, c, d : Val} -> a <= b -> c <= d -> (a <+> c) <= (b <+> d)

The picture makes very clear that, if we allow |Y t x| to be empty for
certain states |x : X t|, we need to be careful in the definition of the
domain and of the codomain of policy functions.

For instance cell |a| cannot be in the domain of a policy for taking
decisions at step 0 that is the head of a policy sequence of length
greater than or equal to three!

Conversely, a policy that supports taking more than 2 decision steps
cannot select a control in cell |b| (|c|) at step 0 that leads to cell
|a| (|b|) at step one!

We can account for these constraints in a simple and general way by
introducing the notion of \emph{viability}.

Informally, a state |x : X t| is viable for |n| steps if one can make
|n| decision steps starting from |x|. We can formalize this idea in
terms of a |Viable n x| type:

> Viable : {t : Nat} -> (n : Nat) -> X t -> Type

We know from lecture 7 that we have to refine the notion of policy. At
the same time, we do not want to impose unnecessary restrictions on
the range of SDPs to which the theory can be applied. 

This suggests that we should probably avoid \emph{defining} |Viable| and
instead specify minimal properties that problem-specific implementations
need to fulfil.

One property of |Viable| is rather obvious: every state should be viable
for zero steps. Thus

> viableSpec0 : {t : Nat} -> (x : X t) -> Viable Z x

We want to formalize the intuition that if one can take |n + 1| decision
steps starting from a state |x|, then |x| must admit a control that
leads to a next state from which at least |n| further steps can be
done. That is, to a next state that is viable for |n| steps:

> viableSpec1  :  {t : Nat} -> {n : Nat} -> (x : X t) -> 
> 
>                 Viable (S n) x -> Exists (\ y => Viable n (next t x y))

The third and last requirement that we impose is the converse: if a
state admits a control which is good for |n| steps, that state is viable
for |n + 1| steps:

> viableSpec2  :  {t : Nat} -> {n : Nat} -> (x : X t) -> 
> 
>                 Exists (\ y => Viable n (next t x y)) -> Viable (S n) x

\begin{shaded}
  \begin{exercise}
    \label{exercise8.1}    
    Give a generic implementation of |Viable| and prove that it fulfills
    |ViableSpec0|, |ViableSpec1| and |ViableSpec2|.
  \end{exercise}
\end{shaded}


\subsubsection*{Exercise \ref{exercise8.1}:}

> Viable {t}    Z     x = Unit
> 
> Viable {t} (  S n)  x = Exists (\ y => Viable n (next t x y))

> viableSpec0 {t} x = ()

> viableSpec1 {t} {n} x (Evidence y gy)  =  Evidence y gy

> viableSpec2 {t} {n} x (Evidence y gy)  =  Evidence y gy


\subsection{Reachability}

In the SDP sketched in the figure, the gray cells on the right cannot be
reached from any initial cell, no matter which controls are selected. 

Including these states in the domain of policies is not logically
problematic but potentially very inefficient.

In concrete SDPs, it is not uncommon that a large number of states in |X
t| are actually unreachable for large |t|.

Computing optimal controls for these states would be an unnecessary
waste of resources.

We can avoid this by restricting the domain of policies of type |Policy
t| to values in |X t| that are actually reachable.

To this end, we need to formalize the notion of reachability. We proceed
in the same way as for viability. Instead of \emph{defining} the notion
of reachability, we specify it.

Client applications will be able to take advantage of the knowledge
about the specific SDP at stake to provide efficient implementations of
|Reachable|:

> Reachable       :  {t' : Nat} -> X t' -> Type

> reachableSpec0  :  (x : X Z) -> Reachable x

> reachableSpec1  :  {t : Nat} -> (x : X t) -> Reachable x -> (y : Y t x) ->

>                    Reachable (next t x y)

The type of |reachableSpec0| encodes the idea that every initial state
is reachable.

The type of |reachableSpec1| formalizes the idea that if |x : X t| is
reachable, every |y : Y t x| implies that |next z x y| is also
reachable.

We also want to encode the idea that if |x' : X (t + 1)| is reachable,
then there must exist a state |x : X t| that is reachable and a control
|y : Y t x| that allows a transition from |x| to |x'|.

\begin{shaded}
  \begin{exercise}
    \label{exercise8.2}    
    Encode this idea in the type of a |reachableSpec2|
    value. Suggestion: first, formalize what it means for a state |x : X
    t| to be a predecessor of a state |x' : X (S t)| by implementing




> Pred : {t : Nat} -> X t -> X (S t) -> Type

    Then refine this notion: define what it means for a state |x : X t|
    to be a reachable predecessor of a state |x' : X (S t)| by
    implementing

> ReachablePred : {t : Nat} -> X t -> X (S t) -> Type

    Finally, give the type of |reachableSpec2|.
  \end{exercise}
\end{shaded}

\subsubsection*{Exercise \ref{exercise8.2}:}

> Pred {t} x x'  =  Exists (\ y => x' = next t x y)

> ReachablePred x x'  = (Reachable x, x `Pred` x')

> reachableSpec2  :  {t : Nat} -> (x' : X (S t)) -> Reachable x' -> Exists (\ x => x `ReachablePred` x')


\begin{shaded}
  \begin{exercise}
    \label{exercise8.3}    
    Give a generic default implementation of |Reachable| and prove that
    it fulfills |reachableSpec0|, |reachableSpec1| and |reachable2|.
  \end{exercise}
\end{shaded}


\subsubsection*{Exercise \ref{exercise8.3}:}

> Reachable {t' = Z}    x'  =  Unit 
>
> Reachable {t' = S t}  x'  =  Exists (\ x => ReachablePred x x')

> reachableSpec0 x = ()

> reachableSpec1 x rx y = Evidence x (rx, Evidence y Refl)

> reachableSpec2 {t} x' rx' = rx'




\subsection{Policies and policy sequences revisited}

We are now ready to refine the notion of policy to avoid the
difficulties discussed in lecture 7. Recall that there we defined

< Policy : (t : Nat) -> Type
< 
< Policy t = (x : X t) -> Y t x

and then realized that a policy which does not support |n| decision steps
cannot be the first element of a policy sequence of length |n|. \\

We encode the idea that a policy at decision step |t| might only support
a finite number |n| of decision steps with an additional parameter:

> Policy : (t : Nat) -> (n : Nat) -> Type
 
For |n = Z| (zero decison steps) we do not actually need any rule and we
can define |Policy t Z| to be the sigleton type: 

< Policy t Z      =  Unit

For |n = S m| we want to express the idea that a value of type |Policy t
n| is a decision rule which associates to each state in |x : X t| that is
reachable and viable for |n| steps a good control in |Y t x|. \\

A good control in |Y t x| is just a |y : Y t x| paired with a proof that
|y| is good:

> GoodY : (t : Nat) -> (x : X t) -> (m : Nat) -> Type
> 
> GoodY t x m = Sigma (Y t x) (\ y => Viable m (next t x y))

With a notion of good controls in place, we can define what a
policy that supports |n = S m| decision steps is. Wrapping up:

> Policy t    Z     =  Unit
> 
> Policy t (  S m)  =  (x : X t) -> Reachable x -> Viable (S m) x -> GoodY t x m

Policy sequences are, as in lecture 7, sequences of policies:

> data PolicySeq : (t : Nat) -> (n : Nat) -> Type where
> 
>   Nil   :  {t : Nat} -> PolicySeq t Z
>   
>   (::)  :  {t, n : Nat} -> Policy t (S n) -> PolicySeq (S t) n -> PolicySeq t (S n)

\textbf{Remark:} Notice the |t (S n)|, |(S t) n|, |t (S n)| pattern in
the |Cons| constructor of policy sequences.


\subsection{The value of policy sequences}

The computation of |val| is essentially as in lecture 7

< val  :  {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Val
<   
< val {t}    Nil       x  =  zero
<   
< val {t} (  p :: ps)  x  =  let y = p x in
< 
<                            let x' = next t x y in
<                            
<                            reward t x y x' <+> val ps x'

but there is a twist. The policy |p| can only be applied to states in |X
t| that are reachable and viable and computes not just a control but a
\emph{good} control! \\

This is crucial because, in order to compute the value of the tail |ps|
in |x' = next t x y|, we have to provide evidence that |x'| is
reachable and viable! \\

The definition of |val| accounts for the fact that we have carefully
restricted both the domain and the codomain of policies.

> val  :  {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Reachable x -> Viable n x -> Val
>   
> val {t}    Nil       x rx vx  =  zero
>
> val {t} (  p :: ps)  x rx vx  =  let gy   =  p x rx vx in
> 
>                                  let y    =  outl gy in
>                                  
>                                  let x'   =  next t x y in
>                                  
>                                  let rx'  =  reachableSpec1 x rx y in  -- ?hole1 
>                                  
>                                  let vx'  =  outr gy in                -- ?hole2
>                                  
>                                  reward t x y x' <+> val ps x' rx' vx'

In the definition of |val|, we have used the helper function
|outl|. |outl| and its counterpart |outr| are just the projections for
existential types: |Sigma|, |Exists|, etc.

< outl : {A : Type} -> {P : A -> Type} -> Sigma A P -> A
< 
< outl (MkSigma a _) = a

< outr : {A : Type} -> {P : A -> Type} -> (s : Sigma A P) -> P (outl s)
< 
< outr (MkSigma _ p) = p

\begin{shaded}
  \begin{exercise}
    \label{exercise8.4}    
    In the definition of |val| we have two holes left: |hole1| and
    |hole2|. Fill in these holes and complete the implementation of
    |val|. Suggestion: recall the specification of |Reachable| and the
    definition of good controls.
  \end{exercise}
\end{shaded}


\subsection{Optimality, optimal extensions, Bellman's principle}

The notions of optimality of policy sequences, of optimal extension of
policy sequences and Bellman's principle are, mutatis mutandis, the same
as in lecture 7:

> OptPolicySeq  :  {t, n : Nat} -> PolicySeq t n -> Type
> 
> OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> 
> 
>                             (x : X t) -> (rx : Reachable x) -> (vx : Viable n x) -> 
> 
>                             val ps' x rx vx <= val ps x rx vx

> OptExt  :  {t, m : Nat} -> PolicySeq (S t) m -> Policy t (S m) -> Type
>
> OptExt {t} {m} ps p  =  (p' : Policy t (S m)) -> 
> 
>                         (x : X t) -> (rx : Reachable x) -> (vx : Viable (S m) x) -> 
> 
>                         val (p' :: ps) x rx vx <= val (p :: ps) x rx vx

> Bellman  :  {t, m : Nat} -> 
> 
>             (ps  :  PolicySeq (S t) m)  ->   OptPolicySeq ps ->
>             
>             (p   :  Policy t (S m))     ->   OptExt ps p ->
>             
>             OptPolicySeq (p :: ps)

Proving Bellman's principle carries over from lecture 7:

> Bellman {t} {m} ps ops p oep = opps where
> 
>   opps (p' :: ps') x rx vx =
>   
>     let gy' =  p' x rx vx in
>     
>     let y'  =  outl gy' in
>     
>     let x'  =  next t x y' in
>     
>     let rx'  =  reachableSpec1 x rx y' in
>     
>     let vx'  =  outr gy' in
>     
>     let s1   =  plusMon lteRefl (ops ps' x' rx' vx') in
>     
>     let s2   =  oep p' x rx vx in
>     
>     lteTrans s1 s2


\subsection{Generic verified backwards induction}

Apart from the additional index in the type of policies, this part of the theory is
unchanged from lecture 7:

> nilOptPolicySeq : OptPolicySeq Nil
> 
> nilOptPolicySeq Nil x rx vx = lteRefl

> optExt  :  {t, n : Nat} -> PolicySeq (S t) n -> Policy t (S n)

> optExtSpec  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)

> bi : (t : Nat) -> (n : Nat) -> PolicySeq t n
> 
> bi t    Z     =  Nil
> 
> bi t (  S n)  =  let ps = bi (S t) n in optExt ps :: ps

> biLemma : (t : Nat) -> (n : Nat) -> OptPolicySeq (bi t n)
> 
> biLemma t    Z     =  nilOptPolicySeq
> 
> biLemma t (  S n)  =  let ps   =  bi (S t) n in
> 
>                       let ops  =  biLemma (S t) n in
>                       
>                       let p    =  optExt ps in
>                     
>                       let oep  =  optExtSpec ps in
>                     
>                       Bellman ps ops p oep


\subsection{What have we gained?}

Let's consider again the example from lecture 7:

> head : {t, n : Nat} -> PolicySeq t (S n) -> Policy t (S n)
> 
> head (p :: ps) = p 

> tail : {t, n : Nat} -> PolicySeq t (S n) -> PolicySeq (S t) n
> 
> tail (p :: ps) = ps 

> data GoodOrBad  =  Good  |  Bad

> implementation Show GoodOrBad where
> 
>   show  Good  =  "Good"
> 
>   show   Bad  =   "Bad"

> data UpOrDown   =  Up    |  Down

> X t  =  GoodOrBad

> Y t  Good  =  UpOrDown 
> 
> Y t   Bad  =  Void

> next t  Good  Up    =  Good
>
> next t  Good  Down  =  Bad
>   
> next t  Bad   v     impossible

> Val = Nat
>
> (<+>) = (+)
>
> zero = Z
>
> (<=) = Prelude.Nat.LTE
>
> reward t Good    Up x'  =  1
> 
> reward t Good  Down x'  =  3
> 
> reward t  Bad     v x'  impossible

Notice that, as in lecture 7, we will not be able to compute an argument
|v : Void| to apply |reward|.

Remember that policies are now parameterized on two natural numbers: |t|
and |n|. The second one characterizes how many decision steps the policy
does support.

In implementing |optExt|, we have to distinguish between policy
extensions of policy sequences of length zero and policy extensions of
sequences of length greater or equal to one.

In the first case we can select both |Up| and |Down| because, even
though the second control implies a transition to |Bad| (remember that
the control set of |Bad| is void!), no further decision steps are
required from there:

> optExt {t} {n = Z} ps Good rGood vGood =
> 
>   let x1'      =  next t Good  Up in
>   
>   let rx1'     =  reachableSpec1 Good rGood Up in
>   
>   let vx1'     =  viableSpec0 {t = S t} x1' in
>   
>   let x2'      =  next t Good  Down in
>   
>   let rx2'     =  reachableSpec1 Good rGood Down in
>   
>   let vx2'     =  viableSpec0 {t = S t} x2' in
>   
>   let valUp    =  reward t Good Up    (next t Good  Up    )  <+>  val ps x1' rx1' vx1' in
>   
>   let valDown  =  reward t Good Down  (next t Good  Down  )  <+>  val ps x2' rx2' vx2' in
>   
>   if valUp >= valDown then (MkSigma Up ()) else (MkSigma Down ())
>   
> optExt {t} {n = Z} ps Bad rBad (Evidence v _) = absurd v

\begin{shaded}
  \begin{exercise}
    \label{exercise8.5}    
    Notice that, in contrast to lecture 7, we can now give a complete
    implementation of the (absurd) |Bad| case. Do you see why |v| is
    absurd?
  \end{exercise}
\end{shaded}

In the second case, we \emph{know} that only one control supports a
transition to a next state from which further decision steps are
doable. Thus, we just pick up this control:

> optExt {t} {n = S m} ps Good rGood vGood = 
> 
>   let ey = viableSpec1 {t = t} {n = S m} Good vGood in
>   
>   MkSigma (getWitness ey) (getProof ey)
>     
> optExt {t} {n = S m} ps Bad rBad (Evidence v _) = absurd v

We can now implement the same computation as in lecture 7. In contrast
to lecture 7, however we have to provide (compute, construct) evidences
that our initial state |x0 = Good| is reachable and viable for two
steps!

> computation : IO () 

> computation  =  let ps   =  bi 0 2 in
> 
>                 let x0   =  Good in
>                 
>                 let rx0  =  () in
>                 
>                 let vx0  =  Evidence Up (Evidence Up ()) in
>                 
>                 let p0   =  head ps in
>                 
>                 let gy0  =  p0 x0 rx0 vx0 in
>                 
>                 let y0   =  outl gy0 in
>                 
>                 let x1   =  next Z x0 y0 in
>                 
>                 let rx1  =  reachableSpec1 {t = 0} x0 rx0 y0 in
>                 
>                 let vx1  =  outr gy0 in
>                 
>                 let p1   =  head (tail ps) in
>                 
>                 let gy1  =  p1 x1 rx1 vx1 in
>                 
>                 let y1   =  outl gy1 in
>                 
>                 let x2   =  next 1 x1 y1 in
>                 
>                 do  putStrLn ("x0 = " ++ show x0)
>                 
>                     putStrLn ("x1 = " ++ show x1)
>      
>                     putStrLn ("x2 = " ++ show x2)

\begin{shaded}
  \begin{exercise}
    \label{exercise8.6}    
    Comment the single steps of the implementation of |computation|.
  \end{exercise}
\end{shaded}

> main : IO ()
> 
> main = computation

\begin{shaded}
  \begin{exercise}
    \label{exercise8.7}    
    Do you expect this program to terminate? If so, what do you expect
    to be the result of the computation? Run |main| from the
    terminal. Do you obtain the expected result?
  \end{exercise}
\end{shaded} 


\subsection{Wrap-up, outlook}

\begin{itemize}

  \item We have managed to fix a deficiency of the naive theory from
    lecture 7.

  \item The new theory requires stronger guarantees from the initial
    states.

  \item Attempts at computing |n| optimal decisions starting from states
    that support less than |n| decision steps are detected at compile
    time.

  \item We still face a major problem, however: fulfilling |optExtSpec|
    for problem-specific implementations of |optExt| like the ones
    discussed here and in lecture 7 is difficult and time consuming!

  \item What we need is a \emph{generic} implementation of |optExt|.

  \item And, in order to apply the theory to decision problems under
    uncertainty and imperfect information, we need to extend it to
    \emph{monadic} SDPs.

\end{itemize}


\section*{Solutions}




\subsubsection*{Exercise \ref{exercise8.4}:}

<  rx'  =  reachableSpec1 x rx y

<  vx'  =  outr gy


