% -*-Latex-*-
%\documentclass[a4paper,landscape]{slides}
\documentclass[handout,colorhighlight,coloremph]{beamer}
\usepackage[mode=buildnew]{standalone}
\usetheme{boxes}
\usepackage{color,soul}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[english]{babel}
\usepackage{ucs}
\usepackage[utf8x]{inputenc}
\usepackage{csquotes}
\usepackage{moreverb}
\usepackage{fancyvrb}
\usepackage{xcolor}
\usepackage{asymptote}
\usepackage{units}
\usepackage{tikz}
\usetikzlibrary{automata}
\usetikzlibrary{positioning}
%\usetikzlibrary{arrows.meta}


\setbeamertemplate{navigation symbols}{}%remove navigation symbols


\newcommand{\yslant}{0.5}
\newcommand{\xslant}{-1.5}

%include lhs.fmt

\input{macros.TeX}
\input{pik_colors.tex}

\definecolor{beamerblue}{rgb}{0.2,0.2,0.7} 

\addheadbox{section}{
  \quad \tiny
  \color{beamerblue} Decision problems in climate research
  \insertsection
}
%\addfootbox{section}{\quad \tiny Modelling Strategy Seminar, PIK, June
%  2012}

\title{Decision problems in climate research}

\author{Nicola Botta\inst{1,2}, Nuria Brede\inst{1}}

\institute{\inst{1}{Potsdam Institute for Climate Impact Research} \\
           \inst{2}{CSE, Chalmers University of Technology}}

\setbeamertemplate{frametitle}[default][center]




\begin{document}

\date{}

\frame{\maketitle}

\begin{frame}
  \frametitle{Objectives of this lecture}
\begin{itemize}
\item Get acquainted with the general idea of using formal methods for
  climate impact research 
\item Learn about basic questions concerning uncertainties in
  sequential decision making
\item Go through two basic examples of decision problems in climate research
      and analyze their common features
\end{itemize}
\end{frame}



%% -------------------------------------------------------------------

\section{\ $\rightarrow$}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Climate research and decision making}
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$ \ Climate research and decision making}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> Climate research shall \textcolor<1->{red}{improve} our
  \textcolor<1->{red}{understanding} of the earth system and climate but also \dots
%
\vfill
\item<2-> \dots inform \textcolor<2->{red}{rational},
  \textcolor<2->{red}{transparent}, \textcolor<2->{red}{accountable}
  and, above all, \textcolor<2->{red}{good} decisions!
%  
\vfill
\item<3-> Best \textcolor<3->{red}{policies} for a decision problem
  typically depend on the \textcolor<3->{red}{uncertainties} that affect
  that problem but \dots
%
\vfill
\item<4-> \dots in climate research, many unavoidable uncertainties
  cannot be estimated through established \textcolor<4->{red}{theories}
  or \textcolor<4->{red}{model} simulations!
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example 1: emission reduction policies}
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$ \ Example 1: emission reduction policies}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> Global GHG emissions have to be reduced
  \textcolor<1->{red}{readily} to avoid \textcolor<1->{red}{possibly}
  unmanageable impacts of climate change:
  %
  \pause
  \begin{minipage}[t][6cm][t]{1.0\textwidth}
  \begin{figure}[htbp]
    \centering
    \includegraphics[clip, trim=2.0cm 6cm 0.0cm 9cm, width=0.83\textwidth]{./img/sr15_spm_final_p19.pdf}
  \end{figure}
  \end{minipage}
  %%\vspace*{15mm}
  \vfill
  \hfill
  \tiny{IPCC Special Report - Global Warming of \unit[1.5]{°C}, Oct. 2018}
  %
\vfill
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> Too fast reductions \textcolor<1->{red}{may} compromise the
  wealth of one or more upcoming generations but ...
%
\vfill
\item<2-> ... they \textcolor<2->{red}{may} promote a transition to societies
  that are more wealthy, safe, fair and manageable.
%
\vfill
\item<3-> New technologies that significantly reduce the costs of fast
  emission reductions \textcolor<3->{red}{may} become available soon.
%
\vfill
\item<4-> Already taken decisions \textcolor<4->{red}{may} not be
  implemented or they may be implemented with delays.
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> The implication of these \textcolor<1->{red}{may} is that
  results like this
  %
  \begin{figure}
    \centering
    \includegraphics[clip, width=0.6\textwidth]{./img/sr15_spm_final_p19_1.pdf}
  \end{figure}
  \vfill
  %
%
are very useful to identify a corridor of viable options but also raise
a number of difficult questions:
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> What are the risks associated with the upper
  (\textcolor<1->{red}{H}) and lower (\textcolor<1->{red}{L})
  boundaries of the emissions corridor? What their costs?
  %
  \begin{figure}
    \centering
    \includegraphics[clip, width=0.6\textwidth]{./img/sr15_spm_final_p19_2.pdf}
  \end{figure}
  \vfill
  %
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> Assume that, between 2040 and 2050, new technologies makes it
  possible to reduce GHG emissions at low costs:
  %
  \begin{figure}
    \centering
    \includegraphics[clip, width=0.6\textwidth]{./img/sr15_spm_final_p19_3.pdf}
  \end{figure}
  \vfill
  %
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> What are ``optimal'' emission paths under this scenario?
  \pause What can optimal possibly mean in this context?
  %
  \begin{figure}
    \centering
    \includegraphics[clip, width=0.6\textwidth]{./img/sr15_spm_final_p19_4.pdf}
  \end{figure}
  \vfill
  %
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> Perhaps are \textcolor<1->{red}{H} emissions until 2040 too
  dangerous? Or \textcolor<1->{red}{L} emissions after 2050 too expensive?
  %
  \begin{figure}
    \centering
    \includegraphics[clip, width=0.6\textwidth]{./img/sr15_spm_final_p19_5.pdf}
  \end{figure}
  \vfill
  %
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Decision making under uncertainty: basic questions}
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$ \ Decision making under uncertainty: basic questions}

%% -------------------------------------------------------------------


\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> In presence of uncertainties, the notion of ``optimal path of
  decisions'' becomes \textcolor<1->{red}{devoid of meaning}!
%
\vfill
\item<2-> But the notion of optimal decision rule =
  \textcolor<2->{red}{optimal policy} is still meaningful and relevant!
%
\vfill
\item<3-> How do \textcolor<3->{red}{optimal policies} look like and
  what is the impact of uncertainties on optimal policies?
%
\vfill
\item<4-> How do optimal policies \textcolor<4->{red}{change} if we
  account for the fact that technological innovations could become
  available later or earlier?
%
\vfill
\item<5-> Or that there is a non-zero probability of exceeding critical
  thresholds even if we stay within the IPCC emission corridor?
%
\vfill
\item<6-> What if decisions are \textcolor<6->{red}{not implemented}
  according to plan or it they are delayed?
%
\vfill
\item<7-> Can we account for these uncertainties rigorously, at least
  for idealized decision problems?
%  
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example 2: a generation dilemma (Heitzig et al. 2018)}
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$ \ Example 2: a generation dilemma (Heitzig et al. 2018)}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<2-> The world can be in one of four states:
  \textcolor<2->{blue}{GU}, \textcolor<2->{blue}{GS}, \textcolor<2->{blue}{B}
  and \textcolor<2->{blue}{BT}.
%  
\vfill
\item<3-> \textcolor<3->{blue}{B} is a \textcolor<3->{red}{bad} state,
  one in which resources are depleted and the wealth of the societies is
  low.
%  
\vfill

\item<4-> \textcolor<4->{blue}{GS} is a \textcolor<3->{red}{good},
  \textcolor<3->{red}{safe} state. In \textcolor<4->{blue}{GS}, plenty of
  resources are available, societies are wealthy and there is no risk
  for the world to turn into \textcolor<4->{blue}{B},
  \textcolor<4->{blue}{GU} or \textcolor<4->{blue}{BT}.

%
\vfill
\item<5-> \textcolor<5->{blue}{GU} is a \textcolor<5->{red}{good} but
  \textcolor<5->{red}{unsafe} state. In \textcolor<5->{blue}{GU}, plenty
  of resources are available, societies are wealthy but there is a
  significant risk for the world to turn into \textcolor<5->{blue}{B}.
%
\vfill
\item<6-> \textcolor<6->{blue}{BT} is a \textcolor<6->{red}{bad} but
  \textcolor<6->{red}{temporary} state. In \textcolor<6->{blue}{BT},
  societies are poor but it is known for certain that the next state
  will be \textcolor<6->{blue}{GS}.
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> A generation in \textcolor<1->{blue}{B},
  \textcolor<1->{blue}{BT} or \textcolor<1->{blue}{GS} has no options: the
  next states will be \textcolor<1->{blue}{B}, \textcolor<1->{blue}{GS}
  and \textcolor<1->{blue}{GS}, respectively.

%  
\vfill
\item<2-> A generation in \textcolor<2->{blue}{GU} also has two options:
  \textcolor<2->{red}{a} and \textcolor<2->{red}{b}. If it picks
  \textcolor<2->{red}{a}, the next generation will possibly be in
  \textcolor<2->{blue}{GU} again. But it can also end up in
  \textcolor<2->{blue}{B}.  \pause If it picks \textcolor<2->{red}{b},
  the next generation will certainly be in \textcolor<2->{blue}{BT}.
%
\end{itemize}
%
\pause
\vfill
\begin{figure}
\begin{tikzpicture}[thick]
  % Draw the states
    \node[state]              (gu) {\textcolor{blue}{GU}};
    \node[state, right=of gu] (bt) {\textcolor{blue}{BT}};
    \node[state, below=of gu]  (b)  {\textcolor{blue}{B}};
    \node[state, below=of bt] (gs) {\textcolor{blue}{GS}};
  
    % Connect the states with arrows
    \draw[every loop]
      (gu) edge[out=130,in=170,looseness=6]  node[above] {\textcolor{red}{a}} (gu)
      (gu) edge                              node[left]  {\textcolor{red}{a}}  (b)
      (gu) edge                              node[above] {\textcolor{red}{b}} (bt)
      (bt) edge             node { } (gs)      
      (gs) edge[loop right] node { } (gs)      
       (b) edge[loop left]  node { }  (b);      
\end{tikzpicture}
\end{figure}
%
\begin{itemize}
%
\vfill
\item<4-> What should a generation in \textcolor<4->{blue}{GU} do? \textcolor<4->{red}{a} or \textcolor<4->{red}{b}?
%  
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Examples 1 and 2: common traits}
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$ \ Examples 1 and 2: common traits}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> Both decision problems have the form of a
  \textcolor<1->{red}{dilemma}.
%
\vfill
\item<2-> In both cases, the consequences of decisions are \textcolor<2->{red}{uncertain}.
%
\vfill
\item<3-> Decisions are taken \textcolor<3->{red}{sequentially} as time unfolds.
%
\vfill
\item<4-> Decisions might be implemented with delays or not implemented at all.
%
\vfill
\item<5-> Can we exploit these similarities? Can we develop a method for
  \textcolor<5->{red}{specifying} and \textcolor<5->{red}{solving} these
  and similar decisions problems rigorously? What does this mean in the
  specific examples?
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example 1 revisited: a tentative specification}
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$ \ Example 1 revisited: a tentative specification}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<2-> A decision maker has to take a \textcolor<2->{red}{sequence}
  of decisions about GHG emissions, one after the other.
%
\vfill
\item<3-> At each decision step, it can only pick up one of two options:
  \textcolor<3->{red}{L} emissions or \textcolor<3->{red}{H}
  emissions of a ``safe'' emission corridor.
%
\vfill
\item<4-> The decision taken by the decision maker
  \textcolor<4->{red}{may} or \textcolor<4->{red}{may not} be implemented
  during the time until the next decision has to be taken.
%
\vfill
\item<5-> If implemented, \textcolor<5->{red}{L} emissions increase
  \textcolor<5->{red}{cumulated emissions} less than \textcolor<5->{red}{H} emissions.
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> At each step, the decision maker has to choose between
  \textcolor<1->{red}{L} and \textcolor<1->{red}{H} emissions on
  the basis of four data:
%
  \begin{itemize}
  %
  \vfill
  \item<2-> The current amount of \textcolor<2->{red}{cumulated emissions}.
  %
  \vfill
  \item<3-> The \textcolor<3->{red}{current} emission level:
    \textcolor<3->{red}{L} or \textcolor<3->{red}{H}.
  %
  \vfill
  \item<4-> The \textcolor<4->{red}{availability} of technologies for
    reducing emissions.
  %
  \vfill
  \item<5-> A state of the world which can be either
    \textcolor<5->{red}{good} or \textcolor<5->{red}{bad} .
  %  
  \end{itemize}
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> At the first decision step, the decision maker observes
  (relatively) \textcolor<1->{red}{low} cumulated emissions,
  \textcolor<1->{red}{H} current emissions,
  \textcolor<1->{red}{unavailable} technologies and a
  \textcolor<1->{red}{good} world.
%
\vfill
\item<2-> In this state, the \textcolor<2->{red}{probability} that
  the world turns bad is (relatively) \textcolor<2->{red}{low}.
%  
\vfill
\item<3-> But if the cumulated emissions increase beyond a
  \textcolor<3->{red}{critical threshold}, the probability that the world
  becomes bad steeply \textcolor<3->{red}{increases}.
%
\vfill
\item<4-> Once the world has reached a bad state, there is no chance to
  turn back to a good state.
%
\vfill
\item<5-> Similarly, the probability that new technologies become
  available is \textcolor<5->{red}{low} at the first decision steps. It
  increases steeply after 2040 or, equivalently, after a
  \textcolor<5->{red}{critical} number of steps.
%  
\vfill
\item<6-> Once available, technologies stay available for ever.
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> Being in a \textcolor<1->{red}{bad} world yields
  \textcolor<1->{red}{less} benefits than being in a
  \textcolor<1->{red}{good} world.
%  
\vfill
\item<2-> \textcolor<2->{red}{L} current emissions yield
  \textcolor<2->{red}{less} benefits (more costs, less growth) than
  \textcolor<2->{red}{H} current emissions.
%
\vfill
\item<3-> Implementing low emissions when technologies are
  \textcolor<3->{red}{unavailable} costs more than implementing
  emissions when technologies are \textcolor<3->{red}{available}.
%
\vfill
\item<4-> The decision maker aim at maximising \textcolor<4->{red}{a}
  sum of the benefits over all decision steps.
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{More details does not mean a better understanding!}
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$ \ More details does not mean a better understanding!}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> We have introduced more \textcolor<1->{red}{details} but we
  are still very far from a complete \textcolor<1->{red}{specification}
  and \textcolor<1->{red}{understanding} of the decision problem!
%
\vfill
\item<2-> Even if we assume that \textcolor<2->{red}{all} the
  \textcolor<2->{red}{uncertainties} that affect the problem have been
  \textcolor<2->{red}{specified}, advising the decision maker requires
  answering a number of crucial questions:
%
  \begin{itemize}
  %
  \vfill
  \item<3-> What kind of \textcolor<3->{red}{solutions} or
    \textcolor<3->{red}{advice} can we offer to the decision maker?
  %
  \vfill
  \item<4-> Can we \textcolor<4->{red}{compute} these solutions or
    advice in a rigorous way? What do we need to do so?
  %    
  \vfill
  \item<6-> Can we guarantee that these solutions or advice are
    \textcolor<6->{red}{correct}?  What does this mean?
  %
  \end{itemize}
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> We answer these questions in \textcolor<1->{red}{three} steps:
  %
  \begin{itemize}
  %
  \vfill
  \item<2-> \textcolor<2->{red}{Abstract away} the details of specific
    decision problems.
  \vfill
  \item<3-> Formulate a whole \textcolor<3->{red}{class} of
    \textcolor<3->{red}{decision problems} rigorously.
  \vfill
  \item<4-> Derive generic, \textcolor<4->{red}{accountable} = correct
    by construction solution methods for these problems.
    
  %
  \end{itemize}
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Abstracting away the details: an informal view}
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$ \ Abstracting away the details: an informal view}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
There are |n + 1| decision steps to go \dots
%
\begin{figure}[h]
 \begin{asy}
  include graph;
  size(11cm);
  int o = 1;
  int l = 8;
  int h = 4;
  pair A, B, C, D;
  int x = 0;
  real[] midxs1;
  real[] midys1;
  real[] midxs2;
  real[] midys2;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs1.push(x + l/2);
    midys1.push(0);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2),.5), blue);
      }
    } else
    {
    A=(x,0); B=(x,h); C=(x+l,h); D=(x+l,0);
    draw(A--B--C--D--A,blue);
    }
  }
  int cx = l+o+floor(l/2);
  int cy = h + h + h;
  int cr = floor(h);
  draw(Circle((cx,cy),cr), white);
  draw(Circle((cx-1.5,cy-1.5),0.5), white);
  draw(Circle((cx+2,cy+2),0.5), white);
  draw(Circle((cx-0.2,cy+1.5),0.5), white);
  draw(Circle((cx+1.8,cy-1.2),0.5), white);
  draw(Circle((cx+1.8,cy-1.2),0.5), white);
  label("n+1 steps left", (x+17,2));
  int y = -15;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs2.push(x + l/2);
    midys2.push(y+h);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2+y),.5), white);
      }
    } else
    {
    A=(x,y); B=(x,y+h); C=(x+l,y+h); D=(x+l,y);
    if (i == 0 || i == 5)
    {
    draw(A--B--C--D--A,white);
    } else
    {
    draw(A--B--C--D--A,white);
    }
    }
  }
  label("n steps left", (x+17,y+2), white);
  for (int j = 5; j < 5; ++j)
  {
    draw((midxs1[1],midys1[1])--(midxs2[j],midys2[j]), white, EndArrow);
  }
\end{asy}
\end{figure}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\dots here is the current state,
%
\begin{figure}[h]
 \begin{asy}
  include graph;
  size(11cm);
  int o = 1;
  int l = 8;
  int h = 4;
  pair A, B, C, D;
  int x = 0;
  real[] midxs1;
  real[] midys1;
  real[] midxs2;
  real[] midys2;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs1.push(x + l/2);
    midys1.push(0);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2),.5), blue);
      }
    } else
    {
      A=(x,0); B=(x,h); C=(x+l,h); D=(x+l,0);
      if (i == 1) {
         fill(A--B--C--D--cycle,blue);
      } else {
         draw(A--B--C--D--A,blue);
      }
    }
  }
  int cx = l+o+floor(l/2);
  int cy = h + h + h;
  int cr = floor(h);
  draw(Circle((cx,cy),cr), white);
  draw(Circle((cx-1.5,cy-1.5),0.5), white);
  draw(Circle((cx+2,cy+2),0.5), white);
  draw(Circle((cx-0.2,cy+1.5),0.5), white);
  draw(Circle((cx+1.8,cy-1.2),0.5), white);
  draw(Circle((cx+1.8,cy-1.2),0.5), white);
  label("n+1 steps left", (x+17,2));
  int y = -15;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs2.push(x + l/2);
    midys2.push(y+h);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2+y),.5), white);
      }
    } else
    {
    A=(x,y); B=(x,y+h); C=(x+l,y+h); D=(x+l,y);
    if (i == 0 || i == 5)
    {
    draw(A--B--C--D--A,white);
    } else
    {
    draw(A--B--C--D--A,white);
    }
    }
  }
  label("n steps left", (x+17,y+2), white);
  for (int j = 5; j < 5; ++j)
  {
    draw((midxs1[1],midys1[1])--(midxs2[j],midys2[j]), white, EndArrow);
  }
\end{asy}
\end{figure}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\dots here are your options.
%
\begin{figure}[h]
 \begin{asy}
  include graph;
  size(11cm);
  int o = 1;
  int l = 8;
  int h = 4;
  pair A, B, C, D;
  int x = 0;
  real[] midxs1;
  real[] midys1;
  real[] midxs2;
  real[] midys2;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs1.push(x + l/2);
    midys1.push(0);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2),.5), blue);
      }
    } else
    {
      A=(x,0); B=(x,h); C=(x+l,h); D=(x+l,0);
      if (i == 1) {
         fill(A--B--C--D--cycle,blue);
      } else {
         draw(A--B--C--D--A,blue);
      }
    }
  }
  int cx = l+o+floor(l/2);
  int cy = h + h + h;
  int cr = floor(h);
  draw(Circle((cx,cy),cr), blue);
  draw(Circle((cx-1.5,cy-1.5),0.5), green);
  draw(Circle((cx+2,cy+2),0.5), yellow);
  draw(Circle((cx-0.2,cy+1.5),0.5), brown);
  draw(Circle((cx+1.8,cy-1.2),0.5), red);
  draw(Circle((cx-2.1,cy+1.2),0.5), black);
  label("n+1 steps left", (x+17,2));
  int y = -15;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs2.push(x + l/2);
    midys2.push(y+h);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2+y),.5), white);
      }
    } else
    {
    A=(x,y); B=(x,y+h); C=(x+l,y+h); D=(x+l,y);
    if (i == 0 || i == 5)
    {
    draw(A--B--C--D--A,white);
    } else
    {
    draw(A--B--C--D--A,white);
    }
    }
  }
  label("n steps left", (x+17,y+2), white);
  for (int j = 5; j < 5; ++j)
  {
    draw((midxs1[1],midys1[1])--(midxs2[j],midys2[j]), white, EndArrow);
  }
\end{asy}
\end{figure}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
Pick one!
%
\begin{figure}[h]
 \begin{asy}
  include graph;
  size(11cm);
  int o = 1;
  int l = 8;
  int h = 4;
  pair A, B, C, D;
  int x = 0;
  real[] midxs1;
  real[] midys1;
  real[] midxs2;
  real[] midys2;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs1.push(x + l/2);
    midys1.push(0);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2),.5), blue);
      }
    } else
    {
      A=(x,0); B=(x,h); C=(x+l,h); D=(x+l,0);
      if (i == 1) {
         fill(A--B--C--D--cycle,blue);
      } else {
         draw(A--B--C--D--A,blue);
      }
    }
  }
  int cx = l+o+floor(l/2);
  int cy = h + h + h;
  int cr = floor(h);
  draw(Circle((cx,cy),cr), blue);
  fill(Circle((cx-1.5,cy-1.5),0.5), green);
  draw(Circle((cx+2,cy+2),0.5), yellow);
  draw(Circle((cx-0.2,cy+1.5),0.5), brown);
  draw(Circle((cx+1.8,cy-1.2),0.5), red);
  draw(Circle((cx-2.1,cy+1.2),0.5), black);
  label("n+1 steps left", (x+17,2));
  int y = -15;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs2.push(x + l/2);
    midys2.push(y+h);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2+y),.5), white);
      }
    } else
    {
    A=(x,y); B=(x,y+h); C=(x+l,y+h); D=(x+l,y);
    if (i == 0 || i == 5)
    {
    draw(A--B--C--D--A,white);
    } else
    {
    draw(A--B--C--D--A,white);
    }
    }
  }
  label("n steps left", (x+17,y+2), white);
  for (int j = 5; j < 5; ++j)
  {
    draw((midxs1[1],midys1[1])--(midxs2[j],midys2[j]), white, EndArrow);
  }
\end{asy}
\end{figure}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
Move to a new state and \dots
%
\begin{figure}[h]
 \begin{asy}
  include graph;
  size(11cm);
  int o = 1;
  int l = 8;
  int h = 4;
  pair A, B, C, D;
  int x = 0;
  real[] midxs1;
  real[] midys1;
  real[] midxs2;
  real[] midys2;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs1.push(x + l/2);
    midys1.push(0);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2),.5), blue);
      }
    } else
    {
      A=(x,0); B=(x,h); C=(x+l,h); D=(x+l,0);
      if (i == 1) {
         fill(A--B--C--D--cycle,blue);
      } else {
         draw(A--B--C--D--A,blue);
      }
    }
  }
  int cx = l+o+floor(l/2);
  int cy = h + h + h;
  int cr = floor(h);
  draw(Circle((cx,cy),cr), blue);
  fill(Circle((cx-1.5,cy-1.5),0.5), green);
  draw(Circle((cx+2,cy+2),0.5), yellow);
  draw(Circle((cx-0.2,cy+1.5),0.5), brown);
  draw(Circle((cx+1.8,cy-1.2),0.5), red);
  draw(Circle((cx-2.1,cy+1.2),0.5), black);
  label("n+1 steps left", (x+17,2));
  int y = -15;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs2.push(x + l/2);
    midys2.push(y+h);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2+y),.5), blue);
      }
    } else
    {
    A=(x,y); B=(x,y+h); C=(x+l,y+h); D=(x+l,y);
    if (i == 0 || i == 5)
    {
    draw(A--B--C--D--A,white);
    } else
    {
    draw(A--B--C--D--A,blue);
    }
    }
  }
  label("n steps left", (x+17,y+2), black);
  for (int j = 2; j < 3; ++j)
  {
    draw((midxs1[1],midys1[1])--(midxs2[j],midys2[j]), red,
    EndArrow);
  }
\end{asy}
\end{figure}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\dots collect rewards and face the next decision step!
%
\begin{figure}[h]
 \begin{asy}
  include graph;
  size(11cm);
  int o = 1;
  int l = 8;
  int h = 4;
  pair A, B, C, D;
  int x = 0;
  real[] midxs1;
  real[] midys1;
  real[] midxs2;
  real[] midys2;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs1.push(x + l/2);
    midys1.push(0);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2),.5), blue);
      }
    } else
    {
      A=(x,0); B=(x,h); C=(x+l,h); D=(x+l,0);
      if (i == 1) {
         draw(A--B--C--D--A,blue);
      } else {
         draw(A--B--C--D--A,blue);
      }
    }
  }
  int cx = l+o+floor(l/2);
  int cy = h + h + h;
  int cr = floor(h);
  draw(Circle((cx,cy),cr), blue);
  fill(Circle((cx-1.5,cy-1.5),0.5), green);
  draw(Circle((cx+2,cy+2),0.5), yellow);
  draw(Circle((cx-0.2,cy+1.5),0.5), brown);
  draw(Circle((cx+1.8,cy-1.2),0.5), red);
  draw(Circle((cx-2.1,cy+1.2),0.5), black);
  label("n+1 steps left", (x+17,2));
  int y = -15;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs2.push(x + l/2);
    midys2.push(y+h);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2+y),.5), blue);
      }
    } else
    {
    A=(x,y); B=(x,y+h); C=(x+l,y+h); D=(x+l,y);
    if (i == 0 || i == 5)
    {
    draw(A--B--C--D--A,white);
    } else
    {
      if (i == 2) {
         fill (A--B--C--D--cycle,blue);
      } else {
         draw(A--B--C--D--A,blue);
      }
    }
    }
  }
  label("n steps left", (x+17,y+2), black);
  for (int j = 2; j < 3; ++j)
  {
    draw((midxs1[1],midys1[1])--(midxs2[j],midys2[j]), red,
    EndArrow);
  }
\end{asy}
\end{figure}
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
What if there are more than one next possible states?
%
\pause
%
\begin{figure}[h]
 \begin{asy}
  include graph;
  size(11cm);
  int o = 1;
  int l = 8;
  int h = 4;
  pair A, B, C, D;
  int x = 0;
  real[] midxs1;
  real[] midys1;
  real[] midxs2;
  real[] midys2;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs1.push(x + l/2);
    midys1.push(0);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2),.5), blue);
      }
    } else
    {
      A=(x,0); B=(x,h); C=(x+l,h); D=(x+l,0);
      if (i == 1) {
         fill(A--B--C--D--cycle,blue);
      } else {
         draw(A--B--C--D--A,blue);
      }
    }
  }
  int cx = l+o+floor(l/2);
  int cy = h + h + h;
  int cr = floor(h);
  draw(Circle((cx,cy),cr), blue);
  fill(Circle((cx-1.5,cy-1.5),0.5), green);
  draw(Circle((cx+2,cy+2),0.5), yellow);
  draw(Circle((cx-0.2,cy+1.5),0.5), brown);
  draw(Circle((cx+1.8,cy-1.2),0.5), red);
  draw(Circle((cx-2.1,cy+1.2),0.5), black);
  label("n+1 steps left", (x+17,2));
  int y = -15;
  for (int i = 0; i < 6; ++i)
  {
    x = i * (l + o);
    midxs2.push(x + l/2);
    midys2.push(y+h);
    if (i == 3) 
    {
      real a = (l+2*o)/3;
      for (int j = 1; j < 4; ++j)
      {
        draw(Circle((x-2*o+j*a-0.5,2+y),.5), blue);
      }
    } else
    {
    A=(x,y); B=(x,y+h); C=(x+l,y+h); D=(x+l,y);
    if (i == 0 || i == 5)
    {
    draw(A--B--C--D--A,white);
    } else
    {
    draw(A--B--C--D--A,blue);
    }
    }
  }
  label("n steps left", (x+17,y+2), black);
  for (int j = 1; j < 5; ++j)
  {
    draw((midxs1[1],midys1[1])--(midxs2[j],midys2[j]), red,
    EndArrow);
  }
\end{asy}
\end{figure}
%
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Wrap-up}
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$ \ Wrap-up}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> We have seen two examples of simple but non-trivial decision
  problems in climate research.
%  
\vfill
\item<2-> The problems have been described through informal narratives.
%  
\vfill
\item<3-> We have outlined some common features and patterns but \dots
%
\vfill
\item<4-> \dots we are far from \textcolor<4->{red}{understanding} the
  problems, let apart from being able to \textcolor<4->{red}{solve}
  them!
%
\vfill

\item<5-> We need \textcolor<5->{red}{more formal} problem
  \textcolor<5->{red}{descriptions} and \textcolor<5->{red}{general},
  \textcolor<5->{red}{accountable methods} for solving the problems.

%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Coming up}
\end{frame}

%% -------------------------------------------------------------------

\section{\ $\rightarrow$ \ Coming up}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
%
\begin{itemize}
%
\vfill
\item<1-> In the next lecture we look at \textcolor<1->{red}{mathematical specifications}.
%  
\vfill
\item<2-> We learn (what it means) to specify problems through simple examples.
%  
\vfill
\item<3-> We also get some elementary ideas about proof methods and a
  little bit of notation.
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------

%% TODO (perhaps)
%%
%%   * Add visualisation of transition function
%%
%%   * Explain why in presence of uncertainties the notion of optimal
%%     path of decision becomes meaningless


\end{document}


