% -*-Latex-*-
\documentclass{article}

\input{setup.tex}

%include lhs.fmt

\input{macros.TeX}


\begin{document}

%include Lecture7.lidr

\bibliographystyle{plain}
\bibliography{references}

\end{document}


