% -*-Latex-*-

%if False

> module Lecture10

> import Syntax.PreorderReasoning
> import Data.Vect
> import Control.Monad.Identity

> import Sigma.Sigma
> import Sigma.Operations
> import Fun.Predicates

> %default total
> %auto_implicits off
> %access public export

> infixr 7 :::
> infixr 7 <++>

> %hide map
> %hide pure
> %hide join
> %hide (>>=)
> %hide head
> %hide lteRefl

%endif

\setcounter{section}{9}

\stepcounter{section}
%\section{Extending the theory to monadic SDPs}

In this lecture we extend the naive theory of deterministic sequential
decision problems of lecture 7 to \emph{monadic} SDPs.  As a first step,
we account for a problem's uncertainties. As we have seen in lecture 6,
this can be done in terms of a type constructor |M| which has the structure
of a \emph{monad}: 

> M  :  Type -> Type


\subsection{States, controls, transition and reward function}

We formalize the notions of state space, control space, transition
function and reward function as usual

> X       :  (t : Nat) -> Type
> 
> Y       :  (t : Nat) -> (x : X t) -> Type
> 
> next    :  (t : Nat) -> (x : X t) -> (y : Y t x) -> M (X (S t))
>
> Val     :  Type
> 
> reward  :  (t : Nat) -> (x : X t) -> (y : Y t x) -> (x' : X (S t)) -> Val
> 
> (<+>)   :  Val -> Val -> Val
>
> zero    :  Val
>
> (<=)    :  Val -> Val -> Type


\subsection{Uncertainty measure}

In the deterministic case |M X = X| and the above functions completely
define a sequential decision problem. 

But when a decision step has an uncertain outcome, uncertainties about
"next" states naturally yield uncertainties about rewards. In these
cases, the decision maker faces a number of possible rewards (one for
each possible next state) and has to explain how to measure such
chances. In stochastic decision problems, possible next states (and,
therefore possible rewards) are labeled with probabilities. In these
cases, possible rewards are often measured in terms of their expected
value.  Here, again, we follow the approach proposed by Ionescu in
\cite{ionescu2009} and introduce a measure

> meas :  M Val -> Val


\subsection{Basic requirements}

The basic requirements for implementing a verified form of backwards
induction are, as in the deterministic case

> map       :  {A, B : Type} -> (A -> B) -> M A -> M B
>
> lteRefl   :  {a : Val} -> a <= a
> 
> lteTrans  :  {a, b, c : Val} -> a <= b -> b <= c -> a <= c
>
> plusMon   :  {a, b, c, d : Val} -> a <= b -> c <= d -> (a <+> c) <= (b <+> d)

Additionally, as shown in \cite{ionescu2009}, |meas| has to fulfill a
monotonicity condition:

> measMon  :  {A : Type} -> (f, g : A -> Val) -> ((a : A) -> (f a) <= (g a)) -> 
> 
>             (ma : M A) -> meas (map f ma) <= meas (map g ma)

Under exact arithmetic, the expected value measure does fulfill
|measMon|, as one would expect. 

It is useful to introduce a binary operator that extends |(<+>)| to
generic functions of codomain |Val|:

> (<++>) : {A : Type} -> (A -> Val) -> (A -> Val) -> A -> Val
> 
> f <++> g = \ a => f a <+> g a


\subsection{Policies and policy sequences}

With these premises, the naive theory from lecture 7 extends very
straightforwardly to the general, monadic case. The notions of policy
and policy sequence are exactly the same:

> Policy : (t : Nat) -> Type
> 
> Policy t = (x : X t) -> Y t x

> data PolicySeq : (t : Nat) -> (n : Nat) -> Type where
> 
>   Nil   :  {t : Nat} -> PolicySeq t Z
>   
>   (::)  :  {t, n : Nat} -> Policy t -> PolicySeq (S t) n -> PolicySeq t (S n)


\subsection{Value function}

The definition of the value function is a natural extension of the
deterministic definition from lecture 7. This was

< val  :  {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Val
<   
< val {t}    Nil       x  =  zero
<   
< val {t} (  p :: ps)  x  =  let y = p x in
< 
<                            let x' = next t x y in
<                            
<                            reward t x y x' <+> val ps x'

In the monadic case, |next t x y| yields an |M|-structure (a list, a
probability distribution, etc.) of values of type |X (S t)|. 

As anticipated above, applying |reward t x y <++> val ps| to these
values yields an |M|-structure of |Val| values. This uncertainty over
possible rewards is then measured with |meas|:

> val : {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Val
> 
> val {t}    Nil       x  =  zero
> 
> val {t} (  p :: ps)  x  =  let y = p x in
> 
>                            let mx' = next t x y in
>                       
>                            meas (map (reward t x y <++> val ps) mx')

\begin{shaded}
  \begin{exercise}
    \label{exercise10.1}    
    What are the types of |mx'|, |reward t x y <++> val ps| and \\ |map
  (reward t x y <++> val ps) mx'| in the definition of |val|?
  \end{exercise} 
\end{shaded}

We will come back to the definition of |val| later in this lecture.


\subsection{Optimality notions and Bellman's principle }

The notions of optimal policy sequence, optimal extension and Bellman's
principle of optimality are exactly the same as in the deterministic
case:

> OptPolicySeq  :  {t, n : Nat} -> PolicySeq t n -> Type
> 
> OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> (x : X t) -> val ps' x <= val ps x

> OptExt  :  {t, m : Nat} -> PolicySeq (S t) m -> Policy t -> Type
>
> OptExt {t} ps p  =  (p' : Policy t) -> (x : X t) -> val (p' :: ps) x <= val (p :: ps) x 

> Bellman  :  {t, m : Nat} -> 
> 
>             (ps  :  PolicySeq (S t) m)  ->   OptPolicySeq ps ->
>             
>             (p   :  Policy t)           ->   OptExt ps p ->
>             
>             OptPolicySeq (p :: ps)

The implementation of |Bellman| now crucially relies on the monotonicity
of |meas|:

> Bellman {t} ps ops p oep (p' :: ps') x = 
> 
>   let y'  =  p' x in
>   
>   let mx' =  next t x y' in
>   
>   let f'  =  reward t x y' <++> val ps' in
>   
>   let f   =  reward t x y' <++> val ps in
>   
>   let s0  =  \ x' => plusMon lteRefl (ops ps' x') in  --  ?
>   
>   let s1  =  measMon f' f s0 mx' in                   --  |val (p' :: ps')  x  <=  val (p' :: ps) x|
>   
>   let s2  =  oep p' x in                              --  |val (p' :: ps)   x  <=  val (p :: ps)  x|
>   
>   lteTrans s1 s2

\begin{shaded}
  \begin{exercise}
    \label{exercise10.2}
    What is the type of |s0| in the definition of |Bellman|?
  \end{exercise} 
\end{shaded}

\subsection{Verified backwards induction}

This fragment of the theory is exactly as in the deterministic case:

> nilOptPolicySeq : OptPolicySeq Nil
> 
> nilOptPolicySeq Nil x = lteRefl

> optExt  :  {t, n : Nat} -> PolicySeq (S t) n -> Policy t

> optExtSpec  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)

> bi : (t : Nat) -> (n : Nat) -> PolicySeq t n
> 
> bi t    Z     =  Nil
> 
> bi t (  S n)  =  let ps = bi (S t) n in optExt ps :: ps

> biLemma : (t : Nat) -> (n : Nat) -> OptPolicySeq (bi t n)
> 
> biLemma t    Z     =  nilOptPolicySeq
> 
> biLemma t (  S n)  =  let ps   =  bi (S t) n in
> 
>                       let ops  =  biLemma (S t) n in
>                     
>                       let p    =  optExt ps in
>                     
>                       let oep  =  optExtSpec ps in
>                     
>                       Bellman ps ops p oep


\subsection{Naive monadic theory, wrap up}

This completes the extension of the naive theory from lecture 7 to the
monadic case. Putting together 

\begin{itemize}

  \item Viability and reachability constraints,

  \item Generic verified optimal extension of arbitrary policy sequences and

  \item General, monadic SDPs, 

\end{itemize} 

is not completely trivial. The full theory (monadic, with viability and
reachability constraints and generic optimal extensions) is implemented
in "IdrisLibs/SequentialDecisionProblems/FullTheory.lidr"
\cite{botta20162020}.

In the remainder of this lecture, we will discuss an important question
related to the interpretation of the value function in the monadic case.

In lecture 11, we dissect an application of the full theory to a climate
emission problem.

\subsection{The |val|-|val'| equivalence in the monadic case}

We have argued that, for |ps : PolicySeq t n| and |x : X t|, |val ps x|
represents the |meas|-measure (for instance, the expected value) of the
sum of the rewards along the trajectories that are obtained under |ps|
when starting in |x|.

In lecture 7, we have shown that, for the deterministic case, this is
indeed the case, i.e.\

< valVal'Th  :  {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> val ps x = val' ps x 

holds. We now want to derive the same result for the general, monadic
case. As in the deterministic case, we start by defining sequences of state-control pairs

> data StateCtrlSeq : (t : Nat) -> (n : Nat) -> Type where
> 
>   Last   :  {t : Nat} -> (x : X t) -> StateCtrlSeq t (S Z)           
>   
>   (:::)  :  {t, n : Nat} -> 
>   
>             Sigma (X t) (Y t) -> StateCtrlSeq (S t) (S n) -> StateCtrlSeq t (S (S n))

and a function |sumR| that computes the sum of the rewards of a
state-control sequence:

> head : {t, n : Nat} -> StateCtrlSeq t (S n) -> X t
> 
> head (Last x)               =  x
> 
> head (MkSigma x y ::: xys)  =  x  

> sumR : {t, n : Nat} -> StateCtrlSeq t n -> Val
> 
> sumR {t} (Last x)               =  zero
> 
> sumR {t} (MkSigma x y ::: xys)  =  reward t x y (head xys) <+> sumR xys

Next, we implement a function which computes all the trajectories that
are obtained under a policy sequence |ps| when starting in |x|. For
this, |M| has to be equipped with monadic operations 

> pure     : {A : Type} -> A -> M A

> (>>=)    : {A, B : Type} -> M A -> (A -> M B) -> M B

> join     : {A : Type} -> M (M A) -> M A

As usual, we require the operations to fulfill the functor and monad
specification from lecture 6:

> mapPresId        :  ExtEq (map id) id
   
> mapPresComp      :  {A, B, C : Type} -> 
> 
>                     (f : A -> B) -> (g : B -> C) -> ExtEq (map (g . f)) (map g . map f)
                 
> mapPresExtEq     :  {A, B : Type} -> (f, g : A -> B) -> ExtEq f g -> ExtEq (map f) (map g)

> pureNatTrans     :  {A, B : Type} -> (f : A -> B) -> ExtEq (map f . pure) (pure . f)

> joinNatTrans     :  {A, B : Type} -> (f : A -> B) -> ExtEq (map f . join) (join . map (map f))

> triangleLeft     :  ExtEq (join . pure) id

> triangleRight    :  ExtEq (join . map pure) id         

> squareLemma      :  ExtEq (join . map join) (join . join)
              
> bindJoinMapSpec  :  {A, B : Type} -> (f : A -> M B) -> ExtEq (>>= f) (join . map f)

With |pure| and |>>=|, we can express the computation of the
trajectories as

> trj : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> M (StateCtrlSeq t (S n))
> 
> trj {t}    Nil       x  =  pure (Last x)                            
> 
> trj {t} (  p :: ps)  x  =  let y   = p x in
>                          
>                            let mx' = next t x y in
>                                                     
>                            map ((MkSigma x y) :::) (mx' >>= trj ps)

and compute the measure of the sum of the rewards obtained along the
trajectories that are obtained under the policy sequence |ps| when
starting in |x|

> val' : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> Val
> 
> val' ps x  =  meas (map sumR (trj ps x)) 

\begin{shaded}
  \begin{exercise}
    \label{exercise10.3}    
     What is the type of |map sumR (trj ps x)| in the definition of |val'
    ps x|? How does the size of |map sumR (trj ps x)| depend
    on the length of |ps|? |val' ps x| applies |meas| only once.
    How does the number of applications of |meas|
    depend on the length of |ps| in |val ps x|?
  \end{exercise} 
\end{shaded}

Now we can formulate the property that |val ps| does indeed compute the
measure of the sum of the rewards obtained along the trajectories
obtained under the policy sequence |ps|:

< valVal'Th : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> val ps x = val' ps x 

As it turns out, the measure function |meas| has to fulfill three
natural conditions for the |val|-|val'| theorem to hold. These are

> measPlusLemma   :  {A : Type} -> (f, g : A -> Val) -> (ma : M A) -> 
>                    
>                    meas (map (f <++> g) ma) = meas (map f ma) <+> meas (map g ma)

> measJoinLemma   :  (vss : M (M Val)) -> meas (join vss) = meas (map meas vss)

> measConstLemma  :  {A : Type} -> (v : Val) -> (ma : M A) -> 
> 
>                    meas (map (const v) ma) = v

\begin{shaded}
  \begin{exercise}
    \label{exercise10.4}
    Describe the meaning of |measPlusLemma|, |measJoinLemma| and
    |measConstLemma| in words. Rewrite the types of the measure lemmas
    using the property |ExtEq|.
  \end{exercise} 
\end{shaded}

In order to prove |valVal'Th|, we first put forward a few auxiliary
lemmas:

> mapConstLemma      :  {A, B, C : Type} -> (c : C) -> (ma : M A) -> (f : A -> B) ->
> 
>                       map (const c) ma = map (const c) (map f ma)

> measRetLemma       :  (v : Val) -> meas (pure v) = v

> mapJoinLemma       :  {A, B, C : Type} -> 
> 
>                       (f : B -> C) -> (g : A -> M B) -> (ma : M A) -> 
>                       
>                       map f (join (map g ma)) = join (map (map f . g) ma)

> mapHeadLemma       :  {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> 
> 
>                       map head (trj ps x) = map (const x) (trj ps x) 

> measMapHeadLemma   :  {t, n : Nat} -> (ps : PolicySeq t n) -> 
> 
>                       (f : X t -> Val) -> (x : X t) -> 
>                      
>                      (meas . (map (f . head) . (trj ps))) x = f x            

> measMapHeadLemma'  :  {t, n : Nat} -> 
> 
>                       (ps : PolicySeq t n) -> (mx : M (X t)) -> (f : X t -> Val) ->
>                       
>                       meas (map (f . head) (mx >>= trj ps)) = meas (map f mx)

> sumRLemma          :  {t, m : Nat} -> (x : X t) -> (y : Y t x) -> 
> 
>                       (xyss : M (StateCtrlSeq (S t) (S m))) -> 
>                       
>                       map sumR (map ((MkSigma x y) :::) xyss)
>                       
>                       = 
>                       
>                       map (((reward t x y) . head) <++> sumR) xyss            

We prove these lemmas in section \ref{auxiliary} below. With their help, we can
prove the equivalence of |val| and |val'| by induction on |ps|:



> valVal'Th  :  {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> val ps x = val' ps x 
             
> valVal'Th Nil x  =   ( val Nil x )
> 
>                  ={  Refl }=
>                  
>                      ( zero )
>                     
>                  ={  sym (measRetLemma zero) }=
>                  
>                      ( meas (pure zero) )
>                     
>                  ={  Refl }=
>                  
>                      ( meas (pure (sumR (Last x))) )
>                     
>                  ={  cong (sym (pureNatTrans sumR (Last x))) }=
>                  
>                      ( meas (map sumR (pure (Last x))) )
>                     
>                  ={  Refl }=
>                  
>                      ( meas (map sumR (trj Nil x)) )
>                     
>                  ={  Refl }=
>                  
>                      ( val' Nil x )
>                     
>                  QED



> valVal'Th {t} {n = S m} (p :: ps) x = 
> 
>   let y    =  p x in
>  
>   let mx'  =  next t x y in
>  
>   let r    =  reward t x y in
>  
>   let h    =  trj ps in
>  
>   let lhs  =  meas (map r mx') in  
>  
>   let lhs' =  meas (map (r . head) (mx' >>= h)) in
>  
>   let rhs  =  meas (map meas (map (map sumR) (map h mx'))) in
>  
>       ( val (p :: ps) x )
>    
>   ={  Refl }=
>  
>       ( meas (map (r <++> val ps) mx') )
>            
>   ={  measPlusLemma r (val ps) mx' }=
>  
>       ( meas (map r mx') <+> meas (map (val ps) mx') )
>    
>     -- |val ps x = val' ps x  =>  map (val ps) mx' = map (val' ps) mx'| 
>     
>   ={  cong  {f = \ α => lhs <+> meas α}   
>             (mapPresExtEq (val ps) (val' ps) (valVal'Th ps) mx') }= 
>             
>       ( lhs <+> meas (map (val' ps) mx') )
>     
>     --  val' ps x = ((meas . map sumR) . h) x  =>
>     --  map (val' ps) mx' = map ((meas . map sumR) . h) mx' 
>     
>   ={  cong  {f = \ α => lhs <+> meas α} 
>             (mapPresExtEq (val' ps) 
>                           ( (meas . map {A = StateCtrlSeq (S t) (S m)} sumR) . h) 
>                             (\ x => Refl) mx') }= 
>       ( lhs <+> meas (map ((meas . map sumR) . h) mx') )
>     
>   ={  cong {f = \ α => lhs <+> meas α} 
>            (mapPresComp h (meas . map {A = StateCtrlSeq (S t) (S m)} sumR) mx') }=
>            
>       ( lhs <+> meas (map (meas . (map sumR)) (map h mx')) )
>     
>   ={  cong {f = \ α => lhs <+> meas α} 
>            (mapPresComp (map {A = StateCtrlSeq (S t) (S m)} sumR) meas (map h mx')) }=
>            
>       ( lhs <+> meas (map meas (map (map sumR) (map h mx'))) )
>     
>   ={  Refl }=  
>   
>       ( meas (map r mx') <+> rhs )    
>     
>     -- |measMapHeadLemma'|: |meas (map (f . head) (xs >>= trj ps)) = meas (map f xs)|
>     
>   ={  cong {f = \ α => α <+> rhs} 
>            (sym (measMapHeadLemma' ps mx' r)) }=
>   
>       ( meas (map (r . head) (mx' >>= h)) <+> rhs )
>     
>   ={  Refl }=  
>   
>       ( meas (map (r . head) (mx' >>= h)) <+> meas (map meas (map (map sumR) (map h mx'))) )
>       
>     -- |measJoinLemma|: |meas (join vss) = meas (map meas vss)|
>     
>   ={  cong {f = \ α => lhs' <+> α} 
>            (sym (measJoinLemma (map (map sumR) (map h mx'))))  }= 
>            
>       ( lhs' <+> meas (join (map (map sumR) (map h mx'))) )
>     
>   ={  cong {f = \ α => lhs' <+> meas (join α)} 
>            (sym (mapPresComp h (map {A = StateCtrlSeq (S t) (S m)} sumR) mx')) }=
>            
>       ( lhs' <+> meas (join {A = Val} (map (map sumR . h) mx')) )  
>     
>   ={  cong {f = \ α => lhs' <+> meas α} 
>            (sym (mapJoinLemma sumR h mx')) }= 
>            
>       ( lhs' <+> meas (map sumR (join (map h mx'))) )
>     
>   ={  cong {f = \ α => lhs' <+> meas (map sumR α)} 
>            (sym (bindJoinMapSpec {B = StateCtrlSeq (S t) (S m)} h mx')) }=
>            
>       ( lhs' <+> meas (map sumR (mx' >>= h)) )  
>     
>   ={  Refl }=
>   
>       ( meas (map (r . head) (mx' >>= h)) <+> meas (map sumR (mx' >>= h)) )    
>     
>   ={  sym (measPlusLemma (r . head) sumR (mx' >>= h)) }=
>   
>       ( meas (map ((r . head) <++> sumR) (mx' >>= h)) )  
>     
>     -- |sumRLemma|: |map sumR (map ((MkSigma x y) :::) xyss) = map ((r . head) <++> sumR) xyss|
>     
>   ={  cong (sym (sumRLemma {m = m} x y (mx' >>= h))) }= 
>   
>       ( meas (map sumR (map {A = StateCtrlSeq (S t) (S m)} ((MkSigma x y) :::) 
>                   (mx' >>= trj ps))) )
>            
>   ={  Refl }=
>   
>       ( meas (map sumR (trj (p :: ps) x)) )
>     
>   ={  Refl }=
>   
>       ( val' (p :: ps) x )
>     
>   QED


\subsection{Auxiliary results}
\label{auxiliary}

> mapConstLemma c ma f  =   ( map (const c) ma )
> 
>                       ={  Refl }=
>                       
>                           ( map ((const c) . f) ma )
>                           
>                       ={ mapPresComp f (const c) ma }=
>                       
>                           ( map (const c) (map f ma) )
>                           
>                       QED

> measRetLemma v  =   ( meas (pure v) )
> 
>                 ={  cong (sym (pureNatTrans (const v) v)) }=
>                 
>                     ( meas (map (const v) (pure v)) )
>                     
>                 ={ measConstLemma v (pure v) }=
>                 
>                     ( v )
>                 QED

> mapJoinLemma f g ma  =   ( map f (join (map g ma)) )
> 
>                      ={  joinNatTrans f (map g ma) }=
>                      
>                          ( join (map (map f) (map g ma)) )
>                          
>                      ={  Refl }=
>                      
>                          ( join ((map (map f) . (map g)) ma) )
>                          
>                      ={ cong (sym (mapPresComp g (map f) ma)) }=
>                      
>                          ( join (map (map f . g) ma) )
>                          
>                      QED

> mapHeadLemma {t} {n = Z} Nil x 
> 
>   =   ( map head (trj Nil x) )
>   
>   ={  Refl }=
>   
>       ( map head (pure (Last x)) )
>       
>   ={  pureNatTrans head (Last x) }=  
>   
>       ( pure (head (Last x)) )
>       
>   ={  Refl }=
>   
>       ( pure x )
>       
>   ={  sym (pureNatTrans {A = StateCtrlSeq t (S Z)} (const x) (Last x)) }=
>   
>       ( map (const x) (pure (Last x)) )    
>       
>   ={  Refl }=
>   
>       ( map (const x) (trj Nil x) )
>       
>   QED
>   
> mapHeadLemma {t} {n = S m} (p :: ps) x 
> 
>   =   let y     =  p x in
>   
>       let xy    =  MkSigma x y in
>       
>       let mx'   =  next t x y in
>       
>       let xyss  =  (mx' >>= trj ps) in 
>       
>       ( map head (trj (p :: ps) x) )
>       
>   ={  Refl }=
>   
>       ( map head (map {B = StateCtrlSeq t (S (S m))} (xy :::) xyss) )
>       
>   ={  sym (mapPresComp {B = StateCtrlSeq t (S (S m))} (xy :::) head xyss) }=
>   
>       ( map (head . (xy :::)) xyss )
>       
>   ={  Refl }=
>   
>       ( map (const x) xyss )
>       
>   ={  mapConstLemma {B = StateCtrlSeq t (S (S m))} x xyss (xy :::) }=
>   
>       ( map (const x) (map {B = StateCtrlSeq t (S (S m))} (xy :::) xyss) )
>       
>   ={  Refl }=
>   
>       ( map (const x) (trj (p :: ps) x) )
>       
>   QED                                

> measMapHeadLemma {t} {n} ps f x 
> 
>   =   ( (meas . (map (f . head) . (trj ps))) x )
>   
>   ={  Refl }=
>   
>       ( meas (map (f . head) (trj ps x)) )
>       
>   ={  cong (mapPresComp head f (trj ps x)) }=
>   
>       ( meas (map f (map head (trj ps x))) )
>       
>   ={  cong {f = \ α => meas (map f α)} (mapHeadLemma ps x) }=
>   
>       ( meas (map f (map (const x) (trj ps x))) )
>       
>   ={  cong (sym (mapPresComp {A = StateCtrlSeq t (S n)} (const x) f (trj ps x))) }=
>   
>       ( meas (map (f . (const x)) (trj ps x)) )
>       
>   ={  Refl }=
>   
>       ( meas (map (const (f x)) (trj ps x)) )
>       
>   ={  measConstLemma (f x) (trj ps x) }=
>   
>       ( f x )
>       
>   QED

> measMapHeadLemma' {t} {n} ps mx f
> 
>   =   let g  = trj ps in
>   
>       ( meas (map (f . head) (mx >>= g)) )
>       
>   ={  cong {f = \ α => meas (map (f . head) α)} (bindJoinMapSpec {B = StateCtrlSeq t (S n)} g mx) }= 
>   
>       ( meas (map (f . head) (join (map g mx))) )
>       
>   ={  cong (mapJoinLemma (f . head) g mx) }= 
>   
>       ( meas (join (map (map (f . head) . g) mx)) )
>       
>   ={  measJoinLemma (map (map (f . head) . g) mx) }= 
>   
>       ( meas (map meas (map (map (f . head) . g) mx)) )  
>       
>   ={  cong (sym (mapPresComp (map (f . head) . g) meas mx) ) }= 
>   
>       ( meas (map (meas . (map (f . head) . g)) mx) )  
>       
>   ={  cong (mapPresExtEq (meas . (map (f . head) . (trj ps))) f (measMapHeadLemma ps f) mx) }= 
>   
>       ( meas (map f mx) )
>       
>   QED

> sumRLemma {t} {m} x y xyss 
> 
>   =   ( map sumR (map {A = StateCtrlSeq (S t) (S m)} ((MkSigma x y) :::) xyss) )
>   
>   ={  sym (mapPresComp {A = StateCtrlSeq (S t) (S m)} ((MkSigma x y) :::) sumR xyss) }=
>   
>       ( map {A = StateCtrlSeq (S t) (S m)} (sumR . ((MkSigma x y) :::)) xyss )
>       
>   ={  Refl }=
>   
>       ( map (((reward t x y) . head) <++> sumR) xyss )
>       
>   QED


% \section*{Solutions}


% \subsubsection*{Exercise \ref{exercise10.1}:}


% \subsubsection*{Exercise \ref{exercise10.2}:}


% \subsubsection*{Exercise \ref{exercise10.3}:}


% \subsubsection*{Exercise \ref{exercise10.4}:}
