% -*-Latex-*-

%if False

> module Lecture4

> import Data.Fin
> import Data.Vect
> import Data.So
> import Syntax.PreorderReasoning

> import Rel.TotalPreorder
> import Rel.TotalPreorderOperations

> import FastSimpleProb.SimpleProb
> import FastSimpleProb.MonadicOperations
> import FastSimpleProb.Functor

> %default total
> %access public export
> %auto_implicits off

> %hide lteRefl

> Real : Type
> Real = Double

> infixr 7 ##
> infixr 7 <++>
 
%endif

\begin{frame}
  \frametitle{Objectives}
\begin{itemize}

\vfill
\item<1-> Build a \hl<1>{theory} for \hl<2>{specifying} and
  \hl<2>{solving} \hl<3>{decision} \hl<3>{problems} under
  \hl<3>{stochastic} uncertainty like those from lecture 1.

\vfill
\item<4-> Extend the theory to problems with \hl<4>{monadic} uncertainty. 
\end{itemize}
\vfill
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: how we build the theory}
%
\begin{itemize}

\vfill
\item<1-> We build the theory in terms of \hl<1>{two} kinds of
  software components:

\vfill
\item<2-> Components for the \hl<2>{specification} of a SDP and
  components for its \hl<2>{solution} that is, the computation of
  \hl<3>{verified} \hl<3>{optimal} \hl<3>{policies} for that SDP.
  
\vfill
\item<4-> Specification components are implemented as \hl<4>{global},
  partial definitions or \hl<4>{holes}.

\vfill
\item<5-> We \hl<5>{apply} the theory by \hl<5>{filling-in} these holes
  for a concrete SDP.
  
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: specification components}
%
\begin{itemize}

\vfill
\item<2-> We specify decision problems in which a decision maker has to
  take a \hl<2>{sequence} \hl<2>{of} \hl<2>{decisions}, one after the
  other.

\vfill
\item<3-> At each decision \hl<3>{step}, the decision maker has to
  select a \hl<3>{control} on the basis of an observable \hl<4>{current}
  \hl<4>{state}.

\vfill
\item<5-> Denote the \hl<5>{set} \hl<5>{of} observable \hl<5>{states} at step \hl<5>{|t :
  Nat|} with \hl<5>{|X t|}:
  
> X : (t : Nat) -> Type
  
\vfill
\item<6-> For \hl<6>{example}, in the GHG emission problem of lecture 1 

%if False

> data CE = GT Nat
> data EL = L | H
> data AU = A | U
> data GB = G | B        
  
%endif

> X t = (CE, EL, AU, GB)   

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: specification components}
%
\begin{itemize}

\vfill
\item<1-> perhaps with

< data CE  =  GT Nat -- Cumulated Emissions
< 
< data EL  =  L | H  -- Emission Level
< 
< data AU  =  A | U  -- Available or Unavailable (technologies)
< 
< data GB  =  G | B  -- Good or Bad (state of climate, economy, etc.)       

\vfill 
\item<2-> Denote the \hl<2>{set} \hl<2>{of} \hl<2>{controls} available
  to the decision maker at step \hl<2>{|t|} and in state \hl<2>{|x : X t|} with
  \hl<2>{|Y t x|}:

> Y : (t : Nat) -> X t -> Type 

\vfill
\item<3-> For \hl<3>{example}, the emission level targeted for the next decade

> Y t x = EL
  
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: specification components}
%
\begin{itemize}

\vfill
\item<1-> We have to specify what are the \hl<1>{possible} \hl<1>{next}
  \hl<1>{states} when we select a control in \hl<1>{|Y t x|} at step
  \hl<1>{|t|} and in state \hl<1>{|x|}:

> next : (t : Nat) -> (x : X t) -> Y t x -> SimpleProb (X (S t))

\vfill 
\item<2-> This is where \hl<2>{climate} + \hl<2>{social} +
  \hl<2>{economic} sciences come in: what is the \hl<3>{probability} of
  entering a |B|-state (perhaps because ``committed'' to unmanageable
  sea-level rise levels in a few centuries) given that we select |H| in
  |(GT 3300, H, U, G)|?

\vfill
\item<4-> Implementation of \hl<4>{|next|} for the first example of
  lecture 1 in \href{https://esd.copernicus.org/articles/9/525/2018/}{\hl<4>{ESD-2018}}.

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: specification components}
%
\begin{itemize}

\vfill
\item<1-> We have to specify what are the (human, material, moral, etc.)
  \hl<1>{benefits} and the \hl<1>{costs} associated with
  transitions. This requires specifying

\vfill
\item<2-> 1) How we \hl<2>{measure} these benefits and costs \dots

> Val : Type

\vfill
\item<3-> For \hl<3>{example}

< Val = Real

or perhaps

< Val = (SustMeas, GDP, ...)

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: specification components}
%
\begin{itemize}

\vfill
\item<1-> \dots and 2) what are the benefits and the costs of
  \hl<1>{possible} \hl<1>{transitions}:

> reward : (t : Nat) -> (x : X t) -> Y t x -> X (S t) -> Val

\vfill 
\item<2-> This is where \hl<2>{climate} + \hl<2>{social} +
  \hl<2>{economic} sciences but also \hl<3>{political} and
  \hl<3>{ethical} value judgments come in: what are the benefits of a
  decade of booming economy if the next state is ``committed'' to
  unmanageable (by today's standards) sea-level rise levels in a few
  centuries?

\vfill
\item<4-> Notice that |reward t| depends on the current state, on the
  current decision \hl<4>{and} on the \hl<4>{next} state!
  
\vfill
\item<5-> Implementation of \hl<5>{|reward|} for the first example of
  lecture 1 in \href{https://esd.copernicus.org/articles/9/525/2018/}{\hl<5>{ESD-2018}}.

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: specification components}
%
\begin{itemize}

\vfill
\item<1-> Remember that \hl<1>{optimal} policies \hl<1>{maximise} a
  \hl<1>{sum} of the rewards over all \hl<1>{possible} decision steps.

\vfill 
\item<2-> Thus, we have to specify how to \hl<2>{add} rewards along
  possible state-control trajectories:
  
> (<+>) : Val -> Val -> Val
  
\vfill
\item<3-> Also, we have to be able to \hl<3>{compare} pairs of rewards

> (<=)             :  Val -> Val -> Type
  
\vfill
\item<4-> If |Val| is a \hl<4>{tuple} representing the values of (possibly
  conflicting) |n + 1| criteria, comparing rewards might require
  introducing |n| \hl<4>{prices}.

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill
\item<1-> We will need a few more \hl<1>{specification} components, but
  let's first look at components for solving SDPs!

%if False

> zero  :  Val

> meas  :  SimpleProb Val -> Val

%endif
  
\vfill 
\item<2-> First, we have to define a data type for
  \hl<2>{policies}. These are \hl<2>{functions} from \hl<2>{states} to
  \hl<2>{controls}:

> Policy : (t : Nat) -> Type
> 
> Policy t = (x : X t) -> Y t x
  
\vfill
\item<3-> Remember that we want to compute \hl<3>{optimal}
  \hl<3>{policy} \hl<3>{sequences}. Policy sequences are \hl<3>{vectors} of policies:

> data PolicySeq : (t : Nat) -> (n : Nat) -> Type where
> 
>   Nil   :  {t : Nat} -> PolicySeq t Z
>   
>   (::)  :  {t, n : Nat} -> Policy t ->
>   
>            PolicySeq (S t) n -> PolicySeq t (S n)

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill
\item<1-> Next, we have to formulate \hl<1>{optimality}
  for \hl<1>{policy} \hl<1>{sequences}.
  
\vfill 
\item<2-> Remember lecture 2, exercise 2.4: A policy |p : X -> Y| is
  optimal w.r.t. |val : X -> Y -> Real| iff |∀ !  x : X|, |∀ ! y ∈ Y|,
  \ |val x y <= val x (p x)|.
  
\vfill
\item<3-> In SDPs we have sequences of policies but \dots what is their
  \hl<3>{value}?

\vfill
\item<4-> We have to define it in terms of the \hl<4>{sum} of the
  rewards over all \hl<4>{possible} decision steps.

\vfill
\item<5-> This requires computing all \hl<5>{possible} state-control
  \hl<5>{trajectories} starting from an initial state.
  
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill
\item<1-> First, define the datatype of \hl<1>{state}-\hl<1>{control} \hl<1>{sequences}:

> data StateCtrlSeq : (t, n : Nat) -> Type where
> 
>   Last  :  {t : Nat} -> (x : X t) -> StateCtrlSeq t (S Z)
>   
>   (##)  :  {t, n : Nat} -> DPair (X t) (Y t) -> 
>   
>            StateCtrlSeq (S t) (S n) -> StateCtrlSeq t (S (S n))  
  
  
\vfill 
\item<2-> Second, compute the \hl<2>{sum} of the \hl<2>{rewards} for a \hl<2>{sequence}:
  
%if False

> head : {t, n : Nat} -> StateCtrlSeq t (S n) -> X t
> 
> head (Last x)              =  x
> 
> head (MkDPair x y ## xys)  =  x

%endif

> sumR : {t, n : Nat} -> StateCtrlSeq t n -> Val
> 
> sumR {t} (Last x)              =  zero
> 
> sumR {t} (MkDPair x y ## xys)  =  reward t x y (head xys)
>                                   <+>
>                                   sumR xys


\vfill
\item<3-> Here |head| returns the \hl<3>{first} \hl<3>{state} of a
  state-control sequence and |zero| is one more specification
  component:

< zero  :  Val

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill
\item<1-> Third, define a function that computes \hl<1>{all} the \hl<1>{possible}
  \hl<1>{trajectories} under a policy sequence \hl<1>{|ps|} and starting in \hl<1>{|x|} \dots

> trj  :  {t, n : Nat} ->  (ps : PolicySeq t n) -> (x : X t) -> 
> 
>                          SimpleProb (StateCtrlSeq t (S n))
> 
> trj {t}     Nil       x  =  ret (Last x)
> 
> trj {t}  (  p :: ps)  x  =  let y = p x in
> 
>                             let mx' = next t x y in
>                             
>                             map ((MkDPair x y) ##) (bind mx' (trj ps))

\vfill 
\item<2-> \dots and the value of the possible \hl<2>{sums} of rewards along the
  trajectories:

< val : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> Val
< val ps = meas . map sumR . trj ps

\vfill 
\item<3-> Notice \hl<3>{|meas : SimpleProb Val -> Val|}! 

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill
\item<1-> With these components, we can finally formulate
  \hl<1>{optimality} for \hl<1>{policy} \hl<1>{sequences} in a \hl<2>{precise}
  and \hl<2>{generic} way:

< OptPolicySeq : {t, n : Nat} -> PolicySeq t n -> Type
<  
< OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> (x : X t) -> 
<
<                             val ps' x <= val ps x

\vfill 
\item<3-> For this definition of |val| we know how to compute optimal
  policy sequences only for \hl<3>{finite} \hl<3>{states} and by
  \hl<3>{brute} \hl<3>{force}!

\vfill 
\item<4-> But the \hl<4>{computational} \hl<4>{cost} of brute force is
  often unacceptable: there are (constant, finite states and controls)
  \hl<4>{${\lvert Y \rvert}^{{\lvert X \rvert} * n}$} policy sequences!

\vfill 
\item<5-> We need a \hl<5>{more} \hl<5>{effective} approach!
  
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill
\item<1-> Bellman's value function:

%if False

> (<++>) : {A : Type} -> (f, g : A -> Val) -> A -> Val
> f <++> g = \ a => f a <+> g a

%endif

> val : {t, n : Nat} -> PolicySeq t n -> X t -> Val
> 
> val {t}  Nil      x  =  zero
> 
> val {t} (p :: ps) x  =  let y    =  p x in
> 
>                         let mx'  =  next t x y in          
>                         
>                         meas (map (reward t x y <++> val ps) mx')

%if False

> OptPolicySeq : {t, n : Nat} -> PolicySeq t n -> Type
>  
> OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> (x : X t) -> 
>
>                             val ps' x <= val ps x

%endif

\vfill 
\item<2-> where |(<++>)| is the canonical function addition

< (<++>) : {A : Type} -> (f, g : A -> Val) -> A -> Val
< 
< f <++> g = \ a => f a <+> g a

\vfill 
\item<3-> It turns out that if |Val = Real|, |(<+>) = (+)| and |meas|
  the expected value, the \hl<3>{two} value functions are
  \hl<3>{equivalent}!

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill 
\item<1-> With Bellman's value function, we can compute \hl<1>{verified}
  \hl<1>{optimal policy} sequences by \hl<1>{backward}
  \hl<1>{induction}!
  
\vfill 
\item<2-> To do this, we need \hl<2>{best} \hl<2>{extensions} of policy
  sequences:

> BestExt  :  {t, n : Nat} -> PolicySeq (S t) n -> Policy t -> Type
> 
> BestExt {t} ps p  =  (p' : Policy t) -> (x : X t) -> 
> 
>                      val (p' :: ps) x <= val (p :: ps) x
  
\vfill 
\item<3-> There is no general recipe but \dots
  \hl<3>{remember} \hl<3>{lecture} \hl<3>{2}!

\vfill 
\item<4-> With one such functions \dots

> bestExt      :  {t, n : Nat} -> PolicySeq (S t) n -> Policy t

> bestExtSpec  :  {t, n : Nat} -> (ps : PolicySeq (S t) n) ->
>
>                 BestExt ps (bestExt ps)  
  
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill 
\item<1-> \dots generic backward induction \dots

> bi  :   (t : Nat) -> (n : Nat) -> PolicySeq t n
> 
> bi t  Z     =  Nil
> 
> bi t (S n)  =  let ps = bi (S t) n in bestExt ps :: ps

\vfill 
\item<2-> \dots fulfils the \hl<2>{specification}:

> biLemma : (t : Nat) -> (n : Nat) -> OptPolicySeq (bi t n)

\vfill 
\item<3-> This is \hl<3>{verified} in \hl<3>{two} steps.

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill 
\item<1-> Step 1: implement \hl<1>{Bellman's} \hl<1>{principle} of optimality:

> Bellman  :  {t, n : Nat} ->
> 
>             (ps   :  PolicySeq (S t) n)  -> OptPolicySeq ps ->
>             
>             (p    :  Policy t)           -> BestExt ps p ->
>             
>             OptPolicySeq (p :: ps)
  
  
\vfill 
\item<2-> Step 2: implement \hl<2>{|biLemma|}:

%if False

> nilOptPolicySeq  :  OptPolicySeq Nil

%endif

> biLemma t  Z     =  nilOptPolicySeq
> 
> biLemma t (S n)  =  let  ps   =  bi (S t) n in
> 
>                     let  ops  =  biLemma (S t) n in
>                     
>                     let  p    =  bestExt ps in
>                     
>                     let  oep  =  bestExtSpec ps in
>                     
>                     Bellman ps ops p oep

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill 
\item<1-> Proving |Bellman| and |nilOptPolicySeq : OptPolicySeq Nil|
  requires filling-in 4 additional specification components:

> lteRefl  : {a : Val} -> a <= a

> lteTrans : {a, b, c : Val} -> a <= b -> b <= c -> a <= c

> plusMon   :  {v1, v2, v3, v4 : Val} -> 
>              v1 <= v2 -> v3 <= v4 -> (v1 <+> v3) <= (v2 <+> v4)

> measMon   :  {A : Type} -> 
>              (f, g : A -> Val) -> ((a : A) -> f a <= g a) ->
>              (ma : SimpleProb A) ->
>              meas (map f ma) <= meas (map g ma)  

\vfill 
\item<2-> Try to understand the proofs in the .lidr file!

%if False

> nilOptPolicySeq Nil x = lteRefl
  
> Bellman {t} ps ops p oep (p' :: ps') x  =
>
>  let y'   =  p' x                                      in
>  let mx'  =  next t x y'                               in
>  let f'   =  reward t x y' <++> val ps'                in
>  let f    =  reward t x y' <++> val ps                 in
>  let s0   =  \ x' => plusMon lteRefl (ops ps' x')      in
>  let s1   =  measMon f' f s0 mx'                       in
>  let s2   =  oep p' x                                  in
>
>  lteTrans s1 s2

What are the types of |y'|, |mx'|, ... |s2|? Solution:

> {- 

> Bellman {t} ps ops p oep (p' :: ps') x  = lteTrans s1 s2 where
>   y'   :  Y t x
>   y'   =  p' x
>   mx'  :  SimpleProb (X (S t))
>   mx'  =  next t x y'
>   f'   :  X (S t) -> Val
>   f'   =  reward t x y' <++> val ps'
>   f    :  X (S t) -> Val
>   f    =  reward t x y' <++> val ps
>   s0   :  (x' : X (S t)) -> f' x' <= f x'
>   s0   =  \ x' => plusMon lteRefl (ops ps' x')
>   s1   :  val (p' :: ps') x <= val (p' :: ps) x
>   s1   =  measMon f' f s0 mx'
>   s2   :  val (p' :: ps)  x <= val (p :: ps)  x
>   s2   =  oep p' x

> ---}  

%endif

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill 
\item<1-> To complete the specification of the first example of lecture
  1 we still need to fill-in a few \hl<1>{holes}: try to type check the .lidr file!

\vfill 
\item<2-> \hl<2>{|next|}, \hl<2>{|Val|}, \hl<2>{|reward|} and \hl<2>{|meas|} are problem specific. 

\vfill 
\item<3-> If we take \hl<3>{|Val = Real|}, we can apply canonical definitions of
  |<+>|, |(<=)|, |zero| and get |lteRefl|, |lteTrans| and |plusMon| from
  IdrisLibs.

\vfill 
\item<4-> If we take \hl<4>{|meas = expectedValue|}, we can get |measMon| for
  free from IdrisLibs.

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Stochastic SDPs: solution components}
%
\begin{itemize}

\vfill 
\item<1-> If \hl<1>{|Y t x|} is \hl<1>{finite} and \hl<1>{non-empty}, we get \hl<1>{|bestExt|}
  and \hl<1>{|bestExtSpec|} for free from IdrisLibs.

\vfill 
\item<2-> IdrisLibs provides some \hl<2>{help} for \hl<2>{defining}
  \hl<2>{|reward|} from high level goals: \hl<3>{stay} \hl<3>{safe},
  \hl<4>{avoid} \hl<4>{bad}, etc.
  
\vfill 
\item<5-> We are far from a DSL for \hl<5>{verified},
  \hl<5>{climate} \hl<5>{decision} \hl<5>{making}!

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Monadic SDPs}
%
\begin{itemize}

\vfill 
\item<1-> All stays (almost) the same if we \hl<1>{replace}
  \hl<1>{|SimpleProb|} with an uncertainty \hl<1>{monad}.

\vfill 
\item<2-> |meas|, |<+>| and the monadic operations have to fulfill certain
  \hl<2>{compatibility} \hl<2>{conditions} for Bellman's value function to be equivalent
  to the function that returns the value of the sums of rewards
  along the trajectories:
  \href{https://www.cambridge.org/core/journals/journal-of-functional-programming/article/on-the-correctness-of-monadic-backward-induction/0F71FFE6AE1222E77F52341C695E366A}{\hl<2>{JFP-2021}}

\vfill 
\item<3-> We have to make sure that \hl<3>{|next t x y|} is \hl<3>{not} \hl<3>{empty} or
  implement a \hl<4>{more} \hl<4>{clever} \hl<4>{theory}:
  \href{https://www.cambridge.org/core/journals/journal-of-functional-programming/article/contributions-to-a-computational-theory-of-policy-advice-and-avoidability/CDB4C9601702AAB336A2FB2C34B8F49B}{\hl<4>{JFP-2017}}

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}
  \frametitle{The end}
\begin{itemize}

\vfill
\item<1-> \hl<1>{Thanks} for your attention!

\vfill
\item<2-> Questions, comments, etc. to \hl<2>{botta@@pik-potsdam.de}.

\end{itemize}
\vfill
\end{frame}

%% -------------------------------------------------------------------






