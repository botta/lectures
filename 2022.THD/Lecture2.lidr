% -*-Latex-*-

%if False

> module Lecture2

> %default total
> %auto_implicits on
> %access public export

> Real : Type
> Real = Double
 
%endif

\begin{frame}
  \frametitle{Objectives}
\begin{itemize}
\vfill
\item<1-> Learn/exercise \hl<1>{mathematical} \hl<1>{specifications}.
\vfill
\item<2-> Learn/recall proof methods: \hl<2>{induction}, \hl<2>{equational} \hl<2>{reasoning}. 
\end{itemize}
\vfill
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Equations, specifications and dependent types}
%
\begin{itemize}

\vfill
\item<2-> \hl<2>{$x^2 = 1$}, \hl<2>{$x = 1$} and \hl<2>{$x = -1$} are all \hl<2>{equations} but \dots

\vfill
\item<3-> \dots \hl<3>{$x^2 = 1$} specifies a \hl<3>{problem}. The
  problem is perhaps not very clear but \dots

\vfill
\item<4-> \dots we can make it precise with a \hl<4>{mathematical} \hl<4>{specification}:
  
\begin{equation*}
  \text{\textbf{Find} |x ∈ ℝ| \textbf{s.t.} |x * x = 1|}
\end{equation*}  

\vfill
\item<5-> In \hl<5>{Idris}, we can apply \hl<5>{types} to do the same \dots

< x      :  Real      
< xSpec  :  x * x = 1 
        
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Equations, specifications and dependent types}
%
\begin{itemize}

\vfill
\item<1-> \hl<0>{$x^2 = 1$}, \hl<0>{$x = 1$} and \hl<0>{$x = -1$} are all \hl<0>{equations} but \dots

\vfill
\item<1-> \dots \hl<0>{$x^2 = 1$} specifies a \hl<0>{problem}. The
  problem is perhaps not very clear but \dots

\vfill
\item<1-> \dots we can make it precise with a \hl<0>{mathematical} \hl<0>{specification}:
  
\begin{equation*}
  \text{\textbf{Find} |x ∈ ℝ| \textbf{s.t.} |x * x = 1|}
\end{equation*}  

\vfill
\item<1-> In \hl<0>{Idris}, we can apply \hl<0>{types} to do the same \dots

< x      :  Real       -- \hl<1>{|x :: Real|} \hl<1>{in} \hl<1>{Haskell}
< xSpec  :  x * x = 1  -- \hl<2>{not} \hl<2>{available} \hl<2>{in} \hl<2>{Haskell!}
        
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Equations, specifications and dependent types}
%
\begin{itemize}

\vfill
\item<1-> \hl<0>{$x^2 = 1$}, \hl<0>{$x = 1$} and \hl<0>{$x = -1$} are all \hl<0>{equations} but \dots

\vfill
\item<1-> \dots \hl<0>{$x^2 = 1$} specifies a \hl<0>{problem}. The
  problem is perhaps not very clear but \dots

\vfill
\item<1-> \dots we can make it precise with a \hl<0>{mathematical} \hl<0>{specification}:
  
\begin{equation*}
  \text{\textbf{Find} |x ∈ ℝ| \textbf{s.t.} |x * x = 1|}
\end{equation*}  

\vfill
\item<1-> In Idris, we can apply \hl<0>{types} to do the same \dots

%format Lecture2.x = "x"

> x      :  Real                          -- \hl<1>{|Real|} is a \hl<1>{type}      
> xSpec  :  Lecture2.x * Lecture2.x = 1   -- \hl<2>{|x * x = 1|} is \hl<2>{also} \hl<2>{a} \hl<2>{type}! 

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Equations, specifications and dependent types}
%
\begin{itemize}
\vfill
\item<1->  \dots define a \hl<1>{solution} \dots

> x      =  1.0   -- a value of type |Real|

\vfill
\item<2-> \dots and \hl<2>{prove} that this solution is \hl<2>{correct}:

> xSpec  =  Refl  -- a value of type |x * x = 1| (a proof)
        
\vfill
\item<3-> This is the essence of \hl<3>{verified} \hl<3>{programming}.

\vfill
\item<4-> Verified programming boils down to \hl<4>{three} steps: \hl<5>{1)}
  \hl<5>{specify} \hl<5>{the} \hl<5>{program} (problem); \hl<6>{2)}
  \hl<6>{implement} (solve) \hl<6>{it} and \hl<7>{3)} \hl<7>{verify}
  \hl<7>{that} \hl<7>{the} \hl<7>{implementation} (solution) \hl<7>{is}
  \hl<7>{correct}.
  

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Equations, specifications and dependent types}
%
\begin{itemize}
\vfill
\item<1->  1) \hl<2>{specify}:

< x      :  Real
< xSpec  :  Lecture2.x * Lecture2.x = 1  

\vfill
\item<1-> 2) \hl<3>{implement}:

< x = 1.0
        
\vfill
\item<1-> 3) \hl<4>{verify}:

< xSpec = Refl  

\vfill
\item<5-> \hl<5>{Verifying} a \hl<5>{program} requires \hl<5>{writing} another \hl<5>{program}!

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Back to mathematical specifications}
%
\begin{itemize}
\vfill
\item<1-> We can try to specify the problem of \hl<1>{finding} the
  \hl<1>{square} \hl<1>{root} of an arbitrary number:

\begin{equation*}
  \text{\textbf{Given} |y ∈ ℝ|, \textbf{find} |x ∈ ℝ| \textbf{s.t.} |x * x = y|}
\end{equation*}  

\vfill
\item<2-> This problem is not solvable (for instance, for |y = -1|), the
  specification is \hl<2>{unfeasible}. 

\vfill
\item<3-> We can try to write a \hl<3>{feasible} specification

\begin{equation*}
  \text{\textbf{Given} |y ∈ ℝ|, |0 ≤ y|, \textbf{find} |x ∈ ℝ| \textbf{s.t.} |x * x = y|}
\end{equation*}  

\vfill
\item<4-> but now we run into \hl<4>{two} \hl<4>{problems}: can you see them?

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Mathematical specifications: functions}
%
\begin{itemize}
\vfill
\item<1-> First: even for |0 ≤ y|, we cannot in general fulfill
  |x * x = y| exactly but only within some \hl<1>{tolerance}.

\vfill
\item<2-> Second: how can we ensure that our computations are
  \hl<2>{correct}? We do not have a specific |y|!

\vfill
\item<3-> One more problem: which root shall be computed? 

\vfill
\item<4-> We can be more precise by putting forward the problem of
  finding a \hl<4>{function}:

\begin{equation*}
  \text{\textbf{Find} |f : RPosz -> RPosz| \ \textbf{s.t.} |∀ y ∈ RPosz|, \ |f y * f y = y|}
\end{equation*}  

\vfill
\item<5-> \sh{Exercise 2.1:} Specify the problem of finding a function
  that computes both square roots.

%if False

\begin{equation*}
  \text{\textbf{Find} |f : ℝ -> PS ℝ| \ \textbf{s.t.} |∀ y ∈ ℝ|,
    \ |∀ x ∈ ℝ, \ x * x = y ⇔ x ∈ f y|}
\end{equation*}  
  
%endif  
  
\end{itemize}

\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Domain-specific notions}
%
\begin{itemize}
\vfill
\item<1-> Mathematical specifications can be applied to \hl<1>{clarify}
  domain-specific \hl<1>{notions}:

  \begin{itemize}
    \vfill
  \item<2-> What does it mean for a \hl<2>{function} to be \hl<2>{injective}?
    \vfill
  \item<3-> What does it mean for a \hl<3>{strategy profile} to be in \hl<3>{equilibrium}?
    \vfill
  \item<4-> What does it mean for a \hl<4>{policy} to be \hl<4>{optimal}?
    
  \end{itemize}
  
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: Nash equilibrium}
%
\begin{itemize}
\vfill
\item<1-> Reducing GHG emissions has different \hl<1>{costs} and
  \hl<1>{benefits} for different countries.

\vfill
\item<2-> The \hl<2>{highest} \hl<2>{global} \hl<2>{benefits} are
  obtained if most countries reduce emissions by certain (NDCs:
  Nationally Determined Contributions) country-specific amounts.

\vfill \item<3-> In this situation many countries have a
\hl<3>{free-ride} opportunity: they would be \hl<4>{better} \hl<4>{off}
if they would \hl<4>{not} \hl<4>{reduce} their own emissions (under the
assumption that the others comply with their obligations!)
  
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: Nash equilibrium (2 countries)}
%
\vfill
\begin{table}[h]
\centering
\begin{tabular}{||c||c||c||}
  \hline
      &   $D$   &   $C$     \\
  \hline
  $D$ & (1,1) & (3,0)       \\  
  \hline
  $C$ & (0,3) & (2,2)       \\
  \hline
\end{tabular}
\end{table}

\begin{itemize}
\vfill
\item<1-> Which property makes |(D,D)| \hl<1>{stable} and yet \hl<1>{undesirable}? 

\vfill
\item<2-> Let |S = {D,C}| and |p1, p2 : cross S S -> ℝ| payoff
  functions. A \hl<2->{strategy} \hl<2->{profile} |(x,y) ∈ cross S S| is
  a \hl<2->{Nash} \hl<2->{equilibrium} iff |∀ x', y' ∈ S|, |p1 (x', y) ≤
  p1 (x, y)| and |p2 (x, y') ≤ p2 (x, y)|.
  
\vfill
\item<3-> \sh{Exercise 2.2:} Modify the payoffs of |(C,C)| in the table
  for |(C,C)| to become a Nash equilibrium.

\vfill
\item<4-> \sh{Exercise 2.3:} Generalize the notion of Nash equilibrium
  to an arbitrary number of players.

%if False  
  
Let $n \in \mathbb{N}$ be the number of players and $S_i$, $i \in \{1,
\dots, n\}$ the strategy set of the $i$-th player. Let $p_i : S_1
\times S_2 \times \dots \times S_n \to \mathbb{R}$ the payoff function
of the $i$-th player. A \textbf{strategy profile} $(x_1, ..., x_n) \in
S_1 \times S_2 \times \dots \times S_n$ is a \textbf{Nash equilibrium}
iff $\forall i \in \{1, \dots, n\}$ and $\forall x_i^{\prime} \in S_i$,
$p_i (x_1, ..., x_{i-1}, x_i^{\prime}, x_{i+1}, ... x_n) \leq
p_i (x_1, ..., x_{i-1}, x_i, x_{i+1}, ... x_n)$.

%endif
  
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: optimal policies}
%
\begin{itemize}
\vfill
\item<1-> Let |X| denote a \hl<1>{set} \hl<1>{of} \hl<1>{states}.
  
\vfill
\item<2-> Let |Y| denote the \hl<2>{controls} (actions, options)
  available to a decision maker: for simplicity, the same in all states
  |x ∈ X|.
  
\vfill
\item<3-> Let |val : X -> Y -> Real| be a \hl<3>{value}
  \hl<3>{function}: |val x y| is the value of taking decision |y| in
  state |x|.

\vfill
\item<4-> A policy |p : X -> Y| is called \hl<4>{optimal} w.r.t to |val|
  if it computes controls that are at least as good as any other control
  for all states.

\vfill
\item<5-> \sh{Exercise 2.4:} Give a mathematical specification of the
  notion of optimality for policies.

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: optimal policies}
%
\begin{itemize}
\vfill
\item<1-> If |Y| is \hl<1>{finite} and \hl<1>{non-empty}, one can implement

%if False

> X : Type
> Y : Type

%endif

> max     :  (Y -> Real) -> Real
>
> argmax  :  (Y -> Real) -> Y

\vfill
\item<2-> that fulfill

\begin{equation*}
  \text{|∀ ! f : Y -> Real|, \ |∀ ! y ∈ Y|, \ |f y <= max f|}
\end{equation*}  
\begin{equation*}
  \text{|∀ ! f : Y -> Real|, \ |f (argmax f) = max f|}
\end{equation*}  
  
\vfill
\item<3-> \sh{Exercise 2.5:} Using |max| and |argmax|, define a function
  |h : (X -> Y -> Real) -> (X -> Y)| that such that |h val : X -> Y| is an
  optimal policy w.r.t to |val|. Prove that |h val| is optimal.

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: optimal policies}
%
\begin{itemize}
\vfill

\item<1-> A policy |p : X -> Y| is called \hl<1>{optimal} w.r.t to |val
  : X -> Y -> Real| if it computes controls that are at least as good as
  any other control for all states.

\vfill
\item<2-> \sh{Exercise 2.4:} Give a mathematical specification of the
  notion of optimality for policies.
  
\vfill 
\item<3-> |p : X -> Y| is optimal w.r.t. |val : X -> Y -> Real| iff |∀ !
  x : X|, |∀ ! y ∈ Y|, \ |val x y <= val x (p x)|

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: verified optimal policies}
%
\begin{itemize}

\vfill
\item<1-> \sh{Exercise 2.5:} Using |max| and |argmax|, define a function
  |h : (X -> Y -> Real) -> (X -> Y)| that such that |h val : X -> Y| is an
  optimal policy w.r.t to |val|.

\vfill
\item<2-> Take

< h : (X -> Y -> Real) -> (X -> Y)
<  
< h val x = 

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: verified optimal policies}
%
\begin{itemize}

\vfill
\item<1-> \sh{Exercise 2.5:} Using |max| and |argmax|, define a function
  |h : (X -> Y -> Real) -> (X -> Y)| that such that |h val : X -> Y| is an
  optimal policy w.r.t to |val|.

\vfill
\item<1-> Take

> h : (X -> Y -> Real) -> (X -> Y)
>  
> h val x = argmax (val x)

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: verified optimal policies}
%
\begin{itemize}

\vfill
\item<1-> \sh{Exercise 2.5:} Prove that |h val : X -> Y| is optimal w.r.t to |val|.

\vfill
\item<2-> We have to show that |val x y <= val x (h val x)| for
  arbitrary |x| and |y|.

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: verified optimal policies}
%
\begin{itemize}

\vfill 
\item<1-> We have to show that |val x y <= val x (h val x)| for
  arbitrary |x| and |y|. We have

< val x y
<
<   <=  {- \hl{why?} -}
<
< max (val x)
<
<   = 
<
< ...
<
<   =
<
< val x (h val x)

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: verified optimal policies}
%
\begin{itemize}

\vfill 
\item<1-> We have to show that |val x y <= val x (h val x)| for
  arbitrary |x| and |y|. We have

< val x y
<
<   <=  {- |∀ ! f|, |∀ ! y|, \ |f y <= max f| \hl{with} |f = val x| -}
<
< max (val x)
<
<   = 
<
< ...
<
<   =
<
< val x (h val x)

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: verified optimal policies}
%
\begin{itemize}

\vfill
\item<1-> We have to show that |val x y <= val x (h val x)| for
  arbitrary |x| and |y|. We have

< val x y
<
<   <=  {- |∀ ! f|, |∀ ! y|, \ |f y <= max f| \hl{with} |f = val x| -}
<
< max (val x)
<
<   =   {- \hl{why?} -}
<
< val x (argmax (val x))
<
<   =   {- \hl{why?} -}
<
< val x (h val x)

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Example: verified optimal policies}
%
\begin{itemize}

\vfill
\item<1-> We have to show that |val x y <= val x (h val x)| for
  arbitrary |x| and |y|. We have

< val x y
<
<   <=  {- |∀ ! f|, |∀ ! y|, \ |f y <= max f| \hl{with} |f = val x| -}
<
< max (val x)
<
<   =   {- |∀ ! f|, \ |f (argmax f) = max f| \hl{with} |f = val x| -}
<
< val x (argmax (val x))
<
<   =   {- |h val x = argmax (val x)| by definition of |h| -}
<
< val x (h val x)

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Mathematical specifications and modelling}
%
\begin{itemize}

\vfill
\item<1-> In agent-based models (of green growth, opinion formation,
  consume, etc.) agents are described by their \hl<1>{properties}:

< status  :  Agent -> {Employed, Unemployed}
<
< income  :  Agent -> RPosz
<
< buy     :  Agent -> Prob {GreenCar, BrownCar, NoCar}

\vfill
\item<2-> Here, \hl<2>{|Prob X|} represents \hl<2>{probability}
  \hl<2>{distributions} over \hl<2>{|X|}. Let

< prob : Prob X -> (X -> Bool) -> [0,1]

be a generic function that returns the probability of an event:
\hl<2>{|prob d e|} is the probability of \hl<2>{|e|} according to
\hl<2>{|d|}.

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Mathematical specifications and modelling}
%
\begin{itemize}
\vfill
\item<1-> We want to specify the requirements that agents with
  \hl<1>{higher} incomes are \hl<1>{more} \hl<1>{likely} to buy
  \hl<1>{green} \hl<1>{cars} than agents with \hl<1>{lower} incomes and
  \dots

\vfill  
\item<2-> \dots that \hl<2>{unemployed} agents are less likely to buy a
  \hl<2>{brown} car than \hl<2>{employed} agents.

\vfill  
\item<3-> \sh{Exercise 2.6:} Express these specifications using the
  functions |status|, |income|, |buy| and |prob|.

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

%if False

\begin{frame}[fragile]

Let |eG, eB : {GreenCar, BrownCar, NoCar} -> Bool| be events such that

< eG GreenCar  =  True 
<
< eB BrownCar  =  True

We specify the two requirements with: |∀ ! x, y : Agent|,

\begin{equation*}
  \text{|income x > income y| |=>| |prob (buy x) eG > prob (buy y) eG|}
\end{equation*}  

and

\begin{equation*}
\begin{split}
  & \text{|status x = Unemployed ∧ status y = Employed|} \\
  & \text{|=>|} \\
  & \text{|prob (buy x) eB < prob (buy y) eB|}
\end{split}  
\end{equation*}  

%
\end{frame}

%endif

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Induction}
%
\begin{itemize}

\vfill
\item<1-> Exponentiation with natural numbers fulfills

\begin{equation*}
  \text{(1) \ |∀ ! x ∈ ℝ|, \ |pow x 0 = 1|}
\end{equation*}  

\begin{equation*}
  \text{(2) |∀ ! x ∈ ℝ|, |∀ ! m ∈ ℕ|, \ |pow x (1 + m) = x * pow x m|}
\end{equation*}  

\vfill
\item<2-> From (1), (2) and the properties of |*| and |+| one has

\begin{equation*}
  \text{|∀ ! x ∈ ℝ|, |∀ ! m,n ∈ ℕ|, \ |pow x (m + n) = pow x m * pow x n|}
\end{equation*}  

\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile] 
\frametitle{Induction} 
% 
\begin{itemize}

\vfill \item<1-> A proof by \hl<1>{induction} \hl<1>{on} \hl<1>{|m|}
consists of \hl<1>{two} subproofs: One proof for the \hl<2>{base}
\hl<2>{case} |m = 0| \dots

\begin{equation*} \text{|∀ ! x ∈ ℝ|, |∀ ! n ∈ ℕ|, \ |pow x (0 + n) = pow
  x 0 * pow x n|} \end{equation*}

\vfill \item<3-> \dots and one proof for the \hl<3>{induction}
\hl<3>{step}:

\begin{equation*} 
\begin{split} 
& \text{|∀ ! x ∈ ℝ|, |∀ ! n ∈ ℕ|, \ |pow x (m + n) = pow x m * pow x n|} \\ 
& \text{|=>|} \\ 
& \text{|∀ ! x ∈ ℝ|, |∀ ! n ∈ ℕ|, \ |pow x ((1 + m) + n) = pow x (1 + m) * pow x n|}
\end{split} 
\end{equation*}

\end{itemize} 
\vfill 
% 
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile] 
\frametitle{Induction} 
% 
\begin{itemize}

\vfill 
\item<1-> Let's look at the induction step first. 

\vfill 
\item<2-> We have 

\begin{equation*} 
\text{|∀ ! x ∈ ℝ|, |∀ ! n ∈ ℕ|, \ |pow x (m + n) = pow x m * pow x n|}
\end{equation*}

for a fixed |m|. 

\vfill 
\item<3-> We have to show

\begin{equation*} 
\begin{split} 
\text{|∀ ! x ∈ ℝ|, |∀ ! n ∈ ℕ|, \ |pow x ((1 + m) + n) = pow x (1 + m) * pow x n|}
\end{split} 
\end{equation*}

\end{itemize} 
\vfill 
% 
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile] 
\frametitle{Induction} 
% 
\begin{itemize}

\vfill 
\item<1-> We proceed by equational reasoning (for fixed |x| and |n|): 

< pow x ((1 + m) + n)
<
<   = {- \hl{why?} -}
<
< pow x (1 + (m + n))
<
<   = {- \hl{why?} -}
<
< x * (pow x (m + n))
<
<   = {- \hl{why?} -}
<
< x * (pow x m * pow x n)
<
<   = {- \hl{why?} -}
<
< (x * pow x m) * pow x n
<
<   = {- \hl{why?} -}
<
< (pow x (1 + m)) * pow x n

\vfill 
\item<2-> \sh{Exercise 2.6:} fill in justifications for all steps.

\end{itemize} 
\vfill 
% 
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile] 
\frametitle{Induction} 
% 
\begin{itemize}

\vfill 
\item<1-> We proceed by equational reasoning (for fixed |x| and |n|): 

< pow x ((1 + m) + n)
<
<   = {- \hl{why?} -}
<
< pow x (1 + (m + n))
<
<   = {- \hl{why?} -}
<
< x * (pow x (m + n))
<
<   = {- \hl{why?} -}
<
< x * (pow x m * pow x n)
<
<   = {- \hl{why?} -}
<
< (x * pow x m) * pow x n
<
<   = {- \hl{why?} -}
<
< (pow x (1 + m)) * pow x n

\vfill 
\item<1-> \sh{Exercise 2.7:} prove the base case.

\end{itemize} 
\vfill 
% 
\end{frame}

%% -------------------------------------------------------------------

%if False

\begin{frame}[fragile] 
\frametitle{Induction} 
% 
\begin{itemize}

\vfill 
\item<1-> \sh{Exercise 2.6:}

< pow x ((1 + m) + n)
<
<   = {- associativity of |+| -}
<
< pow x (1 + (m + n))
<
<   = {- equation (2) -}
<
< x * (pow x (m + n))
<
<   = {- induction hypothesis -}
<
< x * (pow x m * pow x n)
<
<   = {- associativity of |*| -}
<
< (x * pow x m) * pow x n
<
<   = {- equation (2) -}
<
< (pow x (1 + m)) * pow x n
  
\end{itemize} 
\vfill 
% 
\end{frame}

%endif

%% -------------------------------------------------------------------

%if False

\begin{frame}[fragile] 
\frametitle{Induction} 
% 
\begin{itemize}

\vfill 
\item<1-> \sh{Exercise 2.7:}

< pow x (0 + n)
<
<   = {- 0 left neutral element of |+| -}
<
< pow x n
<
<   = {- 1 left neutral element of |*| -}
<
< 1 * pow x n
<
<   = {- equation (1) -}
<
< pow x 0 * pow x n

\end{itemize} 
\vfill 
% 
\end{frame}

%endif

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Wrap-up, next lecture}
%
\begin{itemize}
%
\vfill
\item<1-> We have learned how to write \hl<1>{mathematical}
  \hl<1>{specifications} and how to prove elementary results via
  \hl<2>{induction} and \hl<2>{equational} \hl<2>{reasoning}.
%
\vfill
\item<3-> In the next lecture we learn \hl<3>{programming} with
  \hl<3>{dependent} \hl<3>{types}.
%
\end{itemize}
%
\end{frame}

%% -------------------------------------------------------------------


