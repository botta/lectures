# THD (Technische Hochschule Deggendorf) SS 2022 guest lectures

This repository will contain four lectures on climate policy,
mathematical specifications, dependent types and verified decision
making.

## PDF files

- [Lecture1.pdf](Lecture1.pdf): Decision problems in climate research

- [Lecture2.pdf](Lecture2.pdf): Mathematical specifications

- [Lecture3.pdf](Lecture3.pdf): Dependent types

- [Lecture4.pdf](Lecture4.pdf): Verified optimal decision making under uncertainty


For a more comprehensive course on the subject, please check
[this](https://gitlab.pik-potsdam.de/botta/lectures/-/tree/master/2019-2020.Formal_specifications_and_verified_decision_making).

## Literate Idris files

I have tested the files using Idris version 1.3.4. To install Idris
version 1.3.4, please go to
[starting](https://docs.idris-lang.org/en/latest/tutorial/starting.html).

- [Lecture2.lidr](Lecture2.lidr): the literate Idris file used to
  generate [Lecture2.pdf](Lecture2.pdf). Type check with `idris
  Lecture2.lidr`

- [Lecture3.lidr](Lecture3.lidr): the literate Idris file used to
  generate [Lecture3.pdf](Lecture3.pdf). Type check with `idris
  Lecture3.lidr`

- [Lecture4.lidr](Lecture4.lidr): the literate Idris file used to
  generate [Lecture4.pdf](Lecture4.pdf). You need to download
  [IdrisLibs2](https://gitlab.pik-potsdam.de/botta/IdrisLibs2) in order
  to type check this file. Assuming that `IDRISLIBS2` is the absolute
  path to IdrisLibs2, type check with `idris -i $IDRISLIBS2 --sourcepath
  $IDRISLIBS2 --allow-capitalized-pattern-variables Lecture4.lidr`







